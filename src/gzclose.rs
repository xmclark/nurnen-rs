/* gzclose.c -- zlib gzclose() function
 * Copyright (C) 2004, 2010 Mark Adler
 * For conditions of distribution and use, see copyright notice in zlib.h
 */
/* gzclose() is in a separate file so that it is linked in only if it is used.
   That way the other gzclose functions can be used instead to avoid linking in
   unneeded compression or decompression routines. */
pub static Z_STREAM_ERROR: c_int = -2;
pub static GZ_READ: c_int = 7247;
#[no_mangle]
pub unsafe extern "C" fn gzclose(mut file: gzFile) -> c_int {
    if file.is_null() { return Z_STREAM_ERROR; }
    let mut state = file as gz_statep;
    return if state.mode == GZ_READ {
               gzclose_r(file)
           } else { gzclose_w(file) };
}
