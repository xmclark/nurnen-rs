/* zutil.c -- target dependent utility functions for the compression library
 * Copyright (C) 1995-2017 Jean-loup Gailly
 * For conditions of distribution and use, see copyright notice in zlib.h
 */
/* @(#) $Id$ */
pub static mut z_errmsg: [*mut i8; 10] =
    ["need dictionary", /* Z_NEED_DICT       2  */
     "stream end", /* Z_STREAM_END      1  */
     "", /* Z_OK              0  */
     "file error", /* Z_ERRNO         (-1) */
     "stream error", /* Z_STREAM_ERROR  (-2) */
     "data error", /* Z_DATA_ERROR    (-3) */
     "insufficient memory", /* Z_MEM_ERROR     (-4) */
     "buffer error", /* Z_BUF_ERROR     (-5) */
     "incompatible version", /* Z_VERSION_ERROR (-6) */
     ""];
pub static ZLIB_VERSION: *const i8 = "1.2.11";
#[no_mangle]
pub unsafe extern "C" fn zlibVersion() -> *const i8 { return ZLIB_VERSION; }
#[no_mangle]
pub unsafe extern "C" fn zlibCompileFlags() -> uLong {
    let mut flags = 0;
    match std::mem::size_of::<uInt>() as c_int {
        2 => { }
        4 => flags += 1,
        8 => flags += 2,
        _ => flags += 3,
    }
    match std::mem::size_of::<uLong>() as c_int {
        2 => { }
        4 => flags += 1 << 2,
        8 => flags += 2 << 2,
        _ => flags += 3 << 2,
    }
    match std::mem::size_of::<voidpf>() as c_int {
        2 => { }
        4 => flags += 1 << 4,
        8 => flags += 2 << 4,
        _ => flags += 3 << 4,
    }
    match std::mem::size_of::<c_long>() as c_int {
        2 => { }
        4 => flags += 1 << 6,
        8 => flags += 2 << 6,
        _ => flags += 3 << 6,
    }
    return flags;
}
/* exported to allow conversion of error code to string for compress() and
 * uncompress()
 */
#[no_mangle]
pub unsafe extern "C" fn zError(mut err: c_int) -> *const i8 {
    return z_errmsg[2 - (err)];
}
/* The Microsoft C Run-Time Library for Windows CE doesn't have
     * errno.  We define it as a global variable to simplify porting.
     * Its value is always 0 and should not be used.
     */
/* ??? to be unrolled */
/* ??? to be unrolled */
/* Turbo C in 16-bit mode */
/* Turbo C malloc() does not allow dynamic allocation of 64K bytes
 * and farmalloc(64K) returns a pointer with an offset of 8, so we
 * must fix the pointer. Warning: the pointer must be put back to its
 * original form in order to free it, use zcfree().
 */
/* 10*64K = 640K */
/* This table is used to remember the original form of pointers
 * to large buffers (64K). Such pointers are normalized with a zero offset.
 * Since MSDOS is not a preemptive multitasking OS, this table is not
 * protected from concurrent access. This hack doesn't work anyway on
 * a protected system like OS/2. Use Microsoft C instead.
 */
/* If we allocate less than 65520 bytes, we assume that farmalloc
     * will return a usable pointer which doesn't have to be normalized.
     */
/* Normalize the pointer to seg:0 */
/* object < 64K */
/* Find the original pointer */
/* __TURBOC__ */
/* Microsoft C in 16-bit mode */
/* M_I86 */
/* SYS16BIT */
/* Any system without a special alloc function */
#[no_mangle]
pub unsafe extern "C" fn zcalloc(mut opaque: voidpf, mut items: c_uint,
                                 mut size: c_uint) -> voidpf {
    opaque; /* MY_ZCALLOC */
    return if std::mem::size_of::<uInt>() > 2 {
               malloc(items * size) as voidpf
           } else { calloc(items, size) as voidpf }; /* !Z_SOLO */
}
#[no_mangle]
pub unsafe extern "C" fn zcfree(mut opaque: voidpf, mut ptr: voidpf) {
    opaque;
    free(ptr);
}
