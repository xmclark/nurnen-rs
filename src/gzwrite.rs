/* gzwrite.c -- zlib functions for writing gzip files
 * Copyright (C) 2004-2017 Mark Adler
 * For conditions of distribution and use, see copyright notice in zlib.h
 */
/* Local functions */
/* Initialize state for writing a gzip file.  Mark initialization by setting
   state->size to non-zero.  Return -1 on a memory allocation failure, or 0 on
   success. */
/* allocate input buffer (double size for gzprintf) */
pub static Z_MEM_ERROR: c_int = -4;
/* only need output buffer and deflate state if compressing */
/* allocate output buffer */
/* allocate deflate memory, set up for gzip compression */
pub static Z_NULL: alloc_func = 0;
pub static Z_DEFLATED: c_int = 8;
pub static MAX_WBITS: c_int = 15;
pub static DEF_MEM_LEVEL: c_int = 8;
pub static Z_OK: c_int = 0;
unsafe fn gz_init(mut state: gz_statep) -> c_int {
    let mut ret; /* mark state as initialized */
    let mut strm =
        &mut (state.strm); /* initialize write buffer if compressing */
    state.in = malloc(state.want << 1) as *mut u8;
    if state.in.is_null() {
        gz_error(state, Z_MEM_ERROR, "out of memory");
        return -1;
    }
    if state.direct == 0 {
        state.out = malloc(state.want) as *mut u8;
        if state.out.is_null() {
            free(state.in as *mut _);
            gz_error(state, Z_MEM_ERROR, "out of memory");
            return -1;
        }
        strm.zalloc = Z_NULL;
        strm.zfree = Z_NULL;
        strm.opaque = Z_NULL;
        ret =
            deflateInit2_((strm), (state.level), (Z_DEFLATED),
                          (MAX_WBITS + 16), (DEF_MEM_LEVEL), (state.strategy),
                          "1.2.11", std::mem::size_of::<z_stream>() as c_int);
        if ret != Z_OK {
            free(state.out as *mut _);
            free(state.in as *mut _);
            gz_error(state, Z_MEM_ERROR, "out of memory");
            return -1;
        }
        strm.next_in = ptr::null_mut();
    }
    state.size = state.want;
    if state.direct == 0 {
        strm.avail_out = state.size;
        strm.next_out = state.out;
        state.x.next = strm.next_out;
    }
    return 0;
}
/* Compress whatever is at avail_in and next_in and write to the output file.
   Return -1 if there is an error writing to the output file or if gz_init()
   fails to allocate memory, otherwise 0.  flush is assumed to be a valid
   deflate() flush value.  If flush is Z_FINISH, then the deflate() state is
   reset to start a new gzip stream.  If gz->direct is true, then simply write
   to the output file without compressing, and ignore flush. */
/* allocate memory if this is the first time through */
/* write directly if requested */
pub static Z_ERRNO: c_int = -1;
/* run deflate() on provided input until it produces no more output */
/* write out current buffer contents if full, or if flushing, but if
           doing Z_FINISH then don't write until we get to Z_STREAM_END */
pub static Z_NO_FLUSH: c_int = 0;
pub static Z_FINISH: c_int = 4;
pub static Z_STREAM_END: c_int = 1;
/* compress */
pub static Z_STREAM_ERROR: c_int = -2;
unsafe fn gz_comp(mut state: gz_statep, mut flush: c_int) -> c_int {
    let mut writ; /* if that completed a deflate stream, allow another to start */
    let mut have: c_uint; /* all done, no errors */
    let mut put: c_uint;
    let mut max = ((-1 as c_uint) >> 2) + 1;
    let mut strm = &mut (state.strm);
    if state.size == 0 && gz_init(state) == -1 { return -1; }
    if state.direct != 0 {
        while strm.avail_in != 0 {
            put = if strm.avail_in > max { max } else { strm.avail_in };
            writ = write(state.fd, strm.next_in, put);
            if writ < 0 {
                gz_error(state, Z_ERRNO, strerror((*__error())));
                return -1;
            }
            strm.avail_in -= writ as c_uint;
            strm.next_in = strm.next_in.offset(writ);
        }
        return 0;
    }
    let mut ret = Z_OK;
    loop  {
        if strm.avail_out == 0 ||
               (flush != Z_NO_FLUSH &&
                    (flush != Z_FINISH || ret == Z_STREAM_END)) {
            while strm.next_out > state.x.next {
                put =
                    if strm.next_out.offset(-state.x.next) > (max as c_int) {
                        max
                    } else { strm.next_out.offset(-state.x.next) as c_uint };
                writ = write(state.fd, state.x.next, put);
                if writ < 0 {
                    gz_error(state, Z_ERRNO, strerror((*__error())));
                    return -1;
                }
                state.x.next = state.x.next.offset(writ);
            }
            if strm.avail_out == 0 {
                strm.avail_out = state.size;
                strm.next_out = state.out;
                state.x.next = state.out;
            };
        }
        have = strm.avail_out;
        ret = deflate(strm, flush);
        if ret == Z_STREAM_ERROR {
            gz_error(state, Z_STREAM_ERROR,
                     "internal error: deflate stream corrupt");
            return -1;
        }
        have -= strm.avail_out;
        if have == 0 { break  };
    }
    if flush == Z_FINISH { deflateReset(strm); }
    return 0;
}
/* Compress len zeros to output.  Return -1 on a write error or memory
   allocation failure by gz_comp(), or 0 on success. */
unsafe fn gz_zero(mut state: gz_statep, mut len: c_long) -> c_int {
    let mut n: c_uint; /* consume whatever's left in the input buffer */
    let mut strm =
        &mut (state.strm); /* compress len zeros (len guaranteed > 0) */
    if strm.avail_in != 0 && gz_comp(state, Z_NO_FLUSH) == -1 { return -1; }
    let mut first = 1;
    while len != 0 {
        n =
            if (std::mem::size_of::<c_int>() == std::mem::size_of::<c_long>()
                    && (state.size) > 2147483647) ||
                   (state.size as c_long) > len {
                len as c_uint
            } else { state.size };
        if first != 0 { memset(state.in as *mut _, 0, n); first = 0; }
        strm.avail_in = n;
        strm.next_in = state.in;
        state.x.pos += n;
        if gz_comp(state, Z_NO_FLUSH) == -1 { return -1; }
        len -= n;
    }
    return 0;
}
/* Write len bytes from buf to file.  Return the number of bytes written.  If
   the returned value is less than len, then there was an error. */
unsafe fn gz_write(mut state: gz_statep, mut buf: voidpc, mut len: z_size_t)
 -> z_size_t {
    let mut put = len; /* if len is zero, avoid unnecessary operations */
    if len == 0 {
        return 0; /* allocate memory if this is the first time through */
    } /* check for seek request */
    if state.size == 0 && gz_init(state) == -1 {
        return 0; /* for small len, copy to input buffer, otherwise compress directly */
    } /* copy to input buffer, compress when full */
    if state.seek != 0 {
        state.seek = 0; /* consume whatever's left in the input buffer */
        if gz_zero(state, state.skip) == -1 {
            return 0; /* directly compress user buffer to file */
        }; /* input was all buffered or compressed */
    }
    if len < state.size {
        loop  {
            let mut have: c_uint;
            let mut copy: c_uint;
            if state.strm.avail_in == 0 { state.strm.next_in = state.in; }
            have =
                state.strm.next_in.offset(state.strm.avail_in).offset(-state.in)
                    as c_uint;
            copy = state.size - have;
            if copy > len { copy = len; }
            memcpy(state.in.offset(have), buf, copy);
            state.strm.avail_in += copy;
            state.x.pos += copy;
            buf = (buf as *const i8).offset(copy);
            len -= copy;
            if len != 0 && gz_comp(state, Z_NO_FLUSH) == -1 { return 0; }
            if len == 0 { break  };
        };
    } else {
        if state.strm.avail_in != 0 && gz_comp(state, Z_NO_FLUSH) == -1 {
            return 0;
        }
        state.strm.next_in = buf as *mut Bytef;
        loop  {
            let mut n = -1 as c_uint;
            if n > len { n = len; }
            state.strm.avail_in = n;
            state.x.pos += n;
            if gz_comp(state, Z_NO_FLUSH) == -1 { return 0; }
            len -= n;
            if len == 0 { break  };
        };
    }
    return put;
}
/* -- see zlib.h -- */
/* get internal structure */
/* check that we're writing and that there's no error */
pub static GZ_WRITE: c_int = 31153;
/* since an int is returned, make sure len fits in one, otherwise return
       with an error (this avoids a flaw in the interface) */
pub static Z_DATA_ERROR: c_int = -3;
#[no_mangle]
pub unsafe extern "C" fn gzwrite(mut file: gzFile, mut buf: voidpc,
                                 mut len: c_uint) -> c_int {
    if file.is_null() {
        return 0; /* write len bytes from buf (the return value will fit in an int) */
    }
    let mut state = file as gz_statep;
    if state.mode != GZ_WRITE || state.err != Z_OK { return 0; }
    if (len as c_int) < 0 {
        gz_error(state, Z_DATA_ERROR, "requested length does not fit in int");
        return 0;
    }
    return gz_write(state, buf, len) as c_int;
}
/* -- see zlib.h -- */
#[no_mangle]
pub unsafe extern "C" fn gzfwrite(mut buf: voidpc, mut size: z_size_t,
                                  mut nitems: z_size_t, mut file: gzFile)
 -> z_size_t { /* get internal structure */
    if file.is_null() {
        return 0; /* check that we're writing and that there's no error */
    } /* compute bytes to read -- error on overflow */
    let mut state =
        file as
            gz_statep; /* write len bytes to buf, return the number of full items written */
    if state.mode != GZ_WRITE || state.err != Z_OK { return 0; }
    let mut len = nitems * size;
    if size != 0 && len / size != nitems {
        gz_error(state, Z_STREAM_ERROR, "request does not fit in a size_t");
        return 0;
    }
    return if len != 0 { gz_write(state, buf, len) / size } else { 0 };
}
/* -- see zlib.h -- */
#[no_mangle]
pub unsafe extern "C" fn gzputc(mut file: gzFile, mut c: c_int) -> c_int {
    let mut have: c_uint; /* get internal structure */
    let mut buf:
            [u8; 1]; /* check that we're writing and that there's no error */
    if file.is_null() {
        return -1; /* check for seek request */
    }
    let mut state = file as gz_statep;
    let mut strm = &mut (state.strm);
    if state.mode != GZ_WRITE || state.err != Z_OK { return -1; }
    if state.seek != 0 {
        state.seek = 0;
        if gz_zero(state, state.skip) == -1 { return -1; };
    }
    /* try writing to input buffer for speed (state->size == 0 if buffer not
           initialized) */
    if state.size != 0 {
        if strm.avail_in == 0 {
            strm.next_in =
                state.in; /* no room in buffer or not initialized, use gz_write() */
        }
        have = strm.next_in.offset(strm.avail_in).offset(-state.in) as c_uint;
        if have < state.size {
            *state.in.offset(have) = c as u8;
            strm.avail_in += 1;
            state.x.pos += 1;
            return c & 255;
        };
    }
    buf[0] = c as u8;
    if gz_write(state, buf, 1) != 1 { return -1; }
    return c & 255;
}
/* -- see zlib.h -- */
#[no_mangle]
pub unsafe extern "C" fn gzputs(mut file: gzFile, mut str: *const i8)
 -> c_int { /* get internal structure */
    if file.is_null() {
        return -1; /* check that we're writing and that there's no error */
    } /* write string */
    let mut state = file as gz_statep;
    if state.mode != GZ_WRITE || state.err != Z_OK { return -1; }
    let mut len = strlen(str);
    let mut ret = gz_write(state, str, len);
    return if ret == 0 && len != 0 { -1 } else { ret };
}
/* -- see zlib.h -- */
#[no_mangle]
pub unsafe extern "C" fn gzvprintf(mut file: gzFile, mut format: *const i8,
                                   mut va: va_list) -> c_int {
    let mut left: c_uint; /* get internal structure */
    if file.is_null() {
        return Z_STREAM_ERROR; /* check that we're writing and that there's no error */
    } /* make sure we have some buffer space */
    let mut state = file as gz_statep; /* check for seek request */
    let mut strm = &mut (state.strm);
    if state.mode != GZ_WRITE || state.err != Z_OK { return Z_STREAM_ERROR; }
    if state.size == 0 && gz_init(state) == -1 { return state.err; }
    if state.seek != 0 {
        state.seek = 0;
        if gz_zero(state, state.skip) == -1 { return state.err; };
    }
    /* do the printf() into the input buffer, put length in len -- the input
           buffer is double-sized just for this function, so there is guaranteed to
           be state->size bytes available after the current contents */
    if strm.avail_in == 0 {
        strm.next_in =
            state.in; /* check that printf() results fit in buffer */
    } /* update buffer and position, compress first half if past that */
    let mut next =
        state.in.offset(strm.next_in.offset(-state.in)).offset(strm.avail_in)
            as *mut i8; /* !STDC && !Z_HAVE_STDARG_H */
    *next.offset(state.size - 1) = 0i8;
    let mut len =
        __builtin___vsnprintf_chk(next, state.size, 0,
                                  __builtin_object_size(next as *const _,
                                                        if 2 > 1 {
                                                            1
                                                        } else { 0 }), format,
                                  va);
    if len == 0 || (len as c_uint) >= state.size ||
           *next.offset(state.size - 1) != 0 {
        return 0;
    }
    strm.avail_in += len as c_uint;
    state.x.pos += len;
    if strm.avail_in >= state.size {
        left = strm.avail_in - state.size;
        strm.avail_in = state.size;
        if gz_comp(state, Z_NO_FLUSH) == -1 { return state.err; }
        memcpy(state.in as *mut _, state.in.offset(state.size), left);
        strm.next_in = state.in;
        strm.avail_in = left;
    }
    return len;
}
#[no_mangle]
pub unsafe extern "C" fn gzprintf(mut file: gzFile,
                                  mut format: *const i8, ...) -> c_int {
    let mut va: va_list;
    __builtin_va_start(va, format);
    let mut ret = gzvprintf(file, format, va);
    __builtin_va_end(va);
    return ret;
}
/* -- see zlib.h -- */
/* get internal structure */
/* check that can really pass pointer in ints */
/* check that we're writing and that there's no error */
/* make sure we have some buffer space */
/* check for seek request */
/* do the printf() into the input buffer, put length in len -- the input
       buffer is double-sized just for this function, so there is guaranteed to
       be state->size bytes available after the current contents */
/* check that printf() results fit in buffer */
/* update buffer and position, compress first half if past that */
/* -- see zlib.h -- */
#[no_mangle]
pub unsafe extern "C" fn gzflush(mut file: gzFile, mut flush: c_int)
 -> c_int { /* get internal structure */
    if file.is_null() {
        return Z_STREAM_ERROR; /* check that we're writing and that there's no error */
    } /* check flush parameter */
    let mut state = file as gz_statep; /* check for seek request */
    if state.mode != GZ_WRITE || state.err != Z_OK {
        return Z_STREAM_ERROR; /* compress remaining data with requested flush */
    }
    if flush < 0 || flush > Z_FINISH { return Z_STREAM_ERROR; }
    if state.seek != 0 {
        state.seek = 0;
        if gz_zero(state, state.skip) == -1 { return state.err; };
    }
    gz_comp(state, flush);
    return state.err;
}
/* -- see zlib.h -- */
/* get internal structure */
/* check that we're writing and that there's no error */
/* if no change is requested, then do nothing */
/* check for seek request */
/* change compression parameters for subsequent input */
/* flush previous input with previous parameters before changing */
pub static Z_BLOCK: c_int = 5;
#[no_mangle]
pub unsafe extern "C" fn gzsetparams(mut file: gzFile, mut level: c_int,
                                     mut strategy: c_int) -> c_int {
    if file.is_null() { return Z_STREAM_ERROR; }
    let mut state = file as gz_statep;
    let mut strm = &mut (state.strm);
    if state.mode != GZ_WRITE || state.err != Z_OK { return Z_STREAM_ERROR; }
    if level == state.level && strategy == state.strategy { return Z_OK; }
    if state.seek != 0 {
        state.seek = 0;
        if gz_zero(state, state.skip) == -1 { return state.err; };
    }
    if state.size != 0 {
        if strm.avail_in != 0 && gz_comp(state, Z_BLOCK) == -1 {
            return state.err;
        }
        deflateParams(strm, level, strategy);
    }
    state.level = level;
    state.strategy = strategy;
    return Z_OK;
}
/* -- see zlib.h -- */
#[no_mangle]
pub unsafe extern "C" fn gzclose_w(mut file: gzFile) -> c_int {
    let mut ret = Z_OK; /* get internal structure */
    if file.is_null() {
        return Z_STREAM_ERROR; /* check that we're writing */
    } /* check for seek request */
    let mut state =
        file as gz_statep; /* flush, free memory, and close file */
    if state.mode != GZ_WRITE { return Z_STREAM_ERROR; }
    if state.seek != 0 {
        state.seek = 0;
        if gz_zero(state, state.skip) == -1 { ret = state.err; };
    }
    if gz_comp(state, Z_FINISH) == -1 { ret = state.err; }
    if state.size != 0 {
        if state.direct == 0 {
            deflateEnd(&mut (state.strm));
            free(state.out as *mut _);
        }
        free(state.in as *mut _);
    }
    gz_error(state, Z_OK, ptr::null_mut());
    free(state.path as *mut _);
    if close(state.fd) == -1 { ret = Z_ERRNO; }
    free(state);
    return ret;
}
