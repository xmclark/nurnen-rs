/* inffast.c -- fast decoding
 * Copyright (C) 1995-2017 Mark Adler
 * For conditions of distribution and use, see copyright notice in zlib.h
 */
/*
   Decode literal, length, and distance codes and write out the resulting
   literal and match bytes until either not enough input or output is
   available, an end-of-block is encountered, or a data error is encountered.
   When large enough input and output buffers are supplied to inflate(), for
   example, a 16K input buffer and a 64K output buffer, more than 95% of the
   inflate execution time is spent in this routine.

   Entry assumptions:

        state->mode == LEN
        strm->avail_in >= 6
        strm->avail_out >= 258
        start >= strm->avail_out
        state->bits < 8

   On return, state->mode is one of:

        LEN -- ran out of enough output space or enough available input
        TYPE -- reached end of block code, inflate() to interpret next block
        BAD -- error in block data

   Notes:

    - The maximum input bits used by a length/distance pair is 15 bits for the
      length code, 5 bits for the length extra, 15 bits for the distance code,
      and 13 bits for the distance extra.  This totals 48 bits, or six bytes.
      Therefore if strm->avail_in >= 6, then there is enough input to avoid
      checking for available input while decoding.

    - The maximum bytes that a single length/distance pair can output is 258
      bytes, which is the maximum length that can be coded.  inflate_fast()
      requires strm->avail_out >= 258 for each loop to avoid checking for
      output space.
 */
#[no_mangle]
pub unsafe extern "C" fn inflate_fast(mut strm: z_streamp,
                                      mut start:
                                          c_uint)  /* inflate()'s starting value for strm->avail_out */
 { /* local strm->next_in */
    /* have enough input while in < last */
    /* local strm->next_out */
    /* inflate()'s initial strm->next_out */
    /* while out < end, enough space available */
    /* maximum distance from zlib header */
    /* window size or zero if not using window */
    /* valid bytes in the window */
    /* window write index */
    /* allocated sliding window, if wsize != 0 */
    /* local strm->hold */
    /* local strm->bits */
    /* local strm->lencode */
    /* local strm->distcode */
    /* mask for first level of length codes */
    /* mask for first level of distance codes */
    let mut here: code; /* retrieved table entry */
    let mut op: c_uint; /* code bits, operation, extra bits, or */
    /*  window position, window bytes to copy */
    let mut len: c_uint; /* match length, unused bytes */
    let mut dist: c_uint; /* match distance */
    let mut from: *mut u8; /* where to copy match from */
    /* copy state to local variables */
    let mut state = strm.state as *mut inflate_state;
    let mut in_ = strm.next_in;
    let mut last = in_.offset((strm.avail_in - 5));
    let mut out = strm.next_out;
    let mut beg = out.offset(-(start - strm.avail_out));
    let mut end = out.offset((strm.avail_out - 257));
    let mut wsize = state.wsize;
    let mut whave = state.whave;
    let mut wnext = state.wnext;
    let mut window = state.window;
    let mut hold = state.hold;
    let mut bits = state.bits;
    let mut lcode = state.lencode;
    let mut dcode = state.distcode;
    let mut lmask = (1 << state.lenbits) - 1;
    let mut dmask = (1 << state.distbits) - 1;
    /* decode literals and length/distances until end-of-block or not enough
           input data or output space */
    loop  {
        if bits < 15 {
            hold +=
                ((*{
                       let mut _t = in_; /* literal */
                       in_ = in_.offset(1); /* length base */
                       _t
                   }) as c_long) << bits; /* number of extra bits */
            bits += 8; /* distance base */
            hold +=
                ((*{
                       let mut _t = in_; /* number of extra bits */
                       in_ = in_.offset(1); /* max distance in output */
                       _t
                   }) as c_long) << bits; /* see if copy from window */
            bits += 8; /* distance back in window */
        } /* very common case */
        here = *lcode.offset(hold & lmask); /* some from window */
        dolen:
            loop  {
                op = (here.bits) as c_uint; /* rest from output */
                break
            } /* wrap around window */
        hold >>= op; /* some from end of window */
        bits -= op; /* some from start of window */
        op = (here.op) as c_uint; /* rest from output */
        if op == 0 {
            *{
                 let mut _t = out; /* contiguous in window */
                 out = out.offset(1); /* some from window */
                 _t
             } = (here.val) as u8; /* rest from output */
        } else if op & 16 != 0 {
            len = (here.val) as c_uint; /* copy direct from output */
            op &= 15; /* minimum length is three */
            if op != 0 {
                if bits < op {
                    hold +=
                        ((*{
                               let mut _t = in_; /* 2nd level distance code */
                               in_ =
                                   in_.offset(1); /* 2nd level length code */
                               _t
                           }) as c_long) << bits; /* end-of-block */
                    bits += 8;
                }
                len += (hold as c_uint) & ((1 << op) - 1);
                hold >>= op;
                bits -= op;
            }
            if bits < 15 {
                hold +=
                    ((*{ let mut _t = in_; in_ = in_.offset(1); _t }) as
                         c_long) << bits;
                bits += 8;
                hold +=
                    ((*{ let mut _t = in_; in_ = in_.offset(1); _t }) as
                         c_long) << bits;
                bits += 8;
            }
            here = *dcode.offset(hold & dmask);
            dodist: loop  { op = (here.bits) as c_uint; break  }
            hold >>= op;
            bits -= op;
            op = (here.op) as c_uint;
            if op & 16 != 0 {
                dist = (here.val) as c_uint;
                op &= 15;
                if bits < op {
                    hold +=
                        ((*{ let mut _t = in_; in_ = in_.offset(1); _t }) as
                             c_long) << bits;
                    bits += 8;
                    if bits < op {
                        hold +=
                            ((*{ let mut _t = in_; in_ = in_.offset(1); _t })
                                 as c_long) << bits;
                        bits += 8;
                    };
                }
                dist += (hold as c_uint) & ((1 << op) - 1);
                hold >>= op;
                bits -= op;
                op = out.offset(-beg) as c_uint;
                if dist > op {
                    op = dist - op;
                    if op > whave {
                        if state.sane != 0 {
                            strm.msg = "invalid distance too far back";
                            state.mode = BAD;
                            break ;
                        };
                    }
                    from = window;
                    if wnext == 0 {
                        from = from.offset(wsize - op);
                        if op < len {
                            len -= op;
                            loop  {
                                *{ let mut _t = out; out = out.offset(1); _t }
                                    =
                                    *{
                                         let mut _t = from;
                                         from = from.offset(1);
                                         _t
                                     };
                                if { op -= 1; op } == 0 { break  };
                            }
                            from = out.offset(-dist);
                        };
                    } else if wnext < op {
                        from = from.offset(wsize + wnext - op);
                        op -= wnext;
                        if op < len {
                            len -= op;
                            loop  {
                                *{ let mut _t = out; out = out.offset(1); _t }
                                    =
                                    *{
                                         let mut _t = from;
                                         from = from.offset(1);
                                         _t
                                     };
                                if { op -= 1; op } == 0 { break  };
                            }
                            from = window;
                            if wnext < len {
                                op = wnext;
                                len -= op;
                                loop  {
                                    *{
                                         let mut _t = out;
                                         out = out.offset(1);
                                         _t
                                     } =
                                        *{
                                             let mut _t = from;
                                             from = from.offset(1);
                                             _t
                                         };
                                    if { op -= 1; op } == 0 { break  };
                                }
                                from = out.offset(-dist);
                            };
                        };
                    } else {
                        from = from.offset(wnext - op);
                        if op < len {
                            len -= op;
                            loop  {
                                *{ let mut _t = out; out = out.offset(1); _t }
                                    =
                                    *{
                                         let mut _t = from;
                                         from = from.offset(1);
                                         _t
                                     };
                                if { op -= 1; op } == 0 { break  };
                            }
                            from = out.offset(-dist);
                        };
                    }
                    while len > 2 {
                        *{ let mut _t = out; out = out.offset(1); _t } =
                            *{ let mut _t = from; from = from.offset(1); _t };
                        *{ let mut _t = out; out = out.offset(1); _t } =
                            *{ let mut _t = from; from = from.offset(1); _t };
                        *{ let mut _t = out; out = out.offset(1); _t } =
                            *{ let mut _t = from; from = from.offset(1); _t };
                        len -= 3;
                    }
                    if len != 0 {
                        *{ let mut _t = out; out = out.offset(1); _t } =
                            *{ let mut _t = from; from = from.offset(1); _t };
                        if len > 1 {
                            *{ let mut _t = out; out = out.offset(1); _t } =
                                *{
                                     let mut _t = from;
                                     from = from.offset(1);
                                     _t
                                 };
                        };
                    };
                } else {
                    from = out.offset(-dist);
                    loop  {
                        *{ let mut _t = out; out = out.offset(1); _t } =
                            *{ let mut _t = from; from = from.offset(1); _t };
                        *{ let mut _t = out; out = out.offset(1); _t } =
                            *{ let mut _t = from; from = from.offset(1); _t };
                        *{ let mut _t = out; out = out.offset(1); _t } =
                            *{ let mut _t = from; from = from.offset(1); _t };
                        len -= 3;
                        if len <= 2 { break  };
                    }
                    if len != 0 {
                        *{ let mut _t = out; out = out.offset(1); _t } =
                            *{ let mut _t = from; from = from.offset(1); _t };
                        if len > 1 {
                            *{ let mut _t = out; out = out.offset(1); _t } =
                                *{
                                     let mut _t = from;
                                     from = from.offset(1);
                                     _t
                                 };
                        };
                    };
                };
            } else if (op & 64) == 0 {
                here = *dcode.offset(here.val + (hold & ((1 << op) - 1)));
                break dodist ;
            } else {
                strm.msg = "invalid distance code";
                state.mode = BAD;
                break ;
            };
        } else if (op & 64) == 0 {
            here = *lcode.offset(here.val + (hold & ((1 << op) - 1)));
            break dolen ;
        } else if op & 32 != 0 {
            state.mode = TYPE;
            break ;
        } else {
            strm.msg = "invalid literal/length code";
            state.mode = BAD;
            break ;
        }
        if !(in_ < last && out < end) { break  };
    }
    /* return unused bytes (on entry, bits < 8, so in won't go too far back) */
    len = bits >> 3; /* update state and return */
    in_ = in_.offset(-len);
    bits -= len << 3;
    hold &= (1 << bits) - 1;
    strm.next_in = in_;
    strm.next_out = out;
    strm.avail_in =
        if in_ < last { 5 + last.offset(-in_) } else { 5 - in_.offset(-last) }
            as c_uint;
    strm.avail_out =
        if out < end {
            257 + end.offset(-out)
        } else { 257 - out.offset(-end) } as c_uint;
    state.hold = hold;
    state.bits = bits;
    return;
}
