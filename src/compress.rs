/* compress.c -- compress a memory buffer
 * Copyright (C) 1995-2005, 2014, 2016 Jean-loup Gailly, Mark Adler
 * For conditions of distribution and use, see copyright notice in zlib.h
 */
/* @(#) $Id$ */
/* ===========================================================================
     Compresses the source buffer into the destination buffer. The level
   parameter has the same meaning as in deflateInit.  sourceLen is the byte
   length of the source buffer. Upon entry, destLen is the total size of the
   destination buffer, which must be at least 0.1% larger than sourceLen plus
   12 bytes. Upon exit, destLen is the actual size of the compressed buffer.

     compress2 returns Z_OK if success, Z_MEM_ERROR if there was not enough
   memory, Z_BUF_ERROR if there was not enough room in the output buffer,
   Z_STREAM_ERROR if the level parameter is invalid.
*/
pub static Z_OK: c_int = 0;
pub static Z_NO_FLUSH: c_int = 0;
pub static Z_FINISH: c_int = 4;
pub static Z_STREAM_END: c_int = 1;
#[no_mangle]
pub unsafe extern "C" fn compress2(mut dest: *mut Bytef,
                                   mut destLen: &mut uLongf,
                                   mut source: *const Bytef,
                                   mut sourceLen: uLong, mut level: c_int)
 -> c_int {
    let mut stream: z_stream;
    let max = -1 as uInt;
    let mut left = *destLen;
    *destLen = 0;
    stream.zalloc = 0 as alloc_func;
    stream.zfree = 0 as free_func;
    stream.opaque = 0 as voidpf;
    let mut err =
        deflateInit_((&mut stream), (level), "1.2.11",
                     std::mem::size_of::<z_stream>() as c_int);
    if err != Z_OK { return err; }
    stream.next_out = dest;
    stream.avail_out = 0;
    stream.next_in = source as *mut Bytef;
    stream.avail_in = 0;
    loop  {
        if stream.avail_out == 0 {
            stream.avail_out =
                if left > (max as uLong) { max } else { left as uInt };
            left -= stream.avail_out;
        }
        if stream.avail_in == 0 {
            stream.avail_in =
                if sourceLen > (max as uLong) {
                    max
                } else { sourceLen as uInt };
            sourceLen -= stream.avail_in;
        }
        err =
            deflate(&mut stream,
                    if sourceLen != 0 { Z_NO_FLUSH } else { Z_FINISH });
        if err != Z_OK { break  };
    }
    *destLen = stream.total_out;
    deflateEnd(&mut stream);
    return if err == Z_STREAM_END { Z_OK } else { err };
}
/* ===========================================================================
 */
pub static Z_DEFAULT_COMPRESSION: c_int = -1;
#[no_mangle]
pub unsafe extern "C" fn compress(mut dest: *mut Bytef,
                                  mut destLen: *mut uLongf,
                                  mut source: *const Bytef,
                                  mut sourceLen: uLong) -> c_int {
    return compress2(dest, destLen, source, sourceLen, Z_DEFAULT_COMPRESSION);
}
/* ===========================================================================
     If the default memLevel or windowBits for deflateInit() is changed, then
   this function needs to be updated.
 */
#[no_mangle]
pub unsafe extern "C" fn compressBound(mut sourceLen: uLong) -> uLong {
    return sourceLen + (sourceLen >> 12) + (sourceLen >> 14) +
               (sourceLen >> 25) + 13;
}
