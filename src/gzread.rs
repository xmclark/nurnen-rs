/* gzread.c -- zlib functions for reading gzip files
 * Copyright (C) 2004, 2005, 2010, 2011, 2012, 2013, 2016 Mark Adler
 * For conditions of distribution and use, see copyright notice in zlib.h
 */
/* Local functions */
/* Use read() to load a buffer -- return -1 on error, otherwise 0.  Read from
   state->fd, and update state->eof, state->err, and state->msg as appropriate.
   This function needs to loop on read(), since read() is not guaranteed to
   read the number of bytes requested, depending on the type of descriptor. */
pub static Z_ERRNO: c_int = -1;
unsafe fn gz_load(mut state: gz_statep, mut buf: *mut u8, mut len: c_uint,
                  mut have: &mut c_uint) -> c_int {
    let mut ret;
    let mut get: c_uint;
    let mut max = ((-1 as c_uint) >> 2) + 1;
    *have = 0;
    loop  {
        get = len - *have;
        if get > max { get = max; }
        ret = read(state.fd, buf.offset(*have), get);
        if ret <= 0 { break ; }
        *have += ret as c_uint;
        if *have >= len { break  };
    }
    if ret < 0 {
        gz_error(state, Z_ERRNO, strerror((*__error())));
        return -1;
    }
    if ret == 0 { state.eof = 1; }
    return 0;
}
/* Load up input buffer and set eof flag if last data loaded -- return -1 on
   error, 0 otherwise.  Note that the eof flag is set when the end of the input
   file is reached, even though there may be unused data in the buffer.  Once
   that data has been used, no more attempts will be made to read the file.
   If strm->avail_in != 0, then the current data is moved to the beginning of
   the input buffer, and then the remainder of the buffer is loaded with the
   available data from the input file. */
pub static Z_OK: c_int = 0;
pub static Z_BUF_ERROR: c_int = -5;
unsafe fn gz_avail(mut state: gz_statep) -> c_int {
    let mut got: c_uint; /* copy what's there to the start */
    let mut strm = &mut (state.strm);
    if state.err != Z_OK && state.err != Z_BUF_ERROR { return -1; }
    if state.eof == 0 {
        if strm.avail_in != 0 {
            let mut p = state.in;
            let mut q = strm.next_in;
            let mut n = strm.avail_in;
            loop  {
                *{ let mut _t = p; p = p.offset(1); _t } =
                    *{ let mut _t = q; q = q.offset(1); _t };
                if { n -= 1; n } == 0 { break  };
            };
        }
        if gz_load(state, state.in.offset(strm.avail_in),
                   state.size - strm.avail_in, &mut got) == -1 {
            return -1;
        }
        strm.avail_in += got;
        strm.next_in = state.in;
    }
    return 0;
}
/* Look for gzip header, set up for inflate or copy.  state->x.have must be 0.
   If this is the first time in, allocate required memory.  state->how will be
   left unchanged if there is no more input data available, will be set to COPY
   if there is no gzip header and direct copying will be performed, or it will
   be set to GZIP for decompression.  If direct copying, then leftover input
   data from the input buffer will be copied to the output buffer.  In that
   case, all further file reads will be directly to either the output buffer or
   a user buffer.  If decompressing, the inflate state will be initialized.
   gz_look() will return 0 on success or -1 on failure. */
/* allocate read buffers and inflate memory */
/* allocate buffers */
pub static Z_MEM_ERROR: c_int = -4;
/* allocate inflate memory */
pub static Z_NULL: alloc_func = 0;
/* gunzip */
/* get at least the magic bytes in the input buffer */
/* look for gzip magic bytes -- if there, do gzip decoding (note: there is
       a logical dilemma here when considering the case of a partially written
       gzip file, to wit, if a single 31 byte is written, then we cannot tell
       whether this is a single-byte file, or just a partially written gzip
       file -- for here we assume that if a gzip file is being written, then
       the header will be written in a single operation, so that reading a
       single byte is sufficient indication that it is not a gzip file) */
pub static GZIP: c_int = 2;
/* no gzip header -- if we were decoding gzip before, then this is trailing
       garbage.  Ignore the trailing garbage and finish. */
/* doing raw i/o, copy any leftover input to output -- this assumes that
       the output buffer is larger than the input buffer, which also assures
       space for gzungetc() */
pub static COPY: c_int = 1;
unsafe fn gz_look(mut state: gz_statep) -> c_int {
    let mut strm = &mut (state.strm);
    if state.size == 0 {
        state.in = malloc(state.want) as *mut u8;
        state.out = malloc(state.want << 1) as *mut u8;
        if state.in.is_null() || state.out.is_null() {
            free(state.out as *mut _);
            free(state.in as *mut _);
            gz_error(state, Z_MEM_ERROR, "out of memory");
            return -1;
        }
        state.size = state.want;
        state.strm.zalloc = Z_NULL;
        state.strm.zfree = Z_NULL;
        state.strm.opaque = Z_NULL;
        state.strm.avail_in = 0;
        state.strm.next_in = Z_NULL;
        if inflateInit2_((&mut (state.strm)), (15 + 16), "1.2.11",
                         std::mem::size_of::<z_stream>() as c_int) != Z_OK {
            free(state.out as *mut _);
            free(state.in as *mut _);
            state.size = 0;
            gz_error(state, Z_MEM_ERROR, "out of memory");
            return -1;
        };
    }
    if strm.avail_in < 2 {
        if gz_avail(state) == -1 { return -1; }
        if strm.avail_in == 0 { return 0; };
    }
    if strm.avail_in > 1 && strm.next_in[0] == 31 && strm.next_in[1] == 139 {
        inflateReset(strm);
        state.how = GZIP;
        state.direct = 0;
        return 0;
    }
    if state.direct == 0 {
        strm.avail_in = 0;
        state.eof = 1;
        state.x.have = 0;
        return 0;
    }
    state.x.next = state.out;
    if strm.avail_in != 0 {
        memcpy(state.x.next as *mut _, strm.next_in as *const _,
               strm.avail_in);
        state.x.have = strm.avail_in;
        strm.avail_in = 0;
    }
    state.how = COPY;
    state.direct = 1;
    return 0;
}
/* Decompress from input to the provided next_out and avail_out in the state.
   On return, state->x.have and state->x.next point to the just decompressed
   data.  If the gzip stream completes, state->how is reset to LOOK to look for
   the next gzip stream or raw data, once state->x.have is depleted.  Returns 0
   on success, -1 on failure. */
/* fill output buffer up to end of deflate stream */
/* get more input for inflate() */
/* decompress and handle errors */
pub static Z_NO_FLUSH: c_int = 0;
pub static Z_STREAM_ERROR: c_int = -2;
pub static Z_NEED_DICT: c_int = 2;
pub static Z_DATA_ERROR: c_int = -3;
/* deflate stream invalid */
pub static Z_STREAM_END: c_int = 1;
/* update available output */
/* if the gzip stream completed successfully, look for another */
pub static LOOK: c_int = 0;
unsafe fn gz_decomp(mut state: gz_statep) -> c_int {
    let mut ret = Z_OK; /* good decompression */
    let mut strm = &mut (state.strm);
    let mut had = strm.avail_out;
    loop  {
        if strm.avail_in == 0 && gz_avail(state) == -1 { return -1; }
        if strm.avail_in == 0 {
            gz_error(state, Z_BUF_ERROR, "unexpected end of file");
            break ;
        }
        ret = inflate(strm, Z_NO_FLUSH);
        if ret == Z_STREAM_ERROR || ret == Z_NEED_DICT {
            gz_error(state, Z_STREAM_ERROR,
                     "internal error: inflate stream corrupt");
            return -1;
        }
        if ret == Z_MEM_ERROR {
            gz_error(state, Z_MEM_ERROR, "out of memory");
            return -1;
        }
        if ret == Z_DATA_ERROR {
            gz_error(state, Z_DATA_ERROR,
                     if strm.msg.is_null() {
                         "compressed data error"
                     } else { strm.msg });
            return -1;
        }
        if !(strm.avail_out != 0 && ret != Z_STREAM_END) { break  };
    }
    state.x.have = had - strm.avail_out;
    state.x.next = strm.next_out.offset(-state.x.have);
    if ret == Z_STREAM_END { state.how = LOOK; }
    return 0;
}
/* Fetch data and put it in the output buffer.  Assumes state->x.have is 0.
   Data is either copied from the input file or decompressed from the input
   file depending on state->how.  If state->how is LOOK, then a gzip header is
   looked for to determine whether to copy or decompress.  Returns -1 on error,
   otherwise 0.  gz_fetch() will leave state->how as COPY or GZIP unless the
   end of the input file has been reached and all data has been processed.  */
unsafe fn gz_fetch(mut state: gz_statep) -> c_int {
    let mut strm =
        &mut (state.strm); /* -> LOOK, COPY (only if never GZIP), or GZIP */
    loop  {
        match state.how {
            LOOK => {
                if gz_look(state) == -1 {
                    return -1; /* -> COPY */
                } /* -> GZIP or LOOK (if end of gzip stream) */
                if state.how == LOOK { return 0; }
            }
            COPY => {
                if gz_load(state, state.out, state.size << 1,
                           &mut (state.x.have)) == -1 {
                    return -1;
                }
                state.x.next = state.out;
                return 0
            }
            GZIP => {
                strm.avail_out = state.size << 1;
                strm.next_out = state.out;
                if gz_decomp(state) == -1 { return -1; }
            }
        }
        if !(state.x.have == 0 && (state.eof == 0 || strm.avail_in != 0)) {
            break
        };
    }
    return 0;
}
/* Skip len uncompressed bytes of output.  Return -1 on error, 0 on success. */
unsafe fn gz_skip(mut state: gz_statep, mut len: c_long) -> c_int {
    let mut n:
            c_uint; /* skip over len bytes or reach end-of-file, whichever comes first */
    while len != 0  /* skip over whatever is in output buffer */
          {
        if state.x.have != 0 {
            n =
                if (std::mem::size_of::<c_int>() ==
                        std::mem::size_of::<c_long>() &&
                        (state.x.have) > 2147483647) ||
                       (state.x.have as c_long) > len {
                    len as c_uint
                } else {
                    state.x.have
                }; /* output buffer empty -- return if we're at the end of the input */
            state.x.have -=
                n; /* need more data to skip -- load up output buffer */
            state.x.next =
                state.x.next.offset(n); /* get more output, looking for header if required */
            state.x.pos += n;
            len -= n;
        } else if state.eof != 0 && state.strm.avail_in == 0 {
            break ;
        } else { if gz_fetch(state) == -1 { return -1; }; }
    }
    return 0;
}
/* Read len bytes into buf from file, or less than len up to the end of the
   input.  Return the number of bytes read.  If zero is returned, either the
   end of file was reached, or there was an error.  state->err must be
   consulted in that case to determine which. */
unsafe fn gz_read(mut state: gz_statep, mut buf: voidp, mut len: z_size_t)
 -> z_size_t {
    let mut n: c_uint; /* if len is zero, avoid unnecessary operations */
    if len == 0 {
        return 0; /* process a skip request */
    } /* get len bytes to buf, or less than len if at the end */
    if state.seek != 0 {
        state.seek =
            0; /* set n to the maximum amount of len that fits in an unsigned int */
        if gz_skip(state, state.skip) == -1 {
            return 0; /* first just try copying data from the output buffer */
        }; /* output buffer empty -- return if we're at the end of the input */
    } /* tried to read past end */
    let mut got = 0;
    loop  {
        n = -1;
        if n > len { n = len; }
        if state.x.have != 0 {
            if state.x.have < n { n = state.x.have; }
            memcpy(buf, state.x.next as *const _, n);
            state.x.next = state.x.next.offset(n);
            state.x.have -= n;
        } else if state.eof != 0 && state.strm.avail_in == 0 {
            state.past = 1;
            break ;
        } else if 
         /* need output data -- for small len or new stream load up our output
                    buffer */
         state.how == LOOK || n < (state.size << 1)
         { /* get more output, looking for header if required */
            if gz_fetch(state) == -1 {
                return 0; /* no progress yet -- go back to copy above */
            }
            continue ;
            /* the copy above assures that we will leave with space in the
                           output buffer, allowing at least one gzungetc() to succeed */
        } else if  /* large len -- read directly into user buffer */
         state.how == COPY { /* read directly */
            if gz_load(state, buf as *mut u8, n, &mut n) == -1 {
                return 0; /* large len -- decompress directly into user buffer */
            }; /* state->how == GZIP */
        } else {
            state.strm.avail_out = n; /* update progress */
            state.strm.next_out =
                buf as
                    *mut u8; /* return number of bytes read into user buffer */
            if gz_decomp(state) == -1 { return 0; }
            n = state.x.have;
            state.x.have = 0;
        }
        len -= n;
        buf = (buf as *mut i8).offset(n);
        got += n;
        state.x.pos += n;
        if len == 0 { break  };
    }
    return got;
}
/* -- see zlib.h -- */
/* get internal structure */
/* check that we're reading and that there's no (serious) error */
pub static GZ_READ: c_int = 7247;
#[no_mangle]
pub unsafe extern "C" fn gzread(mut file: gzFile, mut buf: voidp,
                                mut len: c_uint) -> c_int {
    if file.is_null() { return -1; }
    let mut state = file as gz_statep;
    if state.mode != GZ_READ ||
           (state.err != Z_OK && state.err != Z_BUF_ERROR) {
        return -1;
    }
    /* since an int is returned, make sure len fits in one, otherwise return
           with an error (this avoids a flaw in the interface) */
    if (len as c_int) < 0 {
        gz_error(state, Z_STREAM_ERROR,
                 "request does not fit in an int"); /* read len or fewer bytes to buf */
        return -1; /* check for an error */
    } /* return the number of bytes read (this is assured to fit in an int) */
    len = gz_read(state, buf, len);
    if len == 0 && state.err != Z_OK && state.err != Z_BUF_ERROR {
        return -1;
    }
    return len as c_int;
}
/* -- see zlib.h -- */
#[no_mangle]
pub unsafe extern "C" fn gzfread(mut buf: voidp, mut size: z_size_t,
                                 mut nitems: z_size_t, mut file: gzFile)
 -> z_size_t { /* get internal structure */
    if file.is_null() {
        return 0; /* check that we're reading and that there's no (serious) error */
    } /* compute bytes to read -- error on overflow */
    let mut state =
        file as
            gz_statep; /* read len or fewer bytes to buf, return the number of full items read */
    if state.mode != GZ_READ ||
           (state.err != Z_OK && state.err != Z_BUF_ERROR) {
        return 0;
    }
    let mut len = nitems * size;
    if size != 0 && len / size != nitems {
        gz_error(state, Z_STREAM_ERROR, "request does not fit in a size_t");
        return 0;
    }
    return if len != 0 { gz_read(state, buf, len) / size } else { 0 };
}
/* -- see zlib.h -- */
#[no_mangle]
pub unsafe extern "C" fn gzgetc(mut file: gzFile) -> c_int {
    let mut buf: [u8; 1]; /* get internal structure */
    if file.is_null() {
        return -1; /* check that we're reading and that there's no (serious) error */
    } /* try output buffer (no need to check for skip request) */
    let mut state = file as gz_statep; /* nothing there -- try gz_read() */
    if state.mode != GZ_READ ||
           (state.err != Z_OK && state.err != Z_BUF_ERROR) {
        return -1;
    }
    if state.x.have != 0 {
        state.x.have -= 1;
        state.x.pos += 1;
        return *{
                    let mut _t = (state.x.next);
                    (state.x.next) = (state.x.next).offset(1);
                    _t
                };
    }
    let mut ret = gz_read(state, buf, 1);
    return if ret < 1 { -1 } else { buf[0] };
}
#[no_mangle]
pub unsafe extern "C" fn gzgetc_(mut file: gzFile) -> c_int {
    return gzgetc(file);
}
/* -- see zlib.h -- */
#[no_mangle]
pub unsafe extern "C" fn gzungetc(mut c: c_int, mut file: gzFile)
 -> c_int { /* get internal structure */
    if file.is_null() {
        return -1; /* check that we're reading and that there's no (serious) error */
    } /* process a skip request */
    let mut state = file as gz_statep; /* can't push EOF */
    if state.mode != GZ_READ ||
           (state.err != Z_OK && state.err != Z_BUF_ERROR) {
        return -1; /* if output buffer empty, put byte at end (allows more pushing) */
    } /* if no room, give up (must have already done a gzungetc()) */
    if state.seek != 0 {
        state.seek =
            0; /* slide output data if needed and insert byte before existing data */
        if gz_skip(state, state.skip) == -1 { return -1; };
    }
    if c < 0 { return -1; }
    if state.x.have == 0 {
        state.x.have = 1;
        state.x.next = state.out.offset((state.size << 1)).offset(-1);
        *state.x.next.offset(0) = c as u8;
        state.x.pos -= 1;
        state.past = 0;
        return c;
    }
    if state.x.have == (state.size << 1) {
        gz_error(state, Z_DATA_ERROR, "out of room to push characters");
        return -1;
    }
    if state.x.next == state.out {
        let mut src = state.out.offset(state.x.have);
        let mut dest = state.out.offset((state.size << 1));
        while src > state.out {
            *{ dest = dest.offset(-1); dest } =
                *{ src = src.offset(-1); src };
        }
        state.x.next = dest;
    }
    state.x.have += 1;
    state.x.next = state.x.next.offset(-1);
    *state.x.next.offset(0) = c as u8;
    state.x.pos -= 1;
    state.past = 0;
    return c;
}
/* -- see zlib.h -- */
#[no_mangle]
pub unsafe extern "C" fn gzgets(mut file: gzFile, mut buf: *mut i8,
                                mut len: c_int) -> *mut i8 {
    let mut n: c_uint; /* check parameters and get internal structure */
    let mut eol:
            *mut u8; /* check that we're reading and that there's no (serious) error */
    if file.is_null() || buf.is_null() || len < 1 {
        return ptr::null_mut(); /* process a skip request */
    }
    let mut state = file as gz_statep;
    if state.mode != GZ_READ ||
           (state.err != Z_OK && state.err != Z_BUF_ERROR) {
        return ptr::null_mut();
    }
    if state.seek != 0 {
        state.seek = 0;
        if gz_skip(state, state.skip) == -1 { return ptr::null_mut(); };
    }
    /* copy output bytes up to new line or len - 1, whichever comes first --
           append a terminating zero to the string (we don't check for a zero in
           the contents, let the user worry about that) */
    let mut str = buf; /* assure that something is in the output buffer */
    let mut left = (len as c_uint) - 1; /* error */
    if left != 0 {
        loop  {
            if state.x.have == 0 && gz_fetch(state) == -1 {
                return ptr::null_mut(); /* end of file */
            } /* read past end */
            if state.x.have == 0 {
                state.past = 1; /* return what we have */
                break ; /* look for end-of-line in current output buffer */
            } /* copy through end-of-line, or remainder if not found */
            n =
                if state.x.have > left {
                    left
                } else {
                    state.x.have
                }; /* return terminated string, or if nothing, end of file */
            eol = memchr(state.x.next as *const _, '\n', n) as *mut u8;
            if !eol.is_null() {
                n = (eol.offset(-state.x.next) as c_uint) + 1;
            }
            memcpy(buf as *mut _, state.x.next as *const _, n);
            state.x.have -= n;
            state.x.next = state.x.next.offset(n);
            state.x.pos += n;
            left -= n;
            buf = buf.offset(n);
            if !(left != 0 && eol.is_null()) { break  };
        };
    }
    if buf == str { return ptr::null_mut(); }
    *buf.offset(0) = 0i8;
    return str;
}
/* -- see zlib.h -- */
#[no_mangle]
pub unsafe extern "C" fn gzdirect(mut file: gzFile)
 -> c_int { /* get internal structure */
    if file.is_null() { return 0; }
    let mut state = file as gz_statep;
    /* if the state is not known, but we can find out, then do so (this is
           mainly for right after a gzopen() or gzdopen()) */
    if state.mode == GZ_READ && state.how == LOOK && state.x.have == 0 {
        gz_look(state); /* return 1 if transparent, 0 if processing a gzip stream */
    }
    return state.direct;
}
/* -- see zlib.h -- */
#[no_mangle]
pub unsafe extern "C" fn gzclose_r(mut file: gzFile)
 -> c_int { /* get internal structure */
    if file.is_null() {
        return Z_STREAM_ERROR; /* check that we're reading */
    } /* free memory and close file */
    let mut state = file as gz_statep;
    if state.mode != GZ_READ { return Z_STREAM_ERROR; }
    if state.size != 0 {
        inflateEnd(&mut (state.strm));
        free(state.out as *mut _);
        free(state.in as *mut _);
    }
    let mut err = if state.err == Z_BUF_ERROR { Z_BUF_ERROR } else { Z_OK };
    gz_error(state, Z_OK, ptr::null_mut());
    free(state.path as *mut _);
    let mut ret = close(state.fd);
    free(state);
    return if ret != 0 { Z_ERRNO } else { err };
}
