pub const Z_NO_FLUSH: i32 = 0;
pub const Z_PARTIAL_FLUSH: i32 = 1;
pub const Z_FULL_FLUSH: i32 = 3;
pub const Z_FINISH: i32 = 4;
pub const Z_BLOCK: i32 = 5;

pub const Z_OK: i32 = 0;
pub const Z_STREAM_END: i32 = 1;
pub const Z_STREAM_ERROR: i32 = -2;
pub const Z_DATA_ERROR: i32 = -3;
pub const Z_BUF_ERROR: i32 = -5;

pub const Z_DEFAULT_COMPRESSION: i32 = -1;

pub const Z_FILTERED: i32 = 1;
pub const Z_HUFFMAN_ONLY: i32 = 2;
pub const Z_RLE: i32 = 3;
pub const Z_FIXED: i32 = 4;
pub const Z_DEFAULT_STRATEGY: i32 = 0;

pub const Z_UNKNOWN: i32 = 0;

pub const Z_DEFLATED: i32 = 8;

pub const MAX_MEM_LEVEL: i32 = 9;

pub const MAX_WBITS: i32 = 15;

pub const DEF_MEM_LEVEL: i32 = 8;

pub const LENGTH_CODES: i32 = 29;

pub const LITERALS: i32 = 256;

pub const L_CODES: i32 = LITERALS + 1 + LENGTH_CODES;

pub const D_CODES: i32 = 30;

pub const BL_CODES: i32 = 19;

pub const HEAP_SIZE : i32 = 2 * L_CODES + 1;

pub const MAX_BITS: i32 = 15;

pub const MIN_MATCH: i32 = 3;
pub const MAX_MATCH: i32 = 258;

pub const MIN_LOOKAHEAD: i32 = MAX_MATCH + MIN_MATCH + 1;

pub const PRESET_DICT: i32 = 0x20;


/* deflate.c -- compress data using the deflation algorithm
 * Copyright (C) 1995-2017 Jean-loup Gailly and Mark Adler
 * For conditions of distribution and use, see copyright notice in zlib.h
 */
/*
 *  ALGORITHM
 *
 *      The "deflation" process depends on being able to identify portions
 *      of the input text which are identical to earlier input (within a
 *      sliding window trailing behind the input currently being processed).
 *
 *      The most straightforward technique turns out to be the fastest for
 *      most input files: try all possible matches and select the longest.
 *      The key feature of this algorithm is that insertions into the string
 *      dictionary are very simple and thus fast, and deletions are avoided
 *      completely. Insertions are performed at each input character, whereas
 *      string matches are performed only when the previous match ends. So it
 *      is preferable to spend more time in matches to allow very fast string
 *      insertions and avoid deletions. The matching algorithm for small
 *      strings is inspired from that of Rabin & Karp. A brute force approach
 *      is used to find longer strings when a small match has been found.
 *      A similar algorithm is used in comic (by Jan-Mark Wams) and freeze
 *      (by Leonid Broukhis).
 *         A previous version of this file used a more sophisticated algorithm
 *      (by Fiala and Greene) which is guaranteed to run in linear amortized
 *      time, but has a larger average cost, uses more memory and is patented.
 *      However the F&G algorithm may be faster for some highly redundant
 *      files if the parameter max_chain_length (described below) is too large.
 *
 *  ACKNOWLEDGEMENTS
 *
 *      The idea of lazy evaluation of matches is due to Jan-Mark Wams, and
 *      I found it in 'freeze' written by Leonid Broukhis.
 *      Thanks to many people for bug reports and testing.
 *
 *  REFERENCES
 *
 *      Deutsch, L.P.,"DEFLATE Compressed Data Format Specification".
 *      Available in http://tools.ietf.org/html/rfc1951
 *
 *      A description of the Rabin and Karp algorithm is given in the book
 *         "Algorithms" by R. Sedgewick, Addison-Wesley, p252.
 *
 *      Fiala,E.R., and Greene,D.H.
 *         Data Compression with Finite Windows, Comm.ACM, 32,4 (1989) 490-595
 *
 */
/* @(#) $Id$ */
pub static mut deflate_copyright: [i8; 69] =
    " deflate 1.2.11 Copyright 1995-2017 Jean-loup Gailly and Mark Adler ";
/*
  If you use the zlib library in a product, an acknowledgment is welcome
  in the documentation of your product. If for some reason you cannot
  include such an acknowledgment, I would appreciate that you keep this
  copyright string in the executable of your product.
 */
/* ===========================================================================
 *  Function prototypes.
 */
pub enum block_state {
    need_more,
    block_done,
    finish_started,
    finish_done, /* block not completed, need more input or more output */
    /* block flush performed */
    /* finish started, need only more output at next deflate */
    /* finish done, accept no more input or output */
}






// pub type compress_func = *mut unsafe extern "C" fn(, ...);




/* Compression function. Returns the block state after the call. */
/* asm code initialization */
/* ===========================================================================
 * Local data
 */
/* Tail of hash chains */
/* Matches of length 3 are discarded if their distance exceeds TOO_FAR */
/* Values for max_lazy_match, good_match and max_chain_length, depending on
 * the desired pack level (0..9). The values given below have been tuned to
 * exclude worst case performance for pathological files. Better values may be
 * found for specific files.
 */
pub struct config {
    pub good_length: u16,
    /* reduce lazy search above this match length */
    pub max_lazy: u16,
    /* do not perform lazy search above this match length */
    pub nice_length: u16,
    /* quit search above this match length */
    pub max_chain: u16,
    pub func: compress_func,
}
/*      good lazy nice chain */
/* 0 */
/* store only */
/* 1 */
/* max speed, no lazy matches */
pub static mut configuration_table: [config; 10] =
    [
     /*      good lazy nice chain */
     /* 0 */
     config{_0: 0, _1: 0, _2: 0, _3: 0, _4: deflate_stored,}, /* store only */
     /* 1 */
     config{_0: 4,
            _1: 4,
            _2: 8,
            _3: 4,
            _4: deflate_fast,}, /* max speed, no lazy matches */
     /* 2 */
     config{_0: 4, _1: 5, _2: 16, _3: 8, _4: deflate_fast,},
     /* 3 */
     config{_0: 4, _1: 6, _2: 32, _3: 32, _4: deflate_fast,},
     /* 4 */
     config{_0: 4,
            _1: 4,
            _2: 16,
            _3: 16,
            _4: deflate_slow,}, /* lazy matches */
     /* 5 */
     config{_0: 8, _1: 16, _2: 32, _3: 32, _4: deflate_slow,},
     /* 6 */
     config{_0: 8, _1: 16, _2: 128, _3: 128, _4: deflate_slow,},
     /* 7 */
     config{_0: 8, _1: 32, _2: 128, _3: 256, _4: deflate_slow,},
     /* 8 */
     config{_0: 32, _1: 128, _2: 258, _3: 1024, _4: deflate_slow,},
     /* 9 */
     config{_0: 32, _1: 258, _2: 258, _3: 4096, _4: deflate_slow,}];
pub static NIL: c_uint =
     /* max compression */
    /* Note: the deflate() code requires max_lazy >= MIN_MATCH and max_chain >= 4
     * For deflate_fast() (levels <= 3) good is ignored and lazy has a different
     * meaning.
     */
    /* rank Z_BLOCK between Z_NO_FLUSH and Z_PARTIAL_FLUSH */
    /* ===========================================================================
     * Update a hash value with the given input byte
     * IN  assertion: all calls to UPDATE_HASH are made with consecutive input
     *    characters, so that a running hash key can be computed from the previous
     *    key instead of complete recalculation each time.
     */
    /* ===========================================================================
     * Insert string str in the dictionary and set match_head to the previous head
     * of the hash chain (the most recent string with same hash key). Return
     * the previous length of the hash chain.
     * If this file is compiled with -DFASTEST, the compression level is forced
     * to 1, and no hash chains are maintained.
     * IN  assertion: all calls to INSERT_STRING are made with consecutive input
     *    characters and the first MIN_MATCH bytes of str are valid (except for
     *    the last MIN_MATCH-1 bytes of the input file).
     */
    /* ===========================================================================
     * Initialize the hash table (avoiding 64K overflow for 16 bit systems).
     * prev[] will be initialized on the fly.
     */
    /* ===========================================================================
     * Slide the hash table when sliding the window down (could be avoided with 32
     * bit values at the expense of memory usage). We slide even when level == 0 to
     * keep the hash table consistent if we switch back to level > 0 later.
     */
    0;
unsafe fn slide_hash(mut s: &mut deflate_state) {
    let mut m: c_uint;
    let mut wsize = s.w_size;
    let mut n = s.hash_size;
    let mut p = &mut s.head[n];
    loop  {
        m = *{ p = p.offset(-1); p };
        *p = if m >= wsize { m - wsize } else { NIL } as Pos;
        if { n -= 1; n } == 0 { break  };
    }
    n = wsize;
    p = &mut s.prev[n];
    loop  {
        m = *{ p = p.offset(-1); p };
        *p = if m >= wsize { m - wsize } else { NIL } as Pos;
        /* If n is not on any hash chain, prev[n] is garbage but
                 * its value will never be used.
                 */
        if { n -= 1; n } == 0 { break  };
    };
}
/* ========================================================================= */

#[no_mangle]
pub unsafe extern "C" fn deflateInit_(mut strm: z_streamp, mut level: c_int,
                                      mut version: *const i8,
                                      mut stream_size: c_int) -> c_int {
    return deflateInit2_(strm, level, Z_DEFLATED, MAX_WBITS, DEF_MEM_LEVEL,
                         Z_DEFAULT_STRATEGY, version,
                         stream_size); /* To do: ignore strm->next_in if we use it as window */
}
/* ========================================================================= */
pub static ZLIB_VERSION: [i8; 7] = "1.2.11";
/* We overlay pending_buf and d_buf+l_buf. This works since the average
     * output size for (length,distance) codes is <= 24 bits.
     */
pub static Z_NULL: *const i8 = ptr::null_mut();
pub static Z_VERSION_ERROR: c_int = -6;
pub static Z_STREAM_ERROR: c_int = -2;
pub static Z_DEFAULT_COMPRESSION: c_int = -1;
/* suppress zlib wrapper */
/* write gzip wrapper instead */
pub static MAX_MEM_LEVEL: c_int = 9;
pub static Z_FIXED: c_int = 4;
/* until 256-byte window bug fixed */
pub static Z_MEM_ERROR: c_int = -4;
pub static INIT_STATE: c_int = 42;
/* to pass state test in deflateReset() */
pub static MIN_MATCH: c_uint = 3;
/* nothing written to s->window yet */
/* 16K elements by default */
pub static FINISH_STATE: c_int = 666;
#[no_mangle]
pub unsafe extern "C" fn deflateInit2_(mut strm: z_streamp, mut level: c_int,
                                       mut method: c_int,
                                       mut windowBits: c_int,
                                       mut memLevel: c_int,
                                       mut strategy: c_int,
                                       mut version: *const i8,
                                       mut stream_size: c_int) -> c_int {
    let mut wrap = 1;
    let mut my_version = ZLIB_VERSION;
    if version == Z_NULL || *version.offset(0) != my_version[0] ||
           stream_size != std::mem::size_of::<z_stream>() {
        return Z_VERSION_ERROR;
    }
    if strm == Z_NULL { return Z_STREAM_ERROR; }
    strm.msg = Z_NULL;
    if strm.zalloc == (0 as alloc_func) {
        strm.zalloc = zcalloc;
        strm.opaque = 0 as voidpf;
    }
    if strm.zfree == (0 as free_func) { strm.zfree = zcfree; }
    if level == Z_DEFAULT_COMPRESSION { level = 6; }
    if windowBits < 0 {
        wrap = 0;
        windowBits = -windowBits;
    } else if windowBits > 15 { wrap = 2; windowBits -= 16; }
    if memLevel < 1 || memLevel > MAX_MEM_LEVEL || method != Z_DEFLATED ||
           windowBits < 8 || windowBits > 15 || level < 0 || level > 9 ||
           strategy < 0 || strategy > Z_FIXED ||
           (windowBits == 8 && wrap != 1) {
        return Z_STREAM_ERROR;
    }
    if windowBits == 8 { windowBits = 9; }
    let mut s =
        (*((strm).zalloc))((strm).opaque, 1,
                           std::mem::size_of::<deflate_state>()) as
            *mut deflate_state;
    if s == Z_NULL { return Z_MEM_ERROR; }
    strm.state = s as *mut internal_state;
    s.strm = strm;
    s.status = INIT_STATE;
    s.wrap = wrap;
    s.gzhead = Z_NULL;
    s.w_bits = windowBits as uInt;
    s.w_size = 1 << s.w_bits;
    s.w_mask = s.w_size - 1;
    s.hash_bits = (memLevel as uInt) + 7;
    s.hash_size = 1 << s.hash_bits;
    s.hash_mask = s.hash_size - 1;
    s.hash_shift = ((s.hash_bits + MIN_MATCH - 1) / MIN_MATCH);
    s.window =
        (*((strm).zalloc))((strm).opaque, (s.w_size),
                           (2 * std::mem::size_of::<Byte>())) as *mut Bytef;
    s.prev =
        (*((strm).zalloc))((strm).opaque, (s.w_size),
                           std::mem::size_of::<Pos>()) as *mut Posf;
    s.head =
        (*((strm).zalloc))((strm).opaque, (s.hash_size),
                           std::mem::size_of::<Pos>()) as *mut Posf;
    s.high_water = 0;
    s.lit_bufsize = 1 << (memLevel + 6);
    let mut overlay =
        (*((strm).zalloc))((strm).opaque, (s.lit_bufsize),
                           (std::mem::size_of::<ush>() + 2)) as *mut ushf;
    s.pending_buf = overlay as *mut uchf;
    s.pending_buf_size =
        (s.lit_bufsize as ulg) * (std::mem::size_of::<ush>() + 2);
    if s.window == Z_NULL || s.prev == Z_NULL || s.head == Z_NULL ||
           s.pending_buf == Z_NULL {
        s.status = FINISH_STATE;
        strm.msg = z_errmsg[2 - (Z_MEM_ERROR)];
        deflateEnd(strm);
        return Z_MEM_ERROR;
    }
    s.d_buf = overlay.offset(s.lit_bufsize / std::mem::size_of::<ush>());
    s.l_buf =
        s.pending_buf.offset((1 + std::mem::size_of::<ush>()) *
                                 s.lit_bufsize);
    s.level = level;
    s.strategy = strategy;
    s.method = method as Byte;
    return deflateReset(strm);
}
/* =========================================================================
 * Check for a valid deflate stream state. Return 0 if ok, 1 if not.
 */
pub static GZIP_STATE: c_int = 57;
pub static EXTRA_STATE: c_int = 69;
pub static NAME_STATE: c_int = 73;
pub static COMMENT_STATE: c_int = 91;
pub static HCRC_STATE: c_int = 103;
pub static BUSY_STATE: c_int = 113;
unsafe fn deflateStateCheck(mut strm: z_streamp) -> c_int {
    if strm == Z_NULL || strm.zalloc == (0 as alloc_func) ||
           strm.zfree == (0 as free_func) {
        return 1;
    }
    let mut s = strm.state;
    if s == Z_NULL || s.strm != strm ||
           (s.status != INIT_STATE && s.status != GZIP_STATE &&
                s.status != EXTRA_STATE && s.status != NAME_STATE &&
                s.status != COMMENT_STATE && s.status != HCRC_STATE &&
                s.status != BUSY_STATE && s.status != FINISH_STATE) {
        return 1;
    }
    return 0;
}
/* ========================================================================= */
/* when using zlib wrappers, compute Adler-32 for provided dictionary */
/* avoid computing Adler-32 in read_buf */
/* if dictionary would fill window, just replace the history */
/* already empty otherwise */
/* use the tail */
/* insert dictionary into window and hash */
pub static Z_OK: c_int = 0;
#[no_mangle]
pub unsafe extern "C" fn deflateSetDictionary(mut strm: z_streamp,
                                              mut dictionary: *const Bytef,
                                              mut dictLength: uInt) -> c_int {
    let mut str: uInt;
    let mut n: uInt;
    if deflateStateCheck(strm) != 0 || dictionary == Z_NULL {
        return Z_STREAM_ERROR;
    }
    let mut s = strm.state;
    let mut wrap = s.wrap;
    if wrap == 2 || (wrap == 1 && s.status != INIT_STATE) || s.lookahead != 0
       {
        return Z_STREAM_ERROR;
    }
    if wrap == 1 { strm.adler = adler32(strm.adler, dictionary, dictLength); }
    s.wrap = 0;
    if dictLength >= s.w_size {
        if wrap == 0 {
            *s.head.offset(s.hash_size - 1) = 0;
            memset(s.head as *mut Bytef as *mut _, 0,
                   ((s.hash_size - 1) as c_uint) *
                       std::mem::size_of::<Posf>());
            s.strstart = 0;
            s.block_start = 0;
            s.insert = 0;
        }
        dictionary = dictionary.offset(dictLength - s.w_size);
        dictLength = s.w_size;
    }
    let mut avail = strm.avail_in;
    let mut next = strm.next_in;
    strm.avail_in = dictLength;
    strm.next_in = dictionary as *mut Bytef;
    fill_window(s);
    while s.lookahead >= MIN_MATCH {
        str = s.strstart;
        n = s.lookahead - (MIN_MATCH - 1);
        loop  {
            (s.ins_h =
                 (((s.ins_h) << s.hash_shift) ^
                      (*s.window.offset(str + MIN_MATCH - 1))) & s.hash_mask);
            *s.prev.offset(str & s.w_mask) = *s.head.offset(s.ins_h);
            *s.head.offset(s.ins_h) = str as Pos;
            str += 1;
            if { n -= 1; n } == 0 { break  };
        }
        s.strstart = str;
        s.lookahead = MIN_MATCH - 1;
        fill_window(s);
    }
    s.strstart += s.lookahead;
    s.block_start = s.strstart as c_long;
    s.insert = s.lookahead;
    s.lookahead = 0;
    s.match_length = { s.prev_length = MIN_MATCH - 1; s.prev_length };
    s.match_available = 0;
    strm.next_in = next;
    strm.avail_in = avail;
    s.wrap = wrap;
    return Z_OK;
}
/* ========================================================================= */
#[no_mangle]
pub unsafe extern "C" fn deflateGetDictionary(mut strm: z_streamp,
                                              mut dictionary: *mut Bytef,
                                              mut dictLength: &mut uInt)
 -> c_int {
    if deflateStateCheck(strm) != 0 { return Z_STREAM_ERROR; }
    let mut s = strm.state;
    let mut len = s.strstart + s.lookahead;
    if len > s.w_size { len = s.w_size; }
    if dictionary != Z_NULL && len != 0 {
        memcpy(dictionary as *mut _,
               s.window.offset(s.strstart).offset(s.lookahead).offset(-len),
               len);
    }
    if dictLength != Z_NULL { *dictLength = len; }
    return Z_OK;
}
/* ========================================================================= */
/* use zfree if we ever allocate msg dynamically */
pub static Z_UNKNOWN: c_int = 2;
/* was made negative by deflate(..., Z_FINISH); */
//pub static Z_NO_FLUSH: c_int = 0;
#[no_mangle]
pub unsafe extern "C" fn deflateResetKeep(mut strm: z_streamp) -> c_int {
    if deflateStateCheck(strm) != 0 { return Z_STREAM_ERROR; }
    strm.total_in = { strm.total_out = 0; strm.total_out };
    strm.msg = Z_NULL;
    strm.data_type = Z_UNKNOWN;
    let mut s = strm.state as *mut deflate_state;
    s.pending = 0;
    s.pending_out = s.pending_buf;
    if s.wrap < 0 { s.wrap = -s.wrap; }
    s.status =
        if s.wrap == 2 {
            GZIP_STATE
        } else if s.wrap != 0 { INIT_STATE } else { BUSY_STATE };
    strm.adler =
        if s.wrap == 2 { crc32(0, Z_NULL, 0) } else { adler32(0, Z_NULL, 0) };
    s.last_flush = Z_NO_FLUSH;
    _tr_init(s);
    return Z_OK;
}
/* ========================================================================= */
#[no_mangle]
pub unsafe extern "C" fn deflateReset(mut strm: z_streamp) -> c_int {
    let mut ret = deflateResetKeep(strm);
    if ret == Z_OK { lm_init(strm.state); }
    return ret;
}
/* ========================================================================= */
#[no_mangle]
pub unsafe extern "C" fn deflateSetHeader(mut strm: z_streamp,
                                          mut head: gz_headerp) -> c_int {
    if deflateStateCheck(strm) != 0 || strm.state.wrap != 2 {
        return Z_STREAM_ERROR;
    }
    strm.state.gzhead = head;
    return Z_OK;
}
/* ========================================================================= */
#[no_mangle]
pub unsafe extern "C" fn deflatePending(mut strm: z_streamp,
                                        mut pending: &mut c_uint,
                                        mut bits: &mut c_int) -> c_int {
    if deflateStateCheck(strm) != 0 { return Z_STREAM_ERROR; }
    if pending != Z_NULL { *pending = strm.state.pending; }
    if bits != Z_NULL { *bits = strm.state.bi_valid; }
    return Z_OK;
}
/* ========================================================================= */
pub static Buf_size: c_int = 16;
pub static Z_BUF_ERROR: c_int = -5;
#[no_mangle]
pub unsafe extern "C" fn deflatePrime(mut strm: z_streamp, mut bits: c_int,
                                      mut value: c_int) -> c_int {
    let mut put;
    if deflateStateCheck(strm) != 0 { return Z_STREAM_ERROR; }
    let mut s = strm.state;
    if ((s.d_buf) as *mut Bytef) < s.pending_out.offset(((Buf_size + 7) >> 3))
       {
        return Z_BUF_ERROR;
    }
    loop  {
        put = Buf_size - s.bi_valid;
        if put > bits { put = bits; }
        s.bi_buf |= ((value & ((1 << put) - 1)) << s.bi_valid) as ush;
        s.bi_valid += put;
        _tr_flush_bits(s);
        value >>= put;
        bits -= put;
        if bits == 0 { break  };
    }
    return Z_OK;
}
/* ========================================================================= */
/* Flush the last buffer: */
pub static Z_BLOCK: c_int = 5;
#[no_mangle]
pub unsafe extern "C" fn deflateParams(mut strm: z_streamp, mut level: c_int,
                                       mut strategy: c_int) -> c_int {
    if deflateStateCheck(strm) != 0 { return Z_STREAM_ERROR; }
    let mut s = strm.state;
    if level == Z_DEFAULT_COMPRESSION { level = 6; }
    if level < 0 || level > 9 || strategy < 0 || strategy > Z_FIXED {
        return Z_STREAM_ERROR;
    }
    let mut func = (configuration_table[s.level]).func;
    if (strategy != s.strategy || func != (configuration_table[level]).func)
           && s.high_water != 0 {
        let mut err = deflate(strm, Z_BLOCK);
        if err == Z_STREAM_ERROR { return err; }
        if strm.avail_out == 0 { return Z_BUF_ERROR; };
    }
    if s.level != level {
        if s.level == 0 && s.matches != 0 {
            if s.matches == 1 {
                slide_hash(s);
            } else { *s.head.offset(s.hash_size - 1) = 0; }
            memset(s.head as *mut Bytef as *mut _, 0,
                   ((s.hash_size - 1) as c_uint) *
                       std::mem::size_of::<Posf>());
            s.matches = 0;
        }
        s.level = level;
        s.max_lazy_match = (configuration_table[level]).max_lazy;
        s.good_match = (configuration_table[level]).good_length;
        s.nice_match = (configuration_table[level]).nice_length;
        s.max_chain_length = (configuration_table[level]).max_chain;
    }
    s.strategy = strategy;
    return Z_OK;
}
/* ========================================================================= */
#[no_mangle]
pub unsafe extern "C" fn deflateTune(mut strm: z_streamp,
                                     mut good_length: c_int,
                                     mut max_lazy: c_int,
                                     mut nice_length: c_int,
                                     mut max_chain: c_int) -> c_int {
    if deflateStateCheck(strm) != 0 { return Z_STREAM_ERROR; }
    let mut s = strm.state;
    s.good_match = good_length as uInt;
    s.max_lazy_match = max_lazy as uInt;
    s.nice_match = nice_length;
    s.max_chain_length = max_chain as uInt;
    return Z_OK;
}
/* =========================================================================
 * For the default windowBits of 15 and memLevel of 8, this function returns
 * a close to exact, as well as small, upper bound on the compressed size.
 * They are coded as constants here for a reason--if the #define's are
 * changed, then this function needs to be changed as well.  The return
 * value for 15 and 8 only works for those exact settings.
 *
 * For any setting other than those defaults for windowBits and memLevel,
 * the value returned is a conservative worst case for the maximum expansion
 * resulting from using fixed blocks instead of stored blocks, which deflate
 * can emit on compressed data for some combinations of the parameters.
 *
 * This function could be more sophisticated to provide closer upper bounds for
 * every combination of windowBits and memLevel.  But even the conservative
 * upper bound of about 14% expansion does not seem onerous for output buffer
 * allocation.
 */
#[no_mangle]
pub unsafe extern "C" fn deflateBound(mut strm: z_streamp,
                                      mut sourceLen: uLong) -> uLong {
    let mut wraplen: uLong; /* conservative upper bound for compressed data */
    let mut complen =
        sourceLen + ((sourceLen + 7) >> 3) + ((sourceLen + 63) >> 6) +
            5; /* if can't get parameters, return conservative bound plus zlib wrapper */
    if deflateStateCheck(strm) != 0 {
        return complen + 6; /* compute wrapper length */
    } /* raw deflate */
    let mut s = strm.state; /* zlib wrapper */
    match s.wrap {
        0 => wraplen = 0,
        1 => wraplen = 6 + if s.strstart != 0 { 4 } else { 0 },
        2 => { /* gzip wrapper */
            wraplen = 18; /* user-supplied gzip header */
            if s.gzhead != Z_NULL {
                if s.gzhead.extra != Z_NULL {
                    wraplen +=
                        2 + s.gzhead.extra_len; /* for compiler happiness */
                } /* if not default parameters, return conservative bound */
                let mut str =
                    s.gzhead.name; /* default settings: return tight bound for that case */
                if str != Z_NULL {
                    loop  {
                        wraplen += 1;
                        if *{ let mut _t = str; str = str.offset(1); _t } == 0
                           {
                            break
                        };
                    };
                }
                str = s.gzhead.comment;
                if str != Z_NULL {
                    loop  {
                        wraplen += 1;
                        if *{ let mut _t = str; str = str.offset(1); _t } == 0
                           {
                            break
                        };
                    };
                }
                if s.gzhead.hcrc != 0 { wraplen += 2; };
            }
        }
        _ => wraplen = 6,
    }
    if s.w_bits != 15 || s.hash_bits != 8 + 7 { return complen + wraplen; }
    return sourceLen + (sourceLen >> 12) + (sourceLen >> 14) +
               (sourceLen >> 25) + 13 - 6 + wraplen;
}
/* =========================================================================
 * Put a short in the pending buffer. The 16-bit value is put in MSB order.
 * IN assertion: the stream state is correct and there is enough room in
 * pending_buf.
 */
unsafe fn putShortMSB(mut s: &mut deflate_state, mut b: uInt) {
    {
        s.pending_buf[{ let mut _t = s.pending; s.pending += 1; _t }] =
            ((b >> 8) as Byte) as Bytef;
    }
    {
        s.pending_buf[{ let mut _t = s.pending; s.pending += 1; _t }] =
            ((b & 255) as Byte) as Bytef;
    };
}
/* =========================================================================
 * Flush as much pending output as possible. All deflate() output, except for
 * some deflate_stored() output, goes through this function so some
 * applications may wish to modify it to avoid allocating a large
 * strm->next_out buffer and copying into it. (See also read_buf()).
 */
unsafe fn flush_pending(mut strm: z_streamp) {
    let mut s = strm.state;
    _tr_flush_bits(s);
    let mut len = s.pending;
    if len > strm.avail_out { len = strm.avail_out; }
    if len == 0 { return; }
    memcpy(strm.next_out as *mut _, s.pending_out as *const _, len);
    strm.next_out = strm.next_out.offset(len);
    s.pending_out = s.pending_out.offset(len);
    strm.total_out += len;
    strm.avail_out -= len;
    s.pending -= len;
    if s.pending == 0 { s.pending_out = s.pending_buf; };
}
/* ===========================================================================
 * Update the header CRC with the bytes s->pending_buf[beg..s->pending - 1].
 */
/* ========================================================================= */
/* value of flush param for previous deflate call */
//pub static Z_FINISH: c_int = 4;
/* Flush as much pending output as possible */
/* Since avail_out is 0, deflate will be called again with
             * more output space, but possibly with both pending and
             * avail_in equal to zero. There won't be anything to do,
             * but this is not an error situation so make sure we
             * return OK instead of BUF_ERROR at next call of deflate:
             */
/* Make sure there is something to do and avoid duplicate consecutive
     * flushes. For repeated and useless calls with Z_FINISH, we keep
     * returning Z_STREAM_END instead of Z_BUF_ERROR.
     */
/* User must not provide more input after the first FINISH: */
/* Write the header */
/* zlib header */
pub static Z_HUFFMAN_ONLY: c_int = 2;
pub static PRESET_DICT: c_uint = 32;
/* Save the adler32 of the preset dictionary: */
/* Compression must start with an empty pending buffer */
/* gzip header */
pub static OS_CODE: c_int = 19;
/* Compression must start with an empty pending buffer */
/* start of bytes to update crc */
/* start of bytes to update crc */
/* start of bytes to update crc */
/* Compression must start with an empty pending buffer */
/* Start a new block or continue the current one.
     */
pub static Z_RLE: c_int = 3;
/* avoid BUF_ERROR next call, see above */
/* If flush != Z_NO_FLUSH && avail_out == 0, the next call
             * of deflate should use the same flush parameter to make sure
             * that the flush is complete. So we don't have to output an
             * empty block here, this will be done at next call. This also
             * ensures that for a very small output buffer, we emit at most
             * one empty block.
             */
//pub static Z_PARTIAL_FLUSH: c_int = 1;
/* FULL_FLUSH or SYNC_FLUSH */
/* For a full flush, this empty block will be recognized
                 * as a special marker by inflate_sync().
                 */
//pub static Z_FULL_FLUSH: c_int = 3;
/* forget history */
/* avoid BUF_ERROR at next call, see above */
pub static Z_STREAM_END: c_int = 1;
#[no_mangle]
pub unsafe extern "C" fn deflate(mut strm: z_streamp, mut flush: c_int)
 -> c_int {
    if deflateStateCheck(strm) != 0 || flush > Z_BLOCK || flush < 0 {
        return Z_STREAM_ERROR; /* Write the trailer */
    }
    let mut s = strm.state;
    if strm.next_out == Z_NULL ||
           (strm.avail_in != 0 && strm.next_in == Z_NULL) ||
           (s.status == FINISH_STATE && flush != Z_FINISH) {
        return { strm.msg = z_errmsg[2 - (Z_STREAM_ERROR)]; (-2) };
    }
    if strm.avail_out == 0 {
        return { strm.msg = z_errmsg[2 - (Z_BUF_ERROR)]; (-5) };
    }
    let mut old_flush = s.last_flush;
    s.last_flush = flush;
    if s.pending != 0 {
        flush_pending(strm);
        if strm.avail_out == 0 { s.last_flush = -1; return Z_OK; };
    } else if strm.avail_in == 0 &&
                  (((flush) * 2) - if (flush) > 4 { 9 } else { 0 }) <=
                      (((old_flush) * 2) -
                           if (old_flush) > 4 { 9 } else { 0 }) &&
                  flush != Z_FINISH {
        return { strm.msg = z_errmsg[2 - (Z_BUF_ERROR)]; (-5) };
    }
    if s.status == FINISH_STATE && strm.avail_in != 0 {
        return { strm.msg = z_errmsg[2 - (Z_BUF_ERROR)]; (-5) };
    }
    if s.status == INIT_STATE {
        let mut header = (Z_DEFLATED + ((s.w_bits - 8) << 4)) << 8;
        let mut level_flags: uInt;
        if s.strategy >= Z_HUFFMAN_ONLY || s.level < 2 {
            level_flags = 0;
        } else if s.level < 6 {
            level_flags = 1;
        } else if s.level == 6 { level_flags = 2; } else { level_flags = 3; }
        header |= (level_flags << 6);
        if s.strstart != 0 { header |= PRESET_DICT; }
        header += 31 - (header % 31);
        putShortMSB(s, header);
        if s.strstart != 0 {
            putShortMSB(s, (strm.adler >> 16) as uInt);
            putShortMSB(s, (strm.adler & 65535) as uInt);
        }
        strm.adler = adler32(0, Z_NULL, 0);
        s.status = BUSY_STATE;
        flush_pending(strm);
        if s.pending != 0 { s.last_flush = -1; return Z_OK; };
    }
    if s.status == GZIP_STATE {
        strm.adler = crc32(0, Z_NULL, 0);
        {
            *s.pending_buf.offset({
                                      let mut _t = s.pending;
                                      s.pending += 1;
                                      _t
                                  }) = 31 as Bytef;
        }
        {
            *s.pending_buf.offset({
                                      let mut _t = s.pending;
                                      s.pending += 1;
                                      _t
                                  }) = 139 as Bytef;
        }
        {
            *s.pending_buf.offset({
                                      let mut _t = s.pending;
                                      s.pending += 1;
                                      _t
                                  }) = 8 as Bytef;
        }
        if s.gzhead == Z_NULL {
            {
                *s.pending_buf.offset({
                                          let mut _t = s.pending;
                                          s.pending += 1;
                                          _t
                                      }) = 0 as Bytef;
            }
            {
                *s.pending_buf.offset({
                                          let mut _t = s.pending;
                                          s.pending += 1;
                                          _t
                                      }) = 0 as Bytef;
            }
            {
                *s.pending_buf.offset({
                                          let mut _t = s.pending;
                                          s.pending += 1;
                                          _t
                                      }) = 0 as Bytef;
            }
            {
                *s.pending_buf.offset({
                                          let mut _t = s.pending;
                                          s.pending += 1;
                                          _t
                                      }) = 0 as Bytef;
            }
            {
                *s.pending_buf.offset({
                                          let mut _t = s.pending;
                                          s.pending += 1;
                                          _t
                                      }) = 0 as Bytef;
            }
            {
                *s.pending_buf.offset({
                                          let mut _t = s.pending;
                                          s.pending += 1;
                                          _t
                                      }) =
                    if s.level == 9 {
                        2
                    } else if s.strategy >= Z_HUFFMAN_ONLY || s.level < 2 {
                        4
                    } else { 0 } as Bytef;
            }
            {
                *s.pending_buf.offset({
                                          let mut _t = s.pending;
                                          s.pending += 1;
                                          _t
                                      }) = (OS_CODE) as Bytef;
            }
            s.status = BUSY_STATE;
            flush_pending(strm);
            if s.pending != 0 { s.last_flush = -1; return Z_OK; };
        } else {
            {
                *s.pending_buf.offset({
                                          let mut _t = s.pending;
                                          s.pending += 1;
                                          _t
                                      }) =
                    (if s.gzhead.text != 0 { 1 } else { 0 } +
                         if s.gzhead.hcrc != 0 { 2 } else { 0 } +
                         if s.gzhead.extra == Z_NULL { 0 } else { 4 } +
                         if s.gzhead.name == Z_NULL { 0 } else { 8 } +
                         if s.gzhead.comment == Z_NULL { 0 } else { 16 }) as
                        Bytef;
            }
            {
                *s.pending_buf.offset({
                                          let mut _t = s.pending;
                                          s.pending += 1;
                                          _t
                                      }) =
                    ((s.gzhead.time & 255) as Byte) as Bytef;
            }
            {
                *s.pending_buf.offset({
                                          let mut _t = s.pending;
                                          s.pending += 1;
                                          _t
                                      }) =
                    (((s.gzhead.time >> 8) & 255) as Byte) as Bytef;
            }
            {
                *s.pending_buf.offset({
                                          let mut _t = s.pending;
                                          s.pending += 1;
                                          _t
                                      }) =
                    (((s.gzhead.time >> 16) & 255) as Byte) as Bytef;
            }
            {
                *s.pending_buf.offset({
                                          let mut _t = s.pending;
                                          s.pending += 1;
                                          _t
                                      }) =
                    (((s.gzhead.time >> 24) & 255) as Byte) as Bytef;
            }
            {
                *s.pending_buf.offset({
                                          let mut _t = s.pending;
                                          s.pending += 1;
                                          _t
                                      }) =
                    if s.level == 9 {
                        2
                    } else if s.strategy >= Z_HUFFMAN_ONLY || s.level < 2 {
                        4
                    } else { 0 } as Bytef;
            }
            {
                *s.pending_buf.offset({
                                          let mut _t = s.pending;
                                          s.pending += 1;
                                          _t
                                      }) = (s.gzhead.os & 255) as Bytef;
            }
            if s.gzhead.extra != Z_NULL {
                {
                    *s.pending_buf.offset({
                                              let mut _t = s.pending;
                                              s.pending += 1;
                                              _t
                                          }) =
                        (s.gzhead.extra_len & 255) as Bytef;
                }
                {
                    *s.pending_buf.offset({
                                              let mut _t = s.pending;
                                              s.pending += 1;
                                              _t
                                          }) =
                        ((s.gzhead.extra_len >> 8) & 255) as Bytef;
                };
            }
            if s.gzhead.hcrc != 0 {
                strm.adler = crc32(strm.adler, s.pending_buf, s.pending);
            }
            s.gzindex = 0;
            s.status = EXTRA_STATE;
        };
    }
    if s.status == EXTRA_STATE {
        if s.gzhead.extra != Z_NULL {
            let mut beg = s.pending;
            let mut left = (s.gzhead.extra_len & 65535) - s.gzindex;
            while s.pending + left > s.pending_buf_size {
                let mut copy = s.pending_buf_size - s.pending;
                memcpy(s.pending_buf.offset(s.pending),
                       s.gzhead.extra.offset(s.gzindex), copy);
                s.pending = s.pending_buf_size;
                loop  {
                    if s.gzhead.hcrc != 0 && s.pending > (beg) {
                        strm.adler =
                            crc32(strm.adler, s.pending_buf.offset((beg)),
                                  s.pending - (beg));
                    }
                    if 0 == 0 { break  };
                }
                s.gzindex += copy;
                flush_pending(strm);
                if s.pending != 0 { s.last_flush = -1; return Z_OK; }
                beg = 0;
                left -= copy;
            }
            memcpy(s.pending_buf.offset(s.pending),
                   s.gzhead.extra.offset(s.gzindex), left);
            s.pending += left;
            loop  {
                if s.gzhead.hcrc != 0 && s.pending > (beg) {
                    strm.adler =
                        crc32(strm.adler, s.pending_buf.offset((beg)),
                              s.pending - (beg));
                }
                if 0 == 0 { break  };
            }
            s.gzindex = 0;
        }
        s.status = NAME_STATE;
    }
    if s.status == NAME_STATE {
        if s.gzhead.name != Z_NULL {
            let mut beg = s.pending;
            let mut val;
            loop  {
                if s.pending == s.pending_buf_size {
                    loop  {
                        if s.gzhead.hcrc != 0 && s.pending > (beg) {
                            strm.adler =
                                crc32(strm.adler, s.pending_buf.offset((beg)),
                                      s.pending - (beg));
                        }
                        if 0 == 0 { break  };
                    }
                    flush_pending(strm);
                    if s.pending != 0 { s.last_flush = -1; return Z_OK; }
                    beg = 0;
                }
                val =
                    *s.gzhead.name.offset({
                                              let mut _t = s.gzindex;
                                              s.gzindex += 1;
                                              _t
                                          });
                {
                    *s.pending_buf.offset({
                                              let mut _t = s.pending;
                                              s.pending += 1;
                                              _t
                                          }) = (val) as Bytef;
                }
                if val == 0 { break  };
            }
            loop  {
                if s.gzhead.hcrc != 0 && s.pending > (beg) {
                    strm.adler =
                        crc32(strm.adler, s.pending_buf.offset((beg)),
                              s.pending - (beg));
                }
                if 0 == 0 { break  };
            }
            s.gzindex = 0;
        }
        s.status = COMMENT_STATE;
    }
    if s.status == COMMENT_STATE {
        if s.gzhead.comment != Z_NULL {
            let mut beg = s.pending;
            let mut val;
            loop  {
                if s.pending == s.pending_buf_size {
                    loop  {
                        if s.gzhead.hcrc != 0 && s.pending > (beg) {
                            strm.adler =
                                crc32(strm.adler, s.pending_buf.offset((beg)),
                                      s.pending - (beg));
                        }
                        if 0 == 0 { break  };
                    }
                    flush_pending(strm);
                    if s.pending != 0 { s.last_flush = -1; return Z_OK; }
                    beg = 0;
                }
                val =
                    *s.gzhead.comment.offset({
                                                 let mut _t = s.gzindex;
                                                 s.gzindex += 1;
                                                 _t
                                             });
                {
                    *s.pending_buf.offset({
                                              let mut _t = s.pending;
                                              s.pending += 1;
                                              _t
                                          }) = (val) as Bytef;
                }
                if val == 0 { break  };
            }
            loop  {
                if s.gzhead.hcrc != 0 && s.pending > (beg) {
                    strm.adler =
                        crc32(strm.adler, s.pending_buf.offset((beg)),
                              s.pending - (beg));
                }
                if 0 == 0 { break  };
            };
        }
        s.status = HCRC_STATE;
    }
    if s.status == HCRC_STATE {
        if s.gzhead.hcrc != 0 {
            if s.pending + 2 > s.pending_buf_size {
                flush_pending(strm);
                if s.pending != 0 { s.last_flush = -1; return Z_OK; };
            }
            {
                *s.pending_buf.offset({
                                          let mut _t = s.pending;
                                          s.pending += 1;
                                          _t
                                      }) =
                    ((strm.adler & 255) as Byte) as Bytef;
            }
            {
                *s.pending_buf.offset({
                                          let mut _t = s.pending;
                                          s.pending += 1;
                                          _t
                                      }) =
                    (((strm.adler >> 8) & 255) as Byte) as Bytef;
            }
            strm.adler = crc32(0, Z_NULL, 0);
        }
        s.status = BUSY_STATE;
        flush_pending(strm);
        if s.pending != 0 { s.last_flush = -1; return Z_OK; };
    }
    if strm.avail_in != 0 || s.lookahead != 0 ||
           (flush != Z_NO_FLUSH && s.status != FINISH_STATE) {
        let mut bstate =
            if s.level == 0 {
                deflate_stored(s, flush)
            } else if s.strategy == Z_HUFFMAN_ONLY {
                deflate_huff(s, flush)
            } else if s.strategy == Z_RLE {
                deflate_rle(s, flush)
            } else { (*((configuration_table[s.level]).func))(s, flush) };
        if bstate == finish_started || bstate == finish_done {
            s.status = FINISH_STATE;
        }
        if bstate == need_more || bstate == finish_started {
            if strm.avail_out == 0 { s.last_flush = -1; }
            return Z_OK;
        }
        if bstate == block_done {
            if flush == Z_PARTIAL_FLUSH {
                _tr_align(s);
            } else if flush != Z_BLOCK {
                _tr_stored_block(s, ptr::null_mut() as *mut i8, 0, 0);
                if flush == Z_FULL_FLUSH {
                    *s.head.offset(s.hash_size - 1) = 0;
                    memset(s.head as *mut Bytef as *mut _, 0,
                           ((s.hash_size - 1) as c_uint) *
                               std::mem::size_of::<Posf>());
                    if s.lookahead == 0 {
                        s.strstart = 0;
                        s.block_start = 0;
                        s.insert = 0;
                    };
                };
            }
            flush_pending(strm);
            if strm.avail_out == 0 { s.last_flush = -1; return Z_OK; };
        };
    }
    if flush != Z_FINISH { return Z_OK; }
    if s.wrap <= 0 { return Z_STREAM_END; }
    if s.wrap == 2 {
        {
            *s.pending_buf.offset({
                                      let mut _t = s.pending;
                                      s.pending += 1;
                                      _t
                                  }) = ((strm.adler & 255) as Byte) as Bytef;
        }
        {
            *s.pending_buf.offset({
                                      let mut _t = s.pending;
                                      s.pending += 1;
                                      _t
                                  }) =
                (((strm.adler >> 8) & 255) as Byte) as Bytef;
        }
        {
            *s.pending_buf.offset({
                                      let mut _t = s.pending;
                                      s.pending += 1;
                                      _t
                                  }) =
                (((strm.adler >> 16) & 255) as Byte) as Bytef;
        }
        {
            *s.pending_buf.offset({
                                      let mut _t = s.pending;
                                      s.pending += 1;
                                      _t
                                  }) =
                (((strm.adler >> 24) & 255) as Byte) as Bytef;
        }
        {
            *s.pending_buf.offset({
                                      let mut _t = s.pending;
                                      s.pending += 1;
                                      _t
                                  }) =
                ((strm.total_in & 255) as Byte) as Bytef;
        }
        {
            *s.pending_buf.offset({
                                      let mut _t = s.pending;
                                      s.pending += 1;
                                      _t
                                  }) =
                (((strm.total_in >> 8) & 255) as Byte) as Bytef;
        }
        {
            *s.pending_buf.offset({
                                      let mut _t = s.pending;
                                      s.pending += 1;
                                      _t
                                  }) =
                (((strm.total_in >> 16) & 255) as Byte) as Bytef;
        }
        {
            *s.pending_buf.offset({
                                      let mut _t = s.pending;
                                      s.pending += 1;
                                      _t
                                  }) =
                (((strm.total_in >> 24) & 255) as Byte) as Bytef;
        };
    } else {
        putShortMSB(s, (strm.adler >> 16) as uInt);
        putShortMSB(s, (strm.adler & 65535) as uInt);
    }
    flush_pending(strm);
    /* If avail_out is zero, the application will call deflate again
         * to flush the rest.
         */
    if s.wrap > 0 {
        s.wrap = -s.wrap; /* write the trailer only once! */
    }
    return if s.pending != 0 { Z_OK } else { Z_STREAM_END };
}
/* ========================================================================= */
/* Deallocate in reverse order of allocations: */
pub static Z_DATA_ERROR: c_int = -3;
#[no_mangle]
pub unsafe extern "C" fn deflateEnd(mut strm: z_streamp) -> c_int {
    if deflateStateCheck(strm) != 0 { return Z_STREAM_ERROR; }
    let mut status = strm.state.status;
    {
        if !strm.state.pending_buf.is_null() {
            (*((strm).zfree))((strm).opaque,
                              (strm.state.pending_buf) as voidpf);
        };
    }
    {
        if !strm.state.head.is_null() {
            (*((strm).zfree))((strm).opaque, (strm.state.head) as voidpf);
        };
    }
    {
        if !strm.state.prev.is_null() {
            (*((strm).zfree))((strm).opaque, (strm.state.prev) as voidpf);
        };
    }
    {
        if !strm.state.window.is_null() {
            (*((strm).zfree))((strm).opaque, (strm.state.window) as voidpf);
        };
    }
    (*((strm).zfree))((strm).opaque, (strm.state) as voidpf);
    strm.state = Z_NULL;
    return if status == BUSY_STATE { Z_DATA_ERROR } else { Z_OK };
}
/* =========================================================================
 * Copy the source state to the destination state.
 * To simplify the source, this is not supported for 16-bit MSDOS (which
 * doesn't have enough memory anyway to duplicate compression states).
 */
#[no_mangle]
pub unsafe extern "C" fn deflateCopy(mut dest: z_streamp,
                                     mut source: z_streamp) -> c_int {
    if deflateStateCheck(source) != 0 || dest == Z_NULL {
        return Z_STREAM_ERROR; /* following zmemcpy do not work for 16-bit MSDOS */
    } /* MAXSEG_64K */
    let mut ss = source.state;
    memcpy(dest as voidpf, source as voidpf, std::mem::size_of::<z_stream>());
    let mut ds =
        (*((dest).zalloc))((dest).opaque, 1,
                           std::mem::size_of::<deflate_state>()) as
            *mut deflate_state;
    if ds == Z_NULL { return Z_MEM_ERROR; }
    dest.state = ds as *mut internal_state;
    memcpy(ds as voidpf, ss as voidpf, std::mem::size_of::<deflate_state>());
    ds.strm = dest;
    ds.window =
        (*((dest).zalloc))((dest).opaque, (ds.w_size),
                           (2 * std::mem::size_of::<Byte>())) as *mut Bytef;
    ds.prev =
        (*((dest).zalloc))((dest).opaque, (ds.w_size),
                           std::mem::size_of::<Pos>()) as *mut Posf;
    ds.head =
        (*((dest).zalloc))((dest).opaque, (ds.hash_size),
                           std::mem::size_of::<Pos>()) as *mut Posf;
    let mut overlay =
        (*((dest).zalloc))((dest).opaque, (ds.lit_bufsize),
                           (std::mem::size_of::<ush>() + 2)) as *mut ushf;
    ds.pending_buf = overlay as *mut uchf;
    if ds.window == Z_NULL || ds.prev == Z_NULL || ds.head == Z_NULL ||
           ds.pending_buf == Z_NULL {
        deflateEnd(dest);
        return Z_MEM_ERROR;
    }
    memcpy(ds.window as *mut _, ss.window as *const _,
           ds.w_size * 2 * std::mem::size_of::<Byte>());
    memcpy(ds.prev as voidpf, ss.prev as voidpf,
           ds.w_size * std::mem::size_of::<Pos>());
    memcpy(ds.head as voidpf, ss.head as voidpf,
           ds.hash_size * std::mem::size_of::<Pos>());
    memcpy(ds.pending_buf as *mut _, ss.pending_buf as *const _,
           ds.pending_buf_size as uInt);
    ds.pending_out =
        ds.pending_buf.offset(ss.pending_out.offset(-ss.pending_buf));
    ds.d_buf = overlay.offset(ds.lit_bufsize / std::mem::size_of::<ush>());
    ds.l_buf =
        ds.pending_buf.offset((1 + std::mem::size_of::<ush>()) *
                                  ds.lit_bufsize);
    ds.l_desc.dyn_tree = ds.dyn_ltree;
    ds.d_desc.dyn_tree = ds.dyn_dtree;
    ds.bl_desc.dyn_tree = ds.bl_tree;
    return Z_OK;
}
/* ===========================================================================
 * Read a new buffer from the current input stream, update the adler32
 * and total number of bytes read.  All deflate() input goes through
 * this function so some applications may wish to modify it to avoid
 * allocating a large strm->next_in buffer and copying from it.
 * (See also flush_pending()).
 */
unsafe fn read_buf(mut strm: z_streamp, mut buf: *mut Bytef, mut size: c_uint)
 -> c_uint {
    let mut len = strm.avail_in;
    if len > size { len = size; }
    if len == 0 { return 0; }
    strm.avail_in -= len;
    memcpy(buf as *mut _, strm.next_in as *const _, len);
    if strm.state.wrap == 1 {
        strm.adler = adler32(strm.adler, buf, len);
    } else if strm.state.wrap == 2 {
        strm.adler = crc32(strm.adler, buf, len);
    }
    strm.next_in = strm.next_in.offset(len);
    strm.total_in += len;
    return len;
}
/* ===========================================================================
 * Initialize the "longest match" routines for a new zlib stream
 */
unsafe fn lm_init(mut s: &mut deflate_state) {
    s.window_size = (2 as ulg) * s.w_size;
    s.head[s.hash_size - 1] = 0;
    memset(s.head as *mut Bytef as *mut _, 0,
           ((s.hash_size - 1) as c_uint) * std::mem::size_of::<Posf>());
    /* Set the default configuration parameters:
         */
    s.max_lazy_match =
        (configuration_table[s.level]).max_lazy; /* initialize the asm code */
    s.good_match = (configuration_table[s.level]).good_length;
    s.nice_match = (configuration_table[s.level]).nice_length;
    s.max_chain_length = (configuration_table[s.level]).max_chain;
    s.strstart = 0;
    s.block_start = 0;
    s.lookahead = 0;
    s.insert = 0;
    s.match_length = { s.prev_length = MIN_MATCH - 1; s.prev_length };
    s.match_available = 0;
    s.ins_h = 0;
}
/* ===========================================================================
 * Set match_start to the longest match starting at the given string and
 * return its length. Matches shorter or equal to prev_length are discarded,
 * in which case the result is equal to prev_length and match_start is
 * garbage.
 * IN assertions: cur_match is the head of the hash chain for the current
 *   string (strstart) and its distance is <= MAX_DIST, and prev_length >= 1
 * OUT assertion: the match length is not greater than s->lookahead.
 */
/* For 80x86 and 680x0, an optimized version will be provided in match.asm or
 * match.S. The code will be functionally equivalent.
 */
/* current match */
/* max hash chain length */
/* current string */
/* matched string */
/* length of current match */
/* best match length so far */
/* stop if match long enough */
/* Stop when cur_match becomes <= limit. To simplify the code,
     * we prevent matches with the string of window index 0.
     */
/* Compare two bytes at a time. Note: this is not always beneficial.
     * Try with and without -DUNALIGNED_OK to check.
     */
pub static MAX_MATCH: c_int = 258;
unsafe fn longest_match(mut s: &mut deflate_state, mut cur_match: IPos)
 -> uInt {
    let mut chain_length = s.max_chain_length;
    let mut scan = s.window.offset(s.strstart);
    let mut match_: *mut Bytef;
    let mut len;
    let mut best_len = s.prev_length as c_int;
    let mut nice_match = s.nice_match;
    let mut limit =
        if s.strstart > (((s).w_size - (258 + 3 + 1)) as IPos) {
            s.strstart - (((s).w_size - (258 + 3 + 1)) as IPos)
        } else { NIL };
    let mut prev = s.prev;
    let mut wmask = s.w_mask;
    let mut strend = s.window.offset(s.strstart).offset(MAX_MATCH);
    let mut scan_end1 = scan[best_len - 1];
    let mut scan_end = scan[best_len];
    /* The code is optimized for HASH_BITS >= 8 and MAX_MATCH-2 multiple of 16.
         * It is easy to get rid of this optimization if necessary.
         */
    /* Do not waste too much time if we already have a good match: */
    if s.prev_length >= s.good_match { chain_length >>= 2; }
    /* Do not look for matches beyond the end of the input. This is necessary
         * to make deflate deterministic.
         */
    if (nice_match as uInt) > s.lookahead {
        nice_match = s.lookahead as c_int;
    }
    loop  {
        match_ = s.window.offset(cur_match);
        /* Skip to next match if the match length cannot increase
                 * or if the match length is less than 2.  Note that the checks below
                 * for insufficient lookahead only occur occasionally for performance
                 * reasons.  Therefore uninitialized memory will be accessed, and
                 * conditional jumps will be made that depend on those values.
                 * However the length of the match is limited to the lookahead, so
                 * the output of deflate is not affected by the uninitialized values.
                 */
        /* This code assumes sizeof(unsigned short) == 2. Do not use
                 * UNALIGNED_OK if your compiler uses a different size.
                 */
        /* It is not necessary to compare scan[2] and match[2] since they are
                 * always equal when the other bytes match, given that the hash keys
                 * are equal and that HASH_BITS >= 8. Compare 2 bytes at a time at
                 * strstart+3, +5, ... up to strstart+257. We check for insufficient
                 * lookahead only every 4th comparison; the 128th check will be made
                 * at strstart+257. If MAX_MATCH-2 is not a multiple of 8, it is
                 * necessary to put more guard bytes at the end of the window, or
                 * to check more often for insufficient lookahead.
                 */
        /* The funny "do {}" generates better code on most compilers */
        /* Here, scan <= window+strstart+257 */
        /* UNALIGNED_OK */
        if match_[best_len] != scan_end || match_[best_len - 1] != scan_end1
               || *match_ != *scan ||
               *{ match_ = match_.offset(1); match_ } != scan[1] {
            continue ;
        }
        /* The check at best_len-1 can be removed because it will be made
                 * again later. (This heuristic is not always a win.)
                 * It is not necessary to compare scan[2] and match[2] since they
                 * are always equal when the other bytes match, given that
                 * the hash keys are equal and that HASH_BITS >= 8.
                 */
        { scan = scan.offset(2); match_ = match_.offset(1) }
        /* We check for insufficient lookahead only every 8th comparison;
                 * the 256th check will be made at strstart+258.
                 */
        loop  {
            if !(*{
                      scan = scan.offset(1); /* UNALIGNED_OK */
                      scan
                  } ==
                     *{
                          match_ = match_.offset(1); /* ASMV */
                          match_
                      } &&
                     *{
                          scan = scan.offset(1); /* FASTEST */
                          scan
                      } == *{ match_ = match_.offset(1); match_ } &&
                     *{ scan = scan.offset(1); scan } ==
                         *{ match_ = match_.offset(1); match_ } &&
                     *{ scan = scan.offset(1); scan } ==
                         *{ match_ = match_.offset(1); match_ } &&
                     *{ scan = scan.offset(1); scan } ==
                         *{ match_ = match_.offset(1); match_ } &&
                     *{ scan = scan.offset(1); scan } ==
                         *{ match_ = match_.offset(1); match_ } &&
                     *{ scan = scan.offset(1); scan } ==
                         *{ match_ = match_.offset(1); match_ } &&
                     *{ scan = scan.offset(1); scan } ==
                         *{ match_ = match_.offset(1); match_ } &&
                     scan < strend) {
                break
            };
        }
        len = MAX_MATCH - (strend.offset(-scan) as c_int);
        scan = strend.offset(-MAX_MATCH);
        if len > best_len {
            s.match_start = cur_match;
            best_len = len;
            if len >= nice_match { break ; }
            scan_end1 = scan[best_len - 1];
            scan_end = scan[best_len];
        }
        if !({ cur_match = prev[cur_match & wmask]; cur_match } > limit &&
                 { chain_length -= 1; chain_length } != 0) {
            break
        };
    }
    if (best_len as uInt) <= s.lookahead { return best_len as uInt; }
    return s.lookahead;
}
/* ---------------------------------------------------------------------------
 * Optimized version for FASTEST only
 */
/* current match */
/* current string */
/* matched string */
/* length of current match */
/* The code is optimized for HASH_BITS >= 8 and MAX_MATCH-2 multiple of 16.
     * It is easy to get rid of this optimization if necessary.
     */
/* Return failure if the match length is less than 2:
     */
/* The check at best_len-1 can be removed because it will be made
     * again later. (This heuristic is not always a win.)
     * It is not necessary to compare scan[2] and match[2] since they
     * are always equal when the other bytes match, given that
     * the hash keys are equal and that HASH_BITS >= 8.
     */
/* We check for insufficient lookahead only every 8th comparison;
     * the 256th check will be made at strstart+258.
     */
/* FASTEST */
/* result of memcmp for equal strings */
/* ===========================================================================
 * Check that the match at match_start is indeed a match.
 */
/* check that the match is indeed a match */
/* ZLIB_DEBUG */
/* ===========================================================================
 * Fill the window when the lookahead becomes insufficient.
 * Updates strstart and lookahead.
 *
 * IN assertion: lookahead < MIN_LOOKAHEAD
 * OUT assertions: strstart <= window_size-MIN_LOOKAHEAD
 *    At least one byte has been read, or avail_in == 0; reads are
 *    performed for at least two bytes (required for the zip translate_eol
 *    option -- not supported here).
 */
/* Amount of free space at the end of the window. */
/* Deal with !@#$% 64K limit: */
/* Very unlikely, but possible on 16 bit machine if
                 * strstart == 0 && lookahead == 1 (input done a byte at time)
                 */
/* If the window is almost full and there is insufficient lookahead,
         * move the upper half to the lower one to make room in the upper half.
         */
/* we now have strstart >= MAX_DIST */
/* If there was no sliding:
         *    strstart <= WSIZE+MAX_DIST-1 && lookahead <= MIN_LOOKAHEAD - 1 &&
         *    more == window_size - lookahead - strstart
         * => more >= window_size - (MIN_LOOKAHEAD-1 + WSIZE + MAX_DIST-1)
         * => more >= window_size - 2*WSIZE + 2
         * In the BIG_MEM or MMAP case (not yet supported),
         *   window_size == input_size + MIN_LOOKAHEAD  &&
         *   strstart + s->lookahead <= input_size => more >= MIN_LOOKAHEAD.
         * Otherwise, window_size == 2*WSIZE so more >= 2.
         * If there was sliding, more >= WSIZE. So in all cases, more >= 2.
         */
/* Initialize the hash value now that we have some input: */
/* If the whole input has less than MIN_MATCH bytes, ins_h is garbage,
         * but this is not important since only literal bytes will be emitted.
         */
pub static MIN_LOOKAHEAD: c_uint = (258 + 3 + 1);
/* If the WIN_INIT bytes after the end of the current data have never been
     * written, then zero those bytes in order to avoid memory check reports of
     * the use of uninitialized (or uninitialised as Julian writes) bytes by
     * the longest match routines.  Update the high water mark for the next
     * time through here.  WIN_INIT is set to MAX_MATCH since the longest match
     * routines allow scanning to strstart + MAX_MATCH, ignoring lookahead.
     */
/* Previous high water mark below current data -- zero WIN_INIT
             * bytes or up to end of window, whichever is less.
             */
pub static WIN_INIT: c_long = 258;
unsafe fn fill_window(mut s: &mut deflate_state) {
    let mut n: c_uint;
    let mut more: c_uint;
    let mut wsize = s.w_size;
    loop  {
        more =
            (s.window_size - (s.lookahead as ulg) - (s.strstart as ulg)) as
                c_uint;
        if std::mem::size_of::<c_int>() <= 2 {
            if more == 0 && s.strstart == 0 && s.lookahead == 0 {
                more = wsize;
            } else if more == ((-1) as c_uint) { more -= 1; };
        }
        if s.strstart >= wsize + ((s).w_size - (258 + 3 + 1)) {
            memcpy(s.window as *mut _, s.window.offset(wsize),
                   (wsize as c_uint) - more);
            s.match_start -= wsize;
            s.strstart -= wsize;
            s.block_start -= wsize as c_long;
            slide_hash(s);
            more += wsize;
        }
        if s.strm.avail_in == 0 { break ; }
        n =
            read_buf(s.strm, s.window.offset(s.strstart).offset(s.lookahead),
                     more);
        s.lookahead += n;
        if s.lookahead + s.insert >= MIN_MATCH {
            let mut str = s.strstart - s.insert;
            s.ins_h = s.window[str];
            (s.ins_h =
                 (((s.ins_h) << s.hash_shift) ^ (s.window[str + 1])) &
                     s.hash_mask);
            while s.insert != 0 {
                (s.ins_h =
                     (((s.ins_h) << s.hash_shift) ^
                          (s.window[str + MIN_MATCH - 1])) & s.hash_mask);
                s.prev[str & s.w_mask] = s.head[s.ins_h];
                s.head[s.ins_h] = str as Pos;
                str += 1;
                s.insert -= 1;
                if s.lookahead + s.insert < MIN_MATCH { break ; };
            };
        }
        if !(s.lookahead < MIN_LOOKAHEAD && s.strm.avail_in != 0) { break  };
    }
    if s.high_water < s.window_size {
        let mut curr = s.strstart + ((s.lookahead) as ulg);
        let mut init: ulg;
        if s.high_water < curr {
            init = s.window_size - curr;
            if init > WIN_INIT { init = WIN_INIT; }
            memset(s.window.offset(curr), 0, init as c_uint);
            s.high_water = curr + init;
        } else if s.high_water < curr + WIN_INIT {
            /* High water mark at or above current data, but below current data
                         * plus WIN_INIT -- zero out to current data plus WIN_INIT, or up
                         * to end of window, whichever is less.
                         */
            init = curr + WIN_INIT - s.high_water;
            if init > s.window_size - s.high_water {
                init = s.window_size - s.high_water;
            }
            memset(s.window.offset(s.high_water), 0, init as c_uint);
            s.high_water += init;
        };
    };
}
/* ===========================================================================
 * Flush the current block, with given end-of-file flag.
 * IN assertion: strstart is set to the end of the current match.
 */
/* Same but force premature exit if necessary. */
/* Maximum stored block length in deflate format (not including header). */
pub static MAX_STORED: c_uint =

    /* Minimum of a and b. */
    /* ===========================================================================
     * Copy without compression as much as possible from the input stream, return
     * the current block state.
     *
     * In case deflateParams() is used to later switch to a non-zero compression
     * level, s->matches (otherwise unused when storing) keeps track of the number
     * of hash table slides to perform. If s->matches is 1, then one hash table
     * slide will be done when switching. If s->matches is 2, the maximum value
     * allowed here, then the hash table will be cleared, since two or more slides
     * is the same as a clear.
     *
     * deflate_stored() is written to minimize the number of times an input byte is
     * copied. It is most efficient with large input and output buffers, which
     * maximizes the opportunites to have a single copy from next_in to next_out.
     */
    /* Smallest worthy block size when not flushing or finishing. By default
         * this is 32K. This can be as small as 507 bytes for memLevel == 1. For
         * large input and output buffers, the stored block size will be larger.
         */
    /* Copy as many min_block or larger stored blocks directly to next_out as
         * possible. If flushing, copy the remaining available input to next_out as
         * stored blocks, if there is enough space.
         */
    /* Set len to the maximum size block that we can copy directly with the
             * available input data and output space. Set left to how much of that
             * would be copied from what's left in the window.
             */
    65535;
unsafe fn deflate_stored(mut s: &mut deflate_state, mut flush: c_int)
 -> block_state {
    let mut min_block =
        if (s.pending_buf_size - 5) > (s.w_size) {
            (s.w_size)
        } else {
            s.pending_buf_size - 5
        }; /* maximum deflate stored block length */
    let mut len: c_uint; /* number of header bytes */
    let mut left: c_uint; /* need room for header */
    let mut have:
            c_uint; /* maximum stored block length that will fit in avail_out: */
    let mut last = 0; /* bytes left in window */
    let mut used = s.strm.avail_in; /* limit len to the input */
    loop  {
        len = MAX_STORED; /* limit len to the output */
        have = (s.bi_valid + 42) >> 3;
        if s.strm.avail_out < have { break ; }
        have = s.strm.avail_out - have;
        left = s.strstart - s.block_start;
        if len > (left as ulg) + s.strm.avail_in {
            len = left + s.strm.avail_in;
        }
        if len > have { len = have; }
        /* If the stored block would be less than min_block in length, or if
                 * unable to copy all of the available input when flushing, then try
                 * copying to the window and the pending buffer instead. Also don't
                 * write an empty block when flushing -- deflate() does that.
                 */
        if len < min_block &&
               ((len == 0 && flush != Z_FINISH) || flush == Z_NO_FLUSH ||
                    len != left + s.strm.avail_in) {
            break ;
        }
        /* Make a dummy stored block in pending to get the header bytes,
                 * including any pending bits. This also updates the debugging counts.
                 */
        last =
            if flush == Z_FINISH && len == left + s.strm.avail_in {
                1
            } else {
                0
            }; /* Replace the lengths in the dummy stored block with len. */
        _tr_stored_block(s, ptr::null_mut() as *mut i8, 0,
                         last); /* Write the stored block header bytes. */
        s.pending_buf[s.pending - 4] =
            len; /* Update debugging counts for the data about to be copied. */
        s.pending_buf[s.pending - 3] =
            len >>
                8; /* Copy uncompressed bytes from the window to next_out. */
        s.pending_buf[s.pending - 2] = !len;
        s.pending_buf[s.pending - 1] = !len >> 8;
        flush_pending(s.strm);
        if left != 0 {
            if left > len { left = len; }
            memcpy(s.strm.next_out as *mut _, s.window.offset(s.block_start),
                   left);
            s.strm.next_out = s.strm.next_out.offset(left);
            s.strm.avail_out -= left;
            s.strm.total_out += left;
            s.block_start += left;
            len -= left;
        }
        /* Copy uncompressed bytes directly from next_in to next_out, updating
                 * the check value.
                 */
        if len != 0 {
            read_buf(s.strm, s.strm.next_out, len);
            s.strm.next_out = s.strm.next_out.offset(len);
            s.strm.avail_out -= len;
            s.strm.total_out += len;
        }
        if last != 0 { break  };
    }
    /* Update the sliding window with the last s->w_size bytes of the copied
         * data, or append all of the copied data to the existing window if less
         * than s->w_size bytes were copied. Also update the number of bytes to
         * insert in the hash tables, in the event that deflateParams() switches to
         * a non-zero compression level.
         */
    used -= s.strm.avail_in; /* number of input bytes directly copied */
    if used != 0 {
        /* If any input was used, then no unused input remains in the window,
                 * therefore s->block_start == s->strstart.
                 */
        if used >= s.w_size { /* supplant the previous history */
            s.matches = 2; /* clear hash */
            memcpy(s.window as *mut _, s.strm.next_in.offset(-s.w_size),
                   s.w_size); /* Slide the window down. */
            s.strstart = s.w_size; /* add a pending slide_hash() */
        } else {
            if s.window_size - s.strstart <= used {
                s.strstart -=
                    s.w_size; /* If the last block was written to next_out, then done. */
                memcpy(s.window as *mut _, s.window.offset(s.w_size),
                       s.strstart); /* If flushing and all input has been consumed, then done. */
                if s.matches < 2 {
                    s.matches +=
                        1; /* Fill the window with any remaining input. */
                }; /* Slide the window down. */
            } /* add a pending slide_hash() */
            memcpy(s.window.offset(s.strstart), s.strm.next_in.offset(-used),
                   used); /* more space now */
            s.strstart += used;
        }
        s.block_start = s.strstart;
        s.insert +=
            if (used) > (s.w_size - s.insert) {
                s.w_size - s.insert
            } else { (used) };
    }
    if s.high_water < s.strstart { s.high_water = s.strstart; }
    if last != 0 { return finish_done; }
    if flush != Z_NO_FLUSH && flush != Z_FINISH && s.strm.avail_in == 0 &&
           (s.strstart as c_long) == s.block_start {
        return block_done;
    }
    have = s.window_size - s.strstart - 1;
    if s.strm.avail_in > have && s.block_start >= (s.w_size as c_long) {
        s.block_start -= s.w_size;
        s.strstart -= s.w_size;
        memcpy(s.window as *mut _, s.window.offset(s.w_size), s.strstart);
        if s.matches < 2 { s.matches += 1; }
        have += s.w_size;
    }
    if have > s.strm.avail_in { have = s.strm.avail_in; }
    if have != 0 {
        read_buf(s.strm, s.window.offset(s.strstart), have);
        s.strstart += have;
    }
    if s.high_water < s.strstart { s.high_water = s.strstart; }
    /* There was not enough avail_out to write a complete worthy or flushed
         * stored block to next_out. Write a stored block to pending instead, if we
         * have enough input for a worthy block, or if flushing and there is enough
         * room for the remaining input as a stored block in the pending buffer.
         */
    have = (s.bi_valid + 42) >> 3; /* number of header bytes */
    /* maximum stored block length that will fit in pending: */
    have =
        if (s.pending_buf_size - have) > (MAX_STORED) {
            65535
        } else {
            s.pending_buf_size - have
        }; /* We've done all we can with the available input and output. */
    min_block = if (have) > (s.w_size) { (s.w_size) } else { (have) };
    left = s.strstart - s.block_start;
    if left >= min_block ||
           ((left != 0 || flush == Z_FINISH) && flush != Z_NO_FLUSH &&
                s.strm.avail_in == 0 && left <= have) {
        len = if (left) > (have) { (have) } else { (left) };
        last =
            if flush == Z_FINISH && s.strm.avail_in == 0 && len == left {
                1
            } else { 0 };
        _tr_stored_block(s, (s.window as *mut charf).offset(s.block_start),
                         len, last);
        s.block_start += len;
        flush_pending(s.strm);
    }
    return if last != 0 { finish_started } else { need_more };
}
/* ===========================================================================
 * Compress as much as possible from the input stream, return the current
 * block state.
 * This function does not perform lazy evaluation of matches and inserts
 * new strings in the dictionary only for unmatched strings or for short
 * matches. It is used only for the fast compression options.
 */
unsafe fn deflate_fast(mut s: &mut deflate_state, mut flush: c_int)
 -> block_state { /* head of the hash chain */
    let mut bflush; /* set if current block must be flushed */
    let mut hash_head: IPos;
    loop  {
        /* Make sure that we always have enough lookahead, except
                 * at the end of the input file. We need MAX_MATCH bytes
                 * for the next match, plus MIN_MATCH bytes to insert the
                 * string following the next match.
                 */
        if s.lookahead < MIN_LOOKAHEAD {
            fill_window(s); /* flush the current block */
            if s.lookahead < MIN_LOOKAHEAD && flush == Z_NO_FLUSH {
                return need_more;
            }
            if s.lookahead == 0 { break ; };
        }
        /* Insert the string window[strstart .. strstart+2] in the
                 * dictionary, and set hash_head to the head of the hash chain:
                 */
        hash_head = NIL;
        if s.lookahead >= MIN_MATCH {
            {
                (s.ins_h =
                     (((s.ins_h) << s.hash_shift) ^
                          (s.window[(s.strstart) + (3 - 1)])) & s.hash_mask);
                hash_head =
                    {
                        s.prev[(s.strstart) & s.w_mask] = s.head[s.ins_h];
                        s.prev[(s.strstart) & s.w_mask]
                    };
                s.head[s.ins_h] = (s.strstart) as Pos
            };
        }
        /* Find the longest match, discarding those <= prev_length.
                 * At this point we have always match_length < MIN_MATCH
                 */
        if hash_head != NIL &&
               s.strstart - hash_head <= ((s).w_size - (258 + 3 + 1)) {
            /* To simplify the code, we prevent matches with the string
                         * of window index 0 (in particular we have to avoid a match
                         * of the string with itself at the start of the input file).
                         */
            s.match_length =
                longest_match(s,
                              hash_head); /* longest_match() sets match_start */
        }
        if s.match_length >= MIN_MATCH {
            {
                let mut len = (s.match_length - MIN_MATCH) as uch;
                let mut dist = (s.strstart - s.match_start) as ush;
                s.d_buf[s.last_lit] = dist;
                s.l_buf[{ let mut _t = s.last_lit; s.last_lit += 1; _t }] =
                    len;
                dist -= 1;
                (s.dyn_ltree[_length_code[len] + 256 + 1]).fc.freq += 1;
                (s.dyn_dtree[if (dist) < 256 {
                                 _dist_code[dist]
                             } else {
                                 _dist_code[256 + ((dist) >> 7)]
                             }]).fc.freq += 1;
                bflush = (s.last_lit == s.lit_bufsize - 1);
            }
            s.lookahead -= s.match_length;
            /* Insert new strings in the hash table only if the match length
                         * is not too large. This saves time but degrades compression.
                         */
            if s.match_length <= s.max_lazy_match && s.lookahead >= MIN_MATCH
               {
                s.match_length -= 1; /* string at strstart already in table */
                loop  {
                    s.strstart += 1;
                    {
                        (s.ins_h =
                             (((s.ins_h) << s.hash_shift) ^
                                  (s.window[(s.strstart) + (3 - 1)])) &
                                 s.hash_mask);
                        hash_head =
                            {
                                s.prev[(s.strstart) & s.w_mask] =
                                    s.head[s.ins_h];
                                s.prev[(s.strstart) & s.w_mask]
                            };
                        s.head[s.ins_h] = (s.strstart) as Pos
                    }
                    /* strstart never exceeds WSIZE-MAX_MATCH, so there are
                                         * always MIN_MATCH bytes ahead.
                                         */
                    if { s.match_length -= 1; s.match_length } == 0 {
                        break
                    };
                }
                s.strstart += 1;
            } else {
                s.strstart += s.match_length;
                s.match_length = 0;
                s.ins_h = s.window[s.strstart];
                (s.ins_h =
                     (((s.ins_h) << s.hash_shift) ^
                          (s.window[s.strstart + 1])) & s.hash_mask);
                /* If lookahead < MIN_MATCH, ins_h is garbage, but it does not
                                 * matter since it will be recomputed at next deflate call.
                                 */
            }; /* No match, output a literal byte */
        } else {
            {
                let mut cc = (s.window[s.strstart]);
                s.d_buf[s.last_lit] = 0;
                s.l_buf[{ let mut _t = s.last_lit; s.last_lit += 1; _t }] =
                    cc;
                (s.dyn_ltree[cc]).fc.freq += 1;
                bflush = (s.last_lit == s.lit_bufsize - 1);
            }
            s.lookahead -= 1;
            s.strstart += 1;
        }
        if bflush != 0 {
            {
                _tr_flush_block(s,
                                if s.block_start >= 0 {
                                    &mut s.window[s.block_start as c_uint] as
                                        *mut charf
                                } else { ptr::null_mut() as *mut charf },
                                ((s.strstart as c_long) - s.block_start) as
                                    ulg, 0);
                s.block_start = s.strstart;
                flush_pending(s.strm);
            }
            if s.strm.avail_out == 0 {
                return if 0 != 0 { finish_started } else { need_more };
            };
        };
    }
    s.insert =
        if s.strstart < MIN_MATCH - 1 { s.strstart } else { MIN_MATCH - 1 };
    if flush == Z_FINISH {
        {
            {
                _tr_flush_block(s,
                                if s.block_start >= 0 {
                                    &mut s.window[s.block_start as c_uint] as
                                        *mut charf
                                } else { ptr::null_mut() as *mut charf },
                                ((s.strstart as c_long) - s.block_start) as
                                    ulg, 1);
                s.block_start = s.strstart;
                flush_pending(s.strm);
            }
            if s.strm.avail_out == 0 {
                return if 1 != 0 { finish_started } else { need_more };
            };
        }
        return finish_done;
    }
    if s.last_lit != 0 {
        {
            _tr_flush_block(s,
                            if s.block_start >= 0 {
                                &mut s.window[s.block_start as c_uint] as
                                    *mut charf
                            } else { ptr::null_mut() as *mut charf },
                            ((s.strstart as c_long) - s.block_start) as ulg,
                            0);
            s.block_start = s.strstart;
            flush_pending(s.strm);
        }
        if s.strm.avail_out == 0 {
            return if 0 != 0 { finish_started } else { need_more };
        };
    }
    return block_done;
}
/* ===========================================================================
 * Same as above, but achieves better compression. We use a lazy
 * evaluation for matches: a match is finally adopted only if there is
 * no better match at the next window position.
 */
/* head of hash chain */
/* set if current block must be flushed */
/* Process the input block. */
/* Make sure that we always have enough lookahead, except
         * at the end of the input file. We need MAX_MATCH bytes
         * for the next match, plus MIN_MATCH bytes to insert the
         * string following the next match.
         */
/* flush the current block */
/* Insert the string window[strstart .. strstart+2] in the
         * dictionary, and set hash_head to the head of the hash chain:
         */
/* Find the longest match, discarding those <= prev_length.
         */
/* To simplify the code, we prevent matches with the string
             * of window index 0 (in particular we have to avoid a match
             * of the string with itself at the start of the input file).
             */
/* longest_match() sets match_start */
pub static Z_FILTERED: c_int = 1;
pub static TOO_FAR: c_uint = 4096;
unsafe fn deflate_slow(mut s: &mut deflate_state, mut flush: c_int)
 -> block_state {
    let mut bflush;
    let mut hash_head: IPos;
    loop  {
        if s.lookahead < MIN_LOOKAHEAD {
            fill_window(s);
            if s.lookahead < MIN_LOOKAHEAD && flush == Z_NO_FLUSH {
                return need_more;
            }
            if s.lookahead == 0 { break ; };
        }
        hash_head = NIL;
        if s.lookahead >= MIN_MATCH {
            {
                (s.ins_h =
                     (((s.ins_h) << s.hash_shift) ^
                          (s.window[(s.strstart) + (3 - 1)])) & s.hash_mask);
                hash_head =
                    {
                        s.prev[(s.strstart) & s.w_mask] = s.head[s.ins_h];
                        s.prev[(s.strstart) & s.w_mask]
                    };
                s.head[s.ins_h] = (s.strstart) as Pos
            };
        }
        { s.prev_length = s.match_length; s.prev_match = s.match_start }
        s.match_length = MIN_MATCH - 1;
        if hash_head != NIL && s.prev_length < s.max_lazy_match &&
               s.strstart - hash_head <= ((s).w_size - (258 + 3 + 1)) {
            s.match_length = longest_match(s, hash_head);
            if s.match_length <= 5 &&
                   (s.strategy == Z_FILTERED ||
                        (s.match_length == MIN_MATCH &&
                             s.strstart - s.match_start > TOO_FAR)) {
                /* If prev_match is also MIN_MATCH, match_start is garbage
                                 * but we will ignore the current match anyway.
                                 */
                s.match_length = MIN_MATCH - 1;
            };
        }
        /* If there was a match at the previous step and the current
                 * match is not better, output the previous match:
                 */
        if s.prev_length >= MIN_MATCH && s.match_length <= s.prev_length {
            let mut max_insert =
                s.strstart + s.lookahead -
                    MIN_MATCH; /* Do not insert strings in hash table beyond this. */
            {
                let mut len = (s.prev_length - MIN_MATCH) as uch;
                let mut dist = (s.strstart - 1 - s.prev_match) as ush;
                s.d_buf[s.last_lit] = dist;
                s.l_buf[{ let mut _t = s.last_lit; s.last_lit += 1; _t }] =
                    len;
                dist -= 1;
                (s.dyn_ltree[_length_code[len] + 256 + 1]).fc.freq += 1;
                (s.dyn_dtree[if (dist) < 256 {
                                 _dist_code[dist]
                             } else {
                                 _dist_code[256 + ((dist) >> 7)]
                             }]).fc.freq += 1;
                bflush = (s.last_lit == s.lit_bufsize - 1);
            }
            /* Insert in hash table all strings up to the end of the match.
                         * strstart-1 and strstart are already inserted. If there is not
                         * enough lookahead, the last two strings are not inserted in
                         * the hash table.
                         */
            s.lookahead -= s.prev_length - 1;
            s.prev_length -= 2;
            loop  {
                if { s.strstart += 1; s.strstart } <= max_insert {
                    {
                        (s.ins_h =
                             (((s.ins_h) << s.hash_shift) ^
                                  (s.window[(s.strstart) + (3 - 1)])) &
                                 s.hash_mask);
                        hash_head =
                            {
                                s.prev[(s.strstart) & s.w_mask] =
                                    s.head[s.ins_h];
                                s.prev[(s.strstart) & s.w_mask]
                            };
                        s.head[s.ins_h] = (s.strstart) as Pos
                    };
                }
                if { s.prev_length -= 1; s.prev_length } == 0 { break  };
            }
            s.match_available = 0;
            s.match_length = MIN_MATCH - 1;
            s.strstart += 1;
            if bflush != 0 {
                {
                    _tr_flush_block(s,
                                    if s.block_start >= 0 {
                                        &mut s.window[s.block_start as c_uint]
                                            as *mut charf
                                    } else { ptr::null_mut() as *mut charf },
                                    ((s.strstart as c_long) - s.block_start)
                                        as ulg, 0);
                    s.block_start = s.strstart;
                    flush_pending(s.strm);
                }
                if s.strm.avail_out == 0 {
                    return if 0 != 0 { finish_started } else { need_more };
                };
            };
        } else if s.match_available != 0 {
            /* If there was no match at the previous position, output a
                         * single literal. If there was a match but the current match
                         * is longer, truncate the previous match to a single literal.
                         */
            {
                let mut cc = (s.window[s.strstart - 1]);
                s.d_buf[s.last_lit] = 0;
                s.l_buf[{ let mut _t = s.last_lit; s.last_lit += 1; _t }] =
                    cc;
                (s.dyn_ltree[cc]).fc.freq += 1;
                bflush = (s.last_lit == s.lit_bufsize - 1);
            }
            if bflush != 0 {
                {
                    _tr_flush_block(s,
                                    if s.block_start >= 0 {
                                        &mut s.window[s.block_start as c_uint]
                                            as *mut charf
                                    } else { ptr::null_mut() as *mut charf },
                                    ((s.strstart as c_long) - s.block_start)
                                        as ulg, 0);
                    s.block_start = s.strstart;
                    flush_pending(s.strm);
                };
            }
            s.strstart += 1;
            s.lookahead -= 1;
            if s.strm.avail_out == 0 { return need_more; };
        } else {
            /* There is no previous match to compare with, wait for
                         * the next step to decide.
                         */
            s.match_available = 1; /* FASTEST */
            s.strstart += 1;
            s.lookahead -= 1;
        };
    }
    if s.match_available != 0 {
        {
            let mut cc = (s.window[s.strstart - 1]);
            s.d_buf[s.last_lit] = 0;
            s.l_buf[{ let mut _t = s.last_lit; s.last_lit += 1; _t }] = cc;
            (s.dyn_ltree[cc]).fc.freq += 1;
            bflush = (s.last_lit == s.lit_bufsize - 1);
        }
        s.match_available = 0;
    }
    s.insert =
        if s.strstart < MIN_MATCH - 1 { s.strstart } else { MIN_MATCH - 1 };
    if flush == Z_FINISH {
        {
            {
                _tr_flush_block(s,
                                if s.block_start >= 0 {
                                    &mut s.window[s.block_start as c_uint] as
                                        *mut charf
                                } else { ptr::null_mut() as *mut charf },
                                ((s.strstart as c_long) - s.block_start) as
                                    ulg, 1);
                s.block_start = s.strstart;
                flush_pending(s.strm);
            }
            if s.strm.avail_out == 0 {
                return if 1 != 0 { finish_started } else { need_more };
            };
        }
        return finish_done;
    }
    if s.last_lit != 0 {
        {
            _tr_flush_block(s,
                            if s.block_start >= 0 {
                                &mut s.window[s.block_start as c_uint] as
                                    *mut charf
                            } else { ptr::null_mut() as *mut charf },
                            ((s.strstart as c_long) - s.block_start) as ulg,
                            0);
            s.block_start = s.strstart;
            flush_pending(s.strm);
        }
        if s.strm.avail_out == 0 {
            return if 0 != 0 { finish_started } else { need_more };
        };
    }
    return block_done;
}
/* ===========================================================================
 * For Z_RLE, simply look for runs of bytes, generate matches only of distance
 * one.  Do not maintain a hash table.  (It will be regenerated if this run of
 * deflate switches away from Z_RLE.)
 */
unsafe fn deflate_rle(mut s: &mut deflate_state, mut flush: c_int)
 -> block_state {
    let mut bflush; /* set if current block must be flushed */
    let mut prev: uInt; /* byte at distance one to match */
    let mut scan: *mut Bytef; /* scan goes up to strend for length of run */
    let mut strend: *mut Bytef;
    loop  {
        /* Make sure that we always have enough lookahead, except
                 * at the end of the input file. We need MAX_MATCH bytes
                 * for the longest run, plus one for the unrolled loop.
                 */
        if s.lookahead <= MAX_MATCH {
            fill_window(s); /* flush the current block */
            if s.lookahead <= MAX_MATCH && flush == Z_NO_FLUSH {
                return need_more; /* See how many times the previous byte repeats */
            } /* Emit match if have run of MIN_MATCH or longer, else emit literal */
            if s.lookahead == 0 {
                break ; /* No match, output a literal byte */
            };
        }
        s.match_length = 0;
        if s.lookahead >= MIN_MATCH && s.strstart > 0 {
            scan = s.window.offset(s.strstart).offset(-1);
            prev = *scan;
            if prev == *{ scan = scan.offset(1); scan } &&
                   prev == *{ scan = scan.offset(1); scan } &&
                   prev == *{ scan = scan.offset(1); scan } {
                strend = s.window.offset(s.strstart).offset(MAX_MATCH);
                loop  {
                    if !(prev == *{ scan = scan.offset(1); scan } &&
                             prev == *{ scan = scan.offset(1); scan } &&
                             prev == *{ scan = scan.offset(1); scan } &&
                             prev == *{ scan = scan.offset(1); scan } &&
                             prev == *{ scan = scan.offset(1); scan } &&
                             prev == *{ scan = scan.offset(1); scan } &&
                             prev == *{ scan = scan.offset(1); scan } &&
                             prev == *{ scan = scan.offset(1); scan } &&
                             scan < strend) {
                        break
                    };
                }
                s.match_length = MAX_MATCH - (strend.offset(-scan) as uInt);
                if s.match_length > s.lookahead {
                    s.match_length = s.lookahead;
                };
            };
        }
        if s.match_length >= MIN_MATCH {
            {
                let mut len = (s.match_length - MIN_MATCH) as uch;
                let mut dist = 1 as ush;
                s.d_buf[s.last_lit] = dist;
                s.l_buf[{ let mut _t = s.last_lit; s.last_lit += 1; _t }] =
                    len;
                dist -= 1;
                (s.dyn_ltree[_length_code[len] + 256 + 1]).fc.freq += 1;
                (s.dyn_dtree[if (dist) < 256 {
                                 _dist_code[dist]
                             } else {
                                 _dist_code[256 + ((dist) >> 7)]
                             }]).fc.freq += 1;
                bflush = (s.last_lit == s.lit_bufsize - 1);
            }
            s.lookahead -= s.match_length;
            s.strstart += s.match_length;
            s.match_length = 0;
        } else {
            {
                let mut cc = (s.window[s.strstart]);
                s.d_buf[s.last_lit] = 0;
                s.l_buf[{ let mut _t = s.last_lit; s.last_lit += 1; _t }] =
                    cc;
                (s.dyn_ltree[cc]).fc.freq += 1;
                bflush = (s.last_lit == s.lit_bufsize - 1);
            }
            s.lookahead -= 1;
            s.strstart += 1;
        }
        if bflush != 0 {
            {
                _tr_flush_block(s,
                                if s.block_start >= 0 {
                                    &mut s.window[s.block_start as c_uint] as
                                        *mut charf
                                } else { ptr::null_mut() as *mut charf },
                                ((s.strstart as c_long) - s.block_start) as
                                    ulg, 0);
                s.block_start = s.strstart;
                flush_pending(s.strm);
            }
            if s.strm.avail_out == 0 {
                return if 0 != 0 { finish_started } else { need_more };
            };
        };
    }
    s.insert = 0;
    if flush == Z_FINISH {
        {
            {
                _tr_flush_block(s,
                                if s.block_start >= 0 {
                                    &mut s.window[s.block_start as c_uint] as
                                        *mut charf
                                } else { ptr::null_mut() as *mut charf },
                                ((s.strstart as c_long) - s.block_start) as
                                    ulg, 1);
                s.block_start = s.strstart;
                flush_pending(s.strm);
            }
            if s.strm.avail_out == 0 {
                return if 1 != 0 { finish_started } else { need_more };
            };
        }
        return finish_done;
    }
    if s.last_lit != 0 {
        {
            _tr_flush_block(s,
                            if s.block_start >= 0 {
                                &mut s.window[s.block_start as c_uint] as
                                    *mut charf
                            } else { ptr::null_mut() as *mut charf },
                            ((s.strstart as c_long) - s.block_start) as ulg,
                            0);
            s.block_start = s.strstart;
            flush_pending(s.strm);
        }
        if s.strm.avail_out == 0 {
            return if 0 != 0 { finish_started } else { need_more };
        };
    }
    return block_done;
}
/* ===========================================================================
 * For Z_HUFFMAN_ONLY, do not look for matches.  Do not maintain a hash table.
 * (It will be regenerated if this run of deflate switches away from Huffman.)
 */
unsafe fn deflate_huff(mut s: &mut deflate_state, mut flush: c_int)
 -> block_state {
    let mut bflush; /* set if current block must be flushed */
    loop  { /* Make sure that we have a literal to write. */
        if s.lookahead == 0 {
            fill_window(s); /* flush the current block */
            if s.lookahead == 0 {
                if flush == Z_NO_FLUSH {
                    return need_more; /* Output a literal byte */
                }
                break ;
            };
        }
        s.match_length = 0;
        {
            let mut cc = (s.window[s.strstart]);
            s.d_buf[s.last_lit] = 0;
            s.l_buf[{ let mut _t = s.last_lit; s.last_lit += 1; _t }] = cc;
            (s.dyn_ltree[cc]).fc.freq += 1;
            bflush = (s.last_lit == s.lit_bufsize - 1);
        }
        s.lookahead -= 1;
        s.strstart += 1;
        if bflush != 0 {
            {
                _tr_flush_block(s,
                                if s.block_start >= 0 {
                                    &mut s.window[s.block_start as c_uint] as
                                        *mut charf
                                } else { ptr::null_mut() as *mut charf },
                                ((s.strstart as c_long) - s.block_start) as
                                    ulg, 0);
                s.block_start = s.strstart;
                flush_pending(s.strm);
            }
            if s.strm.avail_out == 0 {
                return if 0 != 0 { finish_started } else { need_more };
            };
        };
    }
    s.insert = 0;
    if flush == Z_FINISH {
        {
            {
                _tr_flush_block(s,
                                if s.block_start >= 0 {
                                    &mut s.window[s.block_start as c_uint] as
                                        *mut charf
                                } else { ptr::null_mut() as *mut charf },
                                ((s.strstart as c_long) - s.block_start) as
                                    ulg, 1);
                s.block_start = s.strstart;
                flush_pending(s.strm);
            }
            if s.strm.avail_out == 0 {
                return if 1 != 0 { finish_started } else { need_more };
            };
        }
        return finish_done;
    }
    if s.last_lit != 0 {
        {
            _tr_flush_block(s,
                            if s.block_start >= 0 {
                                &mut s.window[s.block_start as c_uint] as
                                    *mut charf
                            } else { ptr::null_mut() as *mut charf },
                            ((s.strstart as c_long) - s.block_start) as ulg,
                            0);
            s.block_start = s.strstart;
            flush_pending(s.strm);
        }
        if s.strm.avail_out == 0 {
            return if 0 != 0 { finish_started } else { need_more };
        };
    }
    return block_done;
}
