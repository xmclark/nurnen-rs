/* crc32.c -- compute the CRC-32 of a data stream
 * Copyright (C) 1995-2006, 2010, 2011, 2012, 2016 Mark Adler
 * For conditions of distribution and use, see copyright notice in zlib.h
 *
 * Thanks to Rodney Brown <rbrown64@csc.com.au> for his contribution of faster
 * CRC methods: exclusive-oring 32 bits of data at a time, and pre-computing
 * tables for updating the shift register in one step with three exclusive-ors
 * instead of four steps with four exclusive-ors.  This results in about a
 * factor of two increase in speed on a Power PC G4 (PPC7455) using gcc -O3.
 */
/* @(#) $Id$ */
/*
  Note on the use of DYNAMIC_CRC_TABLE: there is no mutex or semaphore
  protection on the static variables used to control the first-use generation
  of the crc tables.  Therefore, if you #define DYNAMIC_CRC_TABLE, you should
  first call get_crc_table() to initialize the tables before allowing more than
  one thread to use crc32().

  DYNAMIC_CRC_TABLE and MAKECRCH can be #defined to write out crc32.h.
 */
/* !DYNAMIC_CRC_TABLE */
/* MAKECRCH */
/* for STDC and FAR definitions */
/* Definitions for doing the crc four data bytes at a time. */
/* BYFOUR */
/* Local functions for crc concatenation */
/* MAKECRCH */
/*
  Generate tables for a byte-wise 32-bit CRC calculation on the polynomial:
  x^32+x^26+x^23+x^22+x^16+x^12+x^11+x^10+x^8+x^7+x^5+x^4+x^2+x+1.

  Polynomials over GF(2) are represented in binary, one bit per coefficient,
  with the lowest powers in the most significant bit.  Then adding polynomials
  is just exclusive-or, and multiplying a polynomial by x is a right shift by
  one.  If we call the above polynomial p, and represent a byte as the
  polynomial q, also with the lowest power in the most significant bit (so the
  byte 0xb1 is the polynomial x^7+x^3+x+1), then the CRC is (q*x^32) mod p,
  where a mod b means the remainder after dividing a by b.

  This calculation is done using the shift-register method of multiplying and
  taking the remainder.  The register is initialized to zero, and for each
  incoming bit, x^32 is added mod p to the register if the bit is a one (where
  x^32 mod p is p+x^32 = x^26+...+1), and the register is multiplied mod p by
  x (which is shifting right by one and adding x^32 mod p if the bit shifted
  out is a one).  We start with the highest power (least significant bit) of
  q and repeat for all eight bits of q.

  The first table is simply the CRC of all possible eight bit values.  This is
  all the information needed to generate CRCs on data a byte at a time for all
  combinations of CRC register values and incoming bytes.  The remaining tables
  allow for word-at-a-time CRC calculation for both big-endian and little-
  endian machines, where a word is four bytes.
*/
/* polynomial exclusive-or pattern */
/* terms of polynomial defining this crc (except x^32): */
/* flag to limit concurrent making */
/* See if another task is already doing this (not thread-safe, but better
       than nothing -- significantly reduces duration of vulnerability in
       case the advice about DYNAMIC_CRC_TABLE is ignored) */
/* make exclusive-or pattern from polynomial (0xedb88320UL) */
/* generate a crc for every 8-bit value */
/* generate crc for each value followed by one, two, and three zeros,
           and then the byte reversal of those as well as the first table */
/* BYFOUR */
/* not first */
/* wait for the other guy to finish (not efficient, but rare) */
/* write out CRC tables to crc32.h */
/* BYFOUR */
/* MAKECRCH */
/* MAKECRCH */
/* !DYNAMIC_CRC_TABLE */
/* ========================================================================
 * Tables of CRC-32s of all single-byte values, made by make_crc_table().
 */
/* DYNAMIC_CRC_TABLE */
/* =========================================================================
 * This function can be used by asm versions of crc32()
 */
#[no_mangle]
pub unsafe extern "C" fn get_crc_table()
 -> *const z_crc_t { /* DYNAMIC_CRC_TABLE */
    return crc_table as *const z_crc_t;
}
/* ========================================================================= */
/* ========================================================================= */
pub static Z_NULL: *const u8 = ptr::null_mut();
pub static DO8: c_long =
     /* DYNAMIC_CRC_TABLE */
    /* BYFOUR */
    {
        crc =
            crc_table[0][((crc as c_int) ^
                              (*{
                                    let mut _t = buf;
                                    buf = buf.offset(1);
                                    _t
                                })) & 255] ^ (crc >> 8);
        crc
    };
pub static DO1: c_long =
    {
        crc =
            crc_table[0][((crc as c_int) ^
                              (*{
                                    let mut _t = buf;
                                    buf = buf.offset(1);
                                    _t
                                })) & 255] ^ (crc >> 8);
        crc
    };
#[no_mangle]
pub unsafe extern "C" fn crc32_z(mut buf: *const u8, mut len: z_size_t)
 -> uLong {
    if buf == Z_NULL { return 0; }
    if std::mem::size_of::<*mut c_void>() == std::mem::size_of::<isize>() {
        let mut endian = 1;
        if *((&mut endian) as *mut u8) != 0 {
            return crc32_little(crc, buf, len);
        } else { return crc32_big(crc, buf, len); };
    }
    crc = crc ^ 4294967295;
    while len >= 8 {
        DO8;
        crc =
            crc_table[0][((crc as c_int) ^
                              (*{
                                    let mut _t = buf;
                                    buf = buf.offset(1);
                                    _t
                                })) & 255] ^ (crc >> 8);
        crc =
            crc_table[0][((crc as c_int) ^
                              (*{
                                    let mut _t = buf;
                                    buf = buf.offset(1);
                                    _t
                                })) & 255] ^ (crc >> 8);
        crc =
            crc_table[0][((crc as c_int) ^
                              (*{
                                    let mut _t = buf;
                                    buf = buf.offset(1);
                                    _t
                                })) & 255] ^ (crc >> 8);
        crc =
            crc_table[0][((crc as c_int) ^
                              (*{
                                    let mut _t = buf;
                                    buf = buf.offset(1);
                                    _t
                                })) & 255] ^ (crc >> 8);
        crc =
            crc_table[0][((crc as c_int) ^
                              (*{
                                    let mut _t = buf;
                                    buf = buf.offset(1);
                                    _t
                                })) & 255] ^ (crc >> 8);
        crc =
            crc_table[0][((crc as c_int) ^
                              (*{
                                    let mut _t = buf;
                                    buf = buf.offset(1);
                                    _t
                                })) & 255] ^ (crc >> 8);
        crc =
            crc_table[0][((crc as c_int) ^
                              (*{
                                    let mut _t = buf;
                                    buf = buf.offset(1);
                                    _t
                                })) & 255] ^ (crc >> 8);
        len -= 8;
    }
    if len != 0 { loop  { DO1; if { len -= 1; len } == 0 { break  }; }; }
    return crc ^ 4294967295;
}
/* ========================================================================= */
#[no_mangle]
pub unsafe extern "C" fn crc32(mut buf: *const u8, mut len: uInt) -> uLong {
    return crc32_z(crc, buf, len);
}
/*
   This BYFOUR code accesses the passed unsigned char * buffer with a 32-bit
   integer pointer type. This violates the strict aliasing rule, where a
   compiler can assume, for optimization purposes, that two pointers to
   fundamentally different types won't ever point to the same memory. This can
   manifest as a problem only if one of the pointers is written to. This code
   only reads from those pointers. So long as this code remains isolated in
   this compilation unit, there won't be a problem. For this reason, this code
   should not be copied and pasted into a compilation unit in which other code
   writes to the buffer that is passed to these routines.
 */
/* ========================================================================= */
pub static DOLIT32: z_crc_t =

    /* ========================================================================= */
    c ^= *{ let mut _t = buf4; buf4 = buf4.offset(1); _t };
pub static DOLIT4: z_crc_t =
    c ^= *{ let mut _t = buf4; buf4 = buf4.offset(1); _t };
unsafe fn crc32_little(mut crc: c_long, mut buf: *const u8, mut len: z_size_t)
 -> c_long {
    let mut c = crc as z_crc_t;
    c = !c;
    while len != 0 && ((buf as isize) & 3) != 0 {
        c =
            crc_table[0][(c ^ *{ let mut _t = buf; buf = buf.offset(1); _t })
                             & 255] ^ (c >> 8);
        len -= 1;
    }
    let mut buf4 = buf as *const c_void as *const z_crc_t;
    while len >= 32 {
        DOLIT32;
        c =
            crc_table[3][c & 255] ^ crc_table[2][(c >> 8) & 255] ^
                crc_table[1][(c >> 16) & 255] ^ crc_table[0][c >> 24];
        c ^= *{ let mut _t = buf4; buf4 = buf4.offset(1); _t };
        c =
            crc_table[3][c & 255] ^ crc_table[2][(c >> 8) & 255] ^
                crc_table[1][(c >> 16) & 255] ^ crc_table[0][c >> 24];
        c ^= *{ let mut _t = buf4; buf4 = buf4.offset(1); _t };
        c =
            crc_table[3][c & 255] ^ crc_table[2][(c >> 8) & 255] ^
                crc_table[1][(c >> 16) & 255] ^ crc_table[0][c >> 24];
        c ^= *{ let mut _t = buf4; buf4 = buf4.offset(1); _t };
        c =
            crc_table[3][c & 255] ^ crc_table[2][(c >> 8) & 255] ^
                crc_table[1][(c >> 16) & 255] ^ crc_table[0][c >> 24];
        c ^= *{ let mut _t = buf4; buf4 = buf4.offset(1); _t };
        c =
            crc_table[3][c & 255] ^ crc_table[2][(c >> 8) & 255] ^
                crc_table[1][(c >> 16) & 255] ^ crc_table[0][c >> 24];
        c ^= *{ let mut _t = buf4; buf4 = buf4.offset(1); _t };
        c =
            crc_table[3][c & 255] ^ crc_table[2][(c >> 8) & 255] ^
                crc_table[1][(c >> 16) & 255] ^ crc_table[0][c >> 24];
        c ^= *{ let mut _t = buf4; buf4 = buf4.offset(1); _t };
        c =
            crc_table[3][c & 255] ^ crc_table[2][(c >> 8) & 255] ^
                crc_table[1][(c >> 16) & 255] ^ crc_table[0][c >> 24];
        c ^= *{ let mut _t = buf4; buf4 = buf4.offset(1); _t };
        c =
            crc_table[3][c & 255] ^ crc_table[2][(c >> 8) & 255] ^
                crc_table[1][(c >> 16) & 255] ^ crc_table[0][c >> 24];
        len -= 32;
    }
    while len >= 4 {
        DOLIT4;
        c =
            crc_table[3][c & 255] ^ crc_table[2][(c >> 8) & 255] ^
                crc_table[1][(c >> 16) & 255] ^ crc_table[0][c >> 24];
        len -= 4;
    }
    buf = buf4 as *const u8;
    if len != 0 {
        loop  {
            c =
                crc_table[0][(c ^
                                  *{
                                       let mut _t = buf;
                                       buf = buf.offset(1);
                                       _t
                                   }) & 255] ^ (c >> 8);
            if { len -= 1; len } == 0 { break  };
        };
    }
    c = !c;
    return c as c_long;
}
/* ========================================================================= */
pub static DOBIG32: z_crc_t =

    /* ========================================================================= */
    c ^=
        *{
             let mut _t = buf4; /* BYFOUR */
             buf4 =
                 buf4.offset(1); /* dimension of GF(2) vectors (length of CRC) */
             _t
         };
pub static DOBIG4: z_crc_t =
    c ^= *{ let mut _t = buf4; buf4 = buf4.offset(1); _t };
unsafe fn crc32_big(mut crc: c_long, mut buf: *const u8, mut len: z_size_t)
 -> c_long {
    let mut c =
        ((((crc as z_crc_t) >> 24) & 255) + (((crc as z_crc_t) >> 8) & 65280)
             + (((crc as z_crc_t) & 65280) << 8) +
             (((crc as z_crc_t) & 255) << 24));
    c = !c;
    while len != 0 && ((buf as isize) & 3) != 0 {
        c =
            crc_table[4][(c >> 24) ^
                             *{ let mut _t = buf; buf = buf.offset(1); _t }] ^
                (c << 8);
        len -= 1;
    }
    let mut buf4 = buf as *const c_void as *const z_crc_t;
    while len >= 32 {
        DOBIG32;
        c =
            crc_table[4][c & 255] ^ crc_table[5][(c >> 8) & 255] ^
                crc_table[6][(c >> 16) & 255] ^ crc_table[7][c >> 24];
        c ^= *{ let mut _t = buf4; buf4 = buf4.offset(1); _t };
        c =
            crc_table[4][c & 255] ^ crc_table[5][(c >> 8) & 255] ^
                crc_table[6][(c >> 16) & 255] ^ crc_table[7][c >> 24];
        c ^= *{ let mut _t = buf4; buf4 = buf4.offset(1); _t };
        c =
            crc_table[4][c & 255] ^ crc_table[5][(c >> 8) & 255] ^
                crc_table[6][(c >> 16) & 255] ^ crc_table[7][c >> 24];
        c ^= *{ let mut _t = buf4; buf4 = buf4.offset(1); _t };
        c =
            crc_table[4][c & 255] ^ crc_table[5][(c >> 8) & 255] ^
                crc_table[6][(c >> 16) & 255] ^ crc_table[7][c >> 24];
        c ^= *{ let mut _t = buf4; buf4 = buf4.offset(1); _t };
        c =
            crc_table[4][c & 255] ^ crc_table[5][(c >> 8) & 255] ^
                crc_table[6][(c >> 16) & 255] ^ crc_table[7][c >> 24];
        c ^= *{ let mut _t = buf4; buf4 = buf4.offset(1); _t };
        c =
            crc_table[4][c & 255] ^ crc_table[5][(c >> 8) & 255] ^
                crc_table[6][(c >> 16) & 255] ^ crc_table[7][c >> 24];
        c ^= *{ let mut _t = buf4; buf4 = buf4.offset(1); _t };
        c =
            crc_table[4][c & 255] ^ crc_table[5][(c >> 8) & 255] ^
                crc_table[6][(c >> 16) & 255] ^ crc_table[7][c >> 24];
        c ^= *{ let mut _t = buf4; buf4 = buf4.offset(1); _t };
        c =
            crc_table[4][c & 255] ^ crc_table[5][(c >> 8) & 255] ^
                crc_table[6][(c >> 16) & 255] ^ crc_table[7][c >> 24];
        len -= 32;
    }
    while len >= 4 {
        DOBIG4;
        c =
            crc_table[4][c & 255] ^ crc_table[5][(c >> 8) & 255] ^
                crc_table[6][(c >> 16) & 255] ^ crc_table[7][c >> 24];
        len -= 4;
    }
    buf = buf4 as *const u8;
    if len != 0 {
        loop  {
            c =
                crc_table[4][(c >> 24) ^
                                 *{
                                      let mut _t = buf;
                                      buf = buf.offset(1);
                                      _t
                                  }] ^ (c << 8);
            if { len -= 1; len } == 0 { break  };
        };
    }
    c = !c;
    return ((((c) >> 24) & 255) + (((c) >> 8) & 65280) + (((c) & 65280) << 8)
                + (((c) & 255) << 24)) as c_long;
}
/* ========================================================================= */
unsafe fn gf2_matrix_times(mut mat: *mut c_long, mut vec: c_long) -> c_long {
    let mut sum = 0;
    while vec != 0 {
        if vec & 1 != 0 { sum ^= *mat; }
        vec >>= 1;
        mat = mat.offset(1);
    }
    return sum;
}
pub static GF2_DIM: c_int =

    /* ========================================================================= */
    32;
unsafe fn gf2_matrix_square(mut square: *mut c_long, mut mat: *mut c_long) {
    for mut n in 0..GF2_DIM { square[n] = gf2_matrix_times(mat, mat[n]) };
}
/* ========================================================================= */
unsafe fn crc32_combine_(mut crc1: uLong, mut crc2: uLong, mut len2: c_long)
 -> uLong {
    let mut even: [c_long; 32]; /* even-power-of-two zeros operator */
    let mut odd: [c_long; 32]; /* odd-power-of-two zeros operator */
    /* degenerate case (also disallow negative lengths) */
    if len2 <= 0 {
        return crc1; /* put operator for one zero bit in odd */
    } /* CRC-32 polynomial */
    odd[0] = 3988292384; /* put operator for two zero bits in even */
    let mut row = 1; /* put operator for four zero bits in odd */
    for mut n in 1..GF2_DIM { odd[n] = row; row <<= 1; }
    gf2_matrix_square(even, odd);
    gf2_matrix_square(odd, even);
    /* apply len2 zeros to crc1 (first square will put the operator for one
           zero byte, eight zero bits, in even) */
    loop  { /* apply zeros operator for this bit of len2 */
        gf2_matrix_square(even, odd); /* if no more bits set, then done */
        if len2 & 1 != 0 {
            crc1 =
                gf2_matrix_times(even,
                                 crc1); /* another iteration of the loop with odd and even swapped */
        } /* if no more bits set, then done */
        len2 >>= 1; /* return combined crc */
        if len2 == 0 { break ; }
        gf2_matrix_square(odd, even);
        if len2 & 1 != 0 { crc1 = gf2_matrix_times(odd, crc1); }
        len2 >>= 1;
        if len2 == 0 { break  };
    }
    crc1 ^= crc2;
    return crc1;
}
/* ========================================================================= */
#[no_mangle]
pub unsafe extern "C" fn crc32_combine(mut crc1: uLong, mut crc2: uLong,
                                       mut len2: c_long) -> uLong {
    return crc32_combine_(crc1, crc2, len2);
}
#[no_mangle]
pub unsafe extern "C" fn crc32_combine64(mut crc1: uLong, mut crc2: uLong,
                                         mut len2: c_long) -> uLong {
    return crc32_combine_(crc1, crc2, len2);
}
