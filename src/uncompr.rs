/* uncompr.c -- decompress a memory buffer
 * Copyright (C) 1995-2003, 2010, 2014, 2016 Jean-loup Gailly, Mark Adler
 * For conditions of distribution and use, see copyright notice in zlib.h
 */
/* @(#) $Id$ */
/* ===========================================================================
     Decompresses the source buffer into the destination buffer.  *sourceLen is
   the byte length of the source buffer. Upon entry, *destLen is the total size
   of the destination buffer, which must be large enough to hold the entire
   uncompressed data. (The size of the uncompressed data must have been saved
   previously by the compressor and transmitted to the decompressor by some
   mechanism outside the scope of this compression library.) Upon exit,
   *destLen is the size of the decompressed data and *sourceLen is the number
   of source bytes consumed. Upon return, source + *sourceLen points to the
   first unused input byte.

     uncompress returns Z_OK if success, Z_MEM_ERROR if there was not enough
   memory, Z_BUF_ERROR if there was not enough room in the output buffer, or
   Z_DATA_ERROR if the input data was corrupted, including if the input data is
   an incomplete zlib stream.
*/
/* for detection of incomplete stream when *destLen == 0 */

const L_CODES: usize = 0;
const DEPTH_SIZE: usize = 2 * L_CODES + 1;
const MAX_BITS: usize = 15;
const BL_SIZE: usize = MAX_BITS + 1;

struct gz_header<'a> {
    text: i32, /* true if compressed data believed to be text */
    time: u32, /* modification time */
    xflags: i32, /* extra flags (not used when writing a gzip file) */
    os: i32, /* operating system */
    extra: &'a u8, /* pointer to extra field or Z_NULL if none */
    extra_len: u32, /* extra field length (valid if extra != Z_NULL) */
    extra_max: u32, /* space at extra (only when reading header) */
    name: Option<&'a str>, /* pointer to zero-terminated file name or Z_NULL */
    name_max: u32, /* space at name (only when reading header) */
    comment: Option<&'a str>, /* pointer to zero-terminated comment or Z_NULL */
    comm_max: u32, /* space at comment (only when reading header) */
    hcrc: i32, /* true if there was or will be a header crc */
    done: i32,       /* true when done reading gzip header (not used */
}

struct internal_state<'a> {
    strm: &'a z_stream<'a>, /* pointer back to this zlib stream */
    status: i32, /* as the name implies */
    pending_buf: &'a u8, /* output still pending */
    pending_buf_size: u32, /* size of pending_buf */
    pending_out: &'a u8, /* next pending byte to output to the stream */
    pending: u32, /* nb of bytes in the pending buffer */
    wrap: i32, /* bit 0 true for zlib, bit 1 true for gzip */

    gzhead: &'a gz_header<'a>, /* gzip header information to write */
    gzindex: u32, /* where in extra, name, or comment */
    method: u8, /* can only be DEFLATED */
    last_flush: i32, /* value of flush param for previous deflate call */

    /* used by deflate.c: */
    w_size: u32, /* LZ77 window size (32K by default) */
    w_bits: u32, /* log2(w_size)  (8..16) */
    w_mask: u32, /* w_size - 1 */

    window: &'a u8,
    /* Sliding window. Input bytes are read into the second half of the window,
     * and move to the first half later to keep a dictionary of at least wSize
     * bytes. With this organization, matches are limited to a distance of
     * wSize-MAX_MATCH bytes, but this ensures that IO is always
     * performed with a length multiple of the block size. Also, it limits
     * the window size to 64K, which is quite useful on MSDOS.
     * To do: use the user input buffer as sliding window.
     */
     window_size: u32,
     /* Actual size of window: 2*wSize, except when the user input buffer
     * is directly used as sliding window.
     */
     prev: &'a u16,
    /* Link to older string with same hash index. To limit the size of this
     * array to 64K, this link is maintained only for the last 32K strings.
     * An index in this array is thus a window index modulo 32K.
     */

    head: Option<&'a u16>, /* Heads of the hash chains or NIL. */

    ins_h: u32, /* hash index of string to be inserted */
    hash_size: u32, /* number of elements in hash table */
    hash_bits: u32, /* log2(hash_size) */
    hash_mask: u32, /* hash_size-1 */

    hash_shift: u32,
    /* Number of bits by which ins_h must be shifted at each input
     * step. It must be such that after MIN_MATCH steps, the oldest
     * byte no longer takes part in the hash key, that is:
     *   hash_shift * MIN_MATCH >= hash_bits
     */

    block_start: i32,
    /* Window position at the beginning of the current output block. Gets
     * negative when the window is moved backwards.
     */
    match_length: u32, /* length of best match */
    prev_match: u32, /* previous match */
    match_available: i32, /* set if previous match exists */
    strstart: u32, /* start of string to insert */
    match_start: u32, /* start of matching string */
    lookahead: u32, /* number of valid bytes ahead in window */

    prev_length: u32,
    /* Length of the best match at previous step. Matches not greater than this
     * are discarded. This is used in the lazy match evaluation.
     */

    max_chain_length: u32,
    /* To speed up deflation, hash chains are never searched beyond this
     * length.  A higher limit improves compression ratio but degrades the
     * speed.
     */

    max_lazy_match: u32,
    /* Attempt to find a better match only when the current match is strictly
     * smaller than this value. This mechanism is used only for compression
     * levels >= 4.
     */

//    #   define max_insert_length  max_lazy_match
    /* Insert new strings in the hash table only if the match length is not
     * greater than this length. This saves time but degrades compression.
     * max_insert_length is used only for compression levels <= 3.
     */

    level: i32, /* compression level (1..9) */
    strategy: i32, /* favor or force Huffman coding*/

    good_match: u32, /* Use a faster search when the previous match is longer than this */
    nice_match: i32, /* Stop searching when current match exceeds this */

    /* used by trees.c: */
    /* Didn't use ct_data typedef below to suppress compiler warning */
//    struct ct_data_s dyn_ltree[HEAP_SIZE];   /* literal and length tree */
//    struct ct_data_s dyn_dtree[2*D_CODES+1]; /* distance tree */
//    struct ct_data_s bl_tree[2*BL_CODES+1];  /* Huffman tree for bit lengths */
//
//    struct tree_desc_s l_desc;               /* desc. for literal tree */
//    struct tree_desc_s d_desc;               /* desc. for distance tree */
//    struct tree_desc_s bl_desc;              /* desc. for bit length tree */
//

    bl_count: [u16; BL_SIZE], /* number of codes at each bit length for an optimal tree */

    heap: [i32; DEPTH_SIZE], /* heap used to build the Huffman trees */
    heap_len: i32, /* number of elements in the heap */
    heap_max: i32, /* element of largest frequency */
    /* The sons of heap[n] are heap[2*n] and heap[2*n+1]. heap[0] is not used.
     * The same heap array is used to build all trees.
     */
    depth: [u8; DEPTH_SIZE],
    /* Depth of each subtree used as tie breaker for trees of equal frequency*/
    l_buf: &'a u8, /* buffer for literals or lengths */

    lit_bufsize: u32,
    /* Size of match buffer for literals/lengths.  There are 4 reasons for
     * limiting lit_bufsize to 64K:
     *   - frequencies can be kept in 16 bit counters
     *   - if compression is not successful for the first block, all input
     *     data is still in the window so we can still emit a stored block even
     *     when input comes from standard input.  (This can also be done for
     *     all blocks if lit_bufsize is not greater than 32K.)
     *   - if compression is not successful for a file smaller than 64K, we can
     *     even emit a stored file instead of a stored block (saving 5 bytes).
     *     This is applicable only for zip (not gzip or zlib).
     *   - creating new Huffman trees less frequently may not provide fast
     *     adaptation to changes in the input data statistics. (Take for
     *     example a binary file with poorly compressible code followed by
     *     a highly compressible string table.) Smaller buffer sizes give
     *     fast adaptation but have of course the overhead of transmitting
     *     trees more frequently.
     *   - I can't count above 4
     */

    last_lit: u32, /* running index in l_buf */

    d_buf: &'a u16,

    /* Buffer for distances. To simplify the code, d_buf and l_buf have
     * the same number of elements. To use different lengths, an extra flag
     * array would be necessary.
     */

    opt_len: u32, /* bit length of current block with optimal trees */
    static_len: u32, /* bit length of current block with static trees */
    matches: u32, /* number of string matches in current block */
    insert: u32, /* bytes at end of window left to insert */

//    #ifdef ZLIB_DEBUG
//    ulg compressed_len; /* total bit length of compressed file mod 2^32 */
//    ulg bits_sent;      /* bit length of compressed data sent mod 2^32 */
//    #endif

    bi_buf: u16,
    /* Output buffer. bits are inserted starting at the bottom (least
     * significant bits).
     */
    bi_valid: i32,
    /* Number of valid bits in bi_buf.  All bits above the last valid bit
     * are always zero.
     */

    high_water: u32,
    /* High water mark offset in window for initialized bytes -- bytes above
     * this are set to zero in order to avoid memory check warnings when
     * longest match routines access bytes past the input.  This is then
     * updated to the new high water mark.
     */
}

struct z_stream<'a> {
    next_in: &'a u8,
    avail_in: u32,
    total_in: u32,

    next_out: &'a mut u8,
    avail_out: u32,
    total_out: u32,

    msg: Option<&'a str>,
//    internal_state:
//    alloc_func
//    free_func
//    voidpf // private data

    data_type: i32,
    adler: u32,
    reserved: u32,
}




pub static Z_OK: i32 = 0;
pub static Z_NO_FLUSH: i32 = 0;
pub static Z_BUF_ERROR: i32 = -5;
pub static Z_STREAM_END: i32 = 1;
pub static Z_NEED_DICT: i32 = 2;
pub static Z_DATA_ERROR: i32 = -3;


fn uncompress_2(dest: &mut[u8], source: &[u8]) -> i32 {
    // some setup...
    let mut stream: z_stream;
    let max = -1 as uInt;
    let mut left: uLong;
    let mut buf: [Byte; 1];
    let mut len = *sourceLen;

    if *destLen != 0 {
        left = *destLen;
        *destLen = 0;
    } else { left = 1; dest = buf; }

    stream.next_in = source as *mut Bytef;
    stream.avail_in = 0;
    stream.zalloc = 0 as alloc_func;
    stream.zfree = 0 as free_func;
    stream.opaque = 0 as voidpf;

    let mut err =
        inflateInit_((&mut stream), "1.2.11",
                     std::mem::size_of::<z_stream>() as c_int);
    if err != Z_OK { return err; }
    stream.next_out = dest;
    stream.avail_out = 0;

    loop  {
        if stream.avail_out == 0 {
            stream.avail_out =
                if left > (max as uLong) { max } else { left as uInt };
            left -= stream.avail_out;
        }
        if stream.avail_in == 0 {
            stream.avail_in =
                if len > (max as uLong) { max } else { len as uInt };
            len -= stream.avail_in;
        }
        err = inflate(&mut stream, Z_NO_FLUSH);
        if err != Z_OK { break  };
    }

    0
}


//#[no_mangle]
//pub unsafe extern "C" fn uncompress2(mut dest: *mut Bytef,
//                                     mut destLen: &mut uLongf,
//                                     mut source: *const Bytef,
//                                     mut sourceLen: &mut uLong) -> c_int {
//    let mut stream: z_stream;
//    let max = -1 as uInt;
//    let mut left: uLong;
//    let mut buf: [Byte; 1];
//    let mut len = *sourceLen;
//    if *destLen != 0 {
//        left = *destLen;
//        *destLen = 0;
//    } else { left = 1; dest = buf; }
//    stream.next_in = source as *mut Bytef;
//    stream.avail_in = 0;
//    stream.zalloc = 0 as alloc_func;
//    stream.zfree = 0 as free_func;
//    stream.opaque = 0 as voidpf;
//    let mut err =
//        inflateInit_((&mut stream), "1.2.11",
//                     std::mem::size_of::<z_stream>() as c_int);
//    if err != Z_OK { return err; }
//    stream.next_out = dest;
//    stream.avail_out = 0;
//
//
//    loop  {
//        if stream.avail_out == 0 {
//            stream.avail_out =
//                if left > (max as uLong) { max } else { left as uInt };
//            left -= stream.avail_out;
//        }
//        if stream.avail_in == 0 {
//            stream.avail_in =
//                if len > (max as uLong) { max } else { len as uInt };
//            len -= stream.avail_in;
//        }
//        err = inflate(&mut stream, Z_NO_FLUSH);
//        if err != Z_OK { break  };
//    }
//
//
//    *sourceLen -= len + stream.avail_in;
//    if dest != buf {
//        *destLen = stream.total_out;
//    } else if stream.total_out != 0 && err == Z_BUF_ERROR { left = 1; }
//    inflateEnd(&mut stream);
//    return if err == Z_STREAM_END {
//               Z_OK
//           } else if err == Z_NEED_DICT {
//               Z_DATA_ERROR
//           } else if err == Z_BUF_ERROR && left + stream.avail_out != 0 {
//               Z_DATA_ERROR
//           } else { err };
//}
//#[no_mangle]
//pub unsafe extern "C" fn uncompress(mut dest: *mut Bytef,
//                                    mut destLen: *mut uLongf,
//                                    mut source: *const Bytef,
//                                    mut sourceLen: uLong) -> c_int {
//    return uncompress2(dest, destLen, source, &mut sourceLen);
//}
