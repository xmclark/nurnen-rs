//pub mod adler32;
//pub mod uncompr;
//pub mod trees;

static Z_NO_FLUSH: usize = 0;
static Z_FINISH: usize = 4;

static Z_OK: usize = 0;
static Z_STREAM_END: usize = 1;
static Z_SYNC_FLUSH: usize = 2;

static Z_DEFAULT_COMPRESSION: i32 = -1;

static Z_DEFAULT_STRATEGY: usize = 0;

static Z_DEFLATED: usize = 8;

pub struct ZStream {
    avail_out: usize,
    avail_in: usize,
    next_out: usize,
    output: Vec<u8>,
    message: String,
}

impl ZStream {
    pub fn new() -> ZStream {
        ZStream {
            avail_out: 0,
            avail_in: 0,
            next_out: 0,
            output: Vec::new(),
            message: "".to_string(),
        }
    }
}

pub fn deflate_init_2(stream: &ZStream, level: i32, method: usize, window_bits: usize,
                      mem_level: usize, strategy: usize) -> usize {
    Z_OK
}

pub fn deflate_set_header(stream: &ZStream, header: usize) {

}

pub fn deflate(stream: &ZStream, mode: usize) -> usize {
    Z_OK
}

pub fn deflate_end(stream: &ZStream) -> usize {
    Z_OK
}

struct DeflateOptions {
    pub level: i32,
    pub method: usize,
    pub chunk_size: usize,
    pub window_bits: usize,
    pub mem_level: usize,
    pub strategy: usize,
    pub to: String,
    pub header: Option<usize> // stubbing this for now
}

impl Default for DeflateOptions {
    fn default() -> DeflateOptions {
        DeflateOptions {
            level: Z_DEFAULT_COMPRESSION,
            method: Z_DEFLATED,
            chunk_size: 16384,
            window_bits: 15,
            mem_level: 8,
            strategy: Z_DEFAULT_STRATEGY,
            to: "".to_string(),
            header: None, // stubbing this for now
        }
    }
}

pub enum DeflateData {
    Str(String),
}

pub struct Deflate {
    options: DeflateOptions,
    stream: ZStream,
    ended: bool,
    chunks: Vec<Vec<u8>>,
    result: Vec<u8>,
    error: usize,
    message: String,
}

impl Deflate {
    pub fn new() -> Deflate {
        let options = DeflateOptions::default();
        let stream = ZStream::new();
        // was zlib_deflate.deflateInit2
        let status = deflate_init_2(
            &stream,
            options.level,
            options.method,
            options.window_bits,
            options.mem_level,
            options.strategy
        );

        if status != Z_OK {
            // must somehow throw, maybe panic?
            // throw new Error(msg[status]);
            panic!("status is not OK!")
        }

        match options.header {
            Some(h) => deflate_set_header(&stream, h),
            _ => ()
        }
//        if (options.header) {
//            deflate_set_header(this.strm, opt.header);
//        }

        let deflate = Deflate{
            options,
            stream,
            ended: false,
            chunks: Vec::new(),
            result: Vec::new(),
            error: 0,
            message: "".to_string(),
        };
        deflate
    }

    pub fn push(&mut self, data: DeflateData, mode: usize) -> bool {
        match data {
            DeflateData::Str(s) => {
                s.into_bytes();
                ()
            }
            _ => ()
        }

//        let mode = (mode === ~~mode) ? mode : ((mode === true) ? Z_FINISH : Z_NO_FLUSH);

        // do-while equivalent
        loop {
            if self.stream.avail_out == 0 {
//                strm.output = new utils.Buf8(chunkSize);
//                self.stream.next_out = 0;
                self.stream.avail_out = self.options.chunk_size;
            }

            let status = deflate(&self.stream, mode);

            if status != Z_STREAM_END && status != Z_OK {
                self.on_end(status);
                self.ended = true;
                return false // early return
            }

            if self.stream.avail_out == 0 || (self.stream.avail_in == 0 && (mode == Z_FINISH || mode == Z_SYNC_FLUSH)) {
                self.on_data();
//                if (this.options.to === 'string') {
//                    this.onData(strings.buf2binstring(utils.shrinkBuf(strm.output, strm.next_out)));
//                } else {
//                    this.onData(utils.shrinkBuf(strm.output, strm.next_out));
//                }
            }

            // do-while condition
            if (self.stream.avail_in > 0 || self.stream.avail_out == 0) && status != Z_STREAM_END {
                break
            }
        }

        // Finalize on the last chunk.
        if mode == Z_FINISH {
//            status = zlib_deflate.deflateEnd(this.strm);
            let status = deflate_end(&self.stream);
            self.on_end(status);
            self.ended = true;
            return status == Z_OK;
        }

        // callback interim results if Z_SYNC_FLUSH.
        if mode == Z_SYNC_FLUSH {
            self.on_end(Z_OK);
            self.stream.avail_out = 0;
            return true;
        }

        return true;
    }

    pub fn on_data(&mut self) {
        self.chunks.push(self.stream.output.clone());
    }

    pub fn on_end(&mut self, status: usize) {
        if status == Z_OK {
//            if (this.options.to === 'string') {
//                this.result = this.chunks.join('');
//            } else {
//                this.result = utils.flattenChunks(this.chunks);
//            }
            
            // flatten the chunks
            self.result = self.chunks.iter()
                .flat_map(|v| v.iter())
                .cloned()
                .collect();
        }
        self.chunks = Vec::new();
        self.error = status;
        self.message = self.stream.message.clone();
    }
}



#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
