
/* adler32.c -- compute the Adler-32 checksum of a data stream
 * Copyright (C) 1995-2011, 2016 Mark Adler
 * For conditions of distribution and use, see copyright notice in zlib.h
 */
/* @(#) $Id$ */
pub static BASE: u32 = 65521; /* largest prime smaller than 65536 */

/* use NO_DIVIDE if your processor does not do division in hardware --
   try it both ways to see which is faster */
/* note that this assumes BASE is 65521, where 65536 % 65521 == 15
   (thank you to John Reiser for pointing this out) */
/* this assumes a is not negative */
/* ========================================================================= */
/* split Adler-32 into component sums */
/* in case user likes doing a byte at a time, keep it fast */

/* initial Adler-32 value (deferred check for len == 1 speed) */
//pub static Z_NULL: *const Bytef = ptr::null_mut();


pub static NMAX: u32 = 5552;
/* NMAX is the largest n such that 255n(n+1)/2 + (n+1)(BASE-1) <= 2^32-1 */


/* in case short lengths are provided, keep it somewhat fast */
/* only added so many BASE's */
/* do length NMAX blocks -- requires just one modulo operation */


pub fn adler32_z<I>(adler: u32, mut buf: I, len: u32) where I: Iterator<Item = u32> {
  /* split Adler-32 into component sums */
  let mut sum = (adler >> 16) & 0xffff;
  let mut adler = adler & 0xffff;

  /* in case user likes doing a byte at a time, keep it fast */
  if len == 1 {
    adler += buf.next().unwrap();
    if adler >= BASE {
      adler -= BASE;
    }
    sum += adler;
    if sum >= BASE {
      sum -= BASE;
    }
    adler | (sum << 16)
  }

  /* in case short lengths are provided, keep it somewhat fast */
  if len < 16 {
    while len-- {
      adler += buf.next().unwrap();
      buf.next();
      // adler += *buf++;
      sum2 += adler;
    }
//    if (adler >= BASE)
//    adler -= BASE;
//    MOD28(sum2);            /* only added so many BASE's */
//    return adler | (sum2 << 16);
  }

}



//#[no_mangle]
//pub unsafe extern "C" fn adler32_z_C(mut adler: uLong, mut buf: *const Bytef,
//                                   mut len: z_size_t) -> uLong {
//    let mut n: c_uint; /* NMAX is divisible by 16 */
//    let mut sum2 = (adler >> 16) & 65535; /* 16 sums unrolled */
//    adler &=
//        65535; /* do remaining bytes (less than NMAX, still just one modulo) */
//    if len == 1 {
//        adler += *buf.offset(0); /* avoid modulos if none remaining */
//        if adler >= BASE {
//            adler -= BASE; /* return recombined sums */
//        }
//        sum2 += adler;
//        if sum2 >= BASE { sum2 -= BASE; }
//        return adler | (sum2 << 16);
//    }
//    if buf == Z_NULL { return 1; }
//    if len < 16 {
//        while { let mut _t = len; len -= 1; _t } != 0 {
//            adler += *{ let mut _t = buf; buf = buf.offset(1); _t };
//            sum2 += adler;
//        }
//        if adler >= BASE { adler -= BASE; }
//        sum2 %= 65521;
//        return adler | (sum2 << 16);
//    }
//    while len >= NMAX {
//        len -= NMAX;
//        n = NMAX / 16;
//        loop  {
//            { adler += *(buf).offset(0); sum2 += adler; }
//            { adler += *(buf).offset(0 + 1); sum2 += adler; }
//            { adler += *(buf).offset(0 + 2); sum2 += adler; }
//            { adler += *(buf).offset(0 + 2 + 1); sum2 += adler; }
//            { adler += *(buf).offset(0 + 4); sum2 += adler; }
//            { adler += *(buf).offset(0 + 4 + 1); sum2 += adler; }
//            { adler += *(buf).offset(0 + 4 + 2); sum2 += adler; }
//            { adler += *(buf).offset(0 + 4 + 2 + 1); sum2 += adler; }
//            { adler += *(buf).offset(8); sum2 += adler; }
//            { adler += *(buf).offset(8 + 1); sum2 += adler; }
//            { adler += *(buf).offset(8 + 2); sum2 += adler; }
//            { adler += *(buf).offset(8 + 2 + 1); sum2 += adler; }
//            { adler += *(buf).offset(8 + 4); sum2 += adler; }
//            { adler += *(buf).offset(8 + 4 + 1); sum2 += adler; }
//            { adler += *(buf).offset(8 + 4 + 2); sum2 += adler; }
//            { adler += *(buf).offset(8 + 4 + 2 + 1); sum2 += adler; }
//            buf = buf.offset(16);
//            if { n -= 1; n } == 0 { break  };
//        }
//        adler %= 65521;
//        sum2 %= 65521;
//    }
//    if len != 0 {
//        while len >= 16 {
//            len -= 16;
//            { adler += *(buf).offset(0); sum2 += adler; }
//            { adler += *(buf).offset(0 + 1); sum2 += adler; }
//            { adler += *(buf).offset(0 + 2); sum2 += adler; }
//            { adler += *(buf).offset(0 + 2 + 1); sum2 += adler; }
//            { adler += *(buf).offset(0 + 4); sum2 += adler; }
//            { adler += *(buf).offset(0 + 4 + 1); sum2 += adler; }
//            { adler += *(buf).offset(0 + 4 + 2); sum2 += adler; }
//            { adler += *(buf).offset(0 + 4 + 2 + 1); sum2 += adler; }
//            { adler += *(buf).offset(8); sum2 += adler; }
//            { adler += *(buf).offset(8 + 1); sum2 += adler; }
//            { adler += *(buf).offset(8 + 2); sum2 += adler; }
//            { adler += *(buf).offset(8 + 2 + 1); sum2 += adler; }
//            { adler += *(buf).offset(8 + 4); sum2 += adler; }
//            { adler += *(buf).offset(8 + 4 + 1); sum2 += adler; }
//            { adler += *(buf).offset(8 + 4 + 2); sum2 += adler; }
//            { adler += *(buf).offset(8 + 4 + 2 + 1); sum2 += adler; }
//            buf = buf.offset(16);
//        }
//        while { let mut _t = len; len -= 1; _t } != 0 {
//            adler += *{ let mut _t = buf; buf = buf.offset(1); _t };
//            sum2 += adler;
//        }
//        adler %= 65521;
//        sum2 %= 65521;
//    }
//    return adler | (sum2 << 16);
//}
//* ========================================================================= */
//#[no_mangle]
//pub unsafe extern "C" fn adler32(mut adler: uLong, mut buf: *const Bytef,
//                                 mut len: uInt) -> uLong {
//    return adler32_z(adler, buf, len);
//}
//* ========================================================================= */
//unsafe fn adler32_combine_(mut adler1: uLong, mut adler2: uLong,
//                           mut len2: c_long)
// ->
//     uLong { /* for negative len, return invalid adler32 as a clue for debugging */
//    if len2 < 0 {
//        return 4294967295; /* the derivation of this formula is left as an exercise for the reader */
//    } /* assumes len2 >= 0 */
//    len2 %= 65521;
//    let mut rem = len2 as c_uint;
//    let mut sum1 = adler1 & 65535;
//    let mut sum2 = rem * sum1;
//    sum2 %= 65521;
//    sum1 += (adler2 & 65535) + BASE - 1;
//    sum2 += ((adler1 >> 16) & 65535) + ((adler2 >> 16) & 65535) + BASE - rem;
//    if sum1 >= BASE { sum1 -= BASE; }
//    if sum1 >= BASE { sum1 -= BASE; }
//    if sum2 >= ((BASE as c_long) << 1) { sum2 -= (BASE as c_long) << 1; }
//    if sum2 >= BASE { sum2 -= BASE; }
//    return sum1 | (sum2 << 16);
//}
//* ========================================================================= */
//#[no_mangle]
//pub unsafe extern "C" fn adler32_combine(mut adler1: uLong, mut adler2: uLong,
//                                         mut len2: c_long) -> uLong {
//    return adler32_combine_(adler1, adler2, len2);
//}
//#[no_mangle]
//pub unsafe extern "C" fn adler32_combine64(mut adler1: uLong,
//                                           mut adler2: uLong,
//                                           mut len2: c_long) -> uLong {
//    return adler32_combine_(adler1, adler2, len2);
//}
