/* infback.c -- inflate using a call-back interface
 * Copyright (C) 1995-2016 Mark Adler
 * For conditions of distribution and use, see copyright notice in zlib.h
 */
/*
   This code is largely copied from inflate.c.  Normally either infback.o or
   inflate.o would be linked into an application--not both.  The interface
   with inffast.c is retained so that optimized assembler-coded versions of
   inflate_fast() can be used with either inflate.c or infback.c.
 */
/* function prototypes */
/*
   strm provides memory allocation functions in zalloc and zfree, or
   Z_NULL to use the library memory allocation functions.

   windowBits is in the range 8..15, and window is a user-supplied
   window and output buffer that is 2**windowBits bytes.
 */
pub static Z_NULL: *const i8 = ptr::null_mut();
pub static ZLIB_VERSION: *mut i8 = "1.2.11";
pub static Z_VERSION_ERROR: c_int = -6;
pub static Z_STREAM_ERROR: c_int = -2;
/* in case we return an error */
pub static Z_MEM_ERROR: c_int = -4;
pub static Z_OK: c_int = 0;
#[no_mangle]
pub unsafe extern "C" fn inflateBackInit_(mut strm: z_streamp,
                                          mut windowBits: c_int,
                                          mut window: *mut u8,
                                          mut version: *const i8,
                                          mut stream_size: c_int) -> c_int {
    if version == Z_NULL || *version.offset(0) != ZLIB_VERSION[0] ||
           stream_size != (std::mem::size_of::<z_stream>() as c_int) {
        return Z_VERSION_ERROR;
    }
    if strm == Z_NULL || window == Z_NULL || windowBits < 8 || windowBits > 15
       {
        return Z_STREAM_ERROR;
    }
    strm.msg = Z_NULL;
    if strm.zalloc == (0 as alloc_func) {
        strm.zalloc = zcalloc;
        strm.opaque = 0 as voidpf;
    }
    if strm.zfree == (0 as free_func) { strm.zfree = zcfree; }
    let mut state =
        (*((strm).zalloc))((strm).opaque, 1,
                           std::mem::size_of::<inflate_state>()) as
            *mut inflate_state;
    if state == Z_NULL { return Z_MEM_ERROR; }
    strm.state = state as *mut internal_state;
    state.dmax = 32768;
    state.wbits = windowBits as uInt;
    state.wsize = 1 << windowBits;
    state.window = window;
    state.wnext = 0;
    state.whave = 0;
    return Z_OK;
}
/*
   Return state with length and distance decoding tables and index sizes set to
   fixed code decoding.  Normally this returns fixed tables from inffixed.h.
   If BUILDFIXED is defined, then instead this routine builds the tables the
   first time it's called, and returns those tables the first time and
   thereafter.  This reduces the size of the code by about 2K bytes, in
   exchange for a little execution time.  However, BUILDFIXED should not be
   used for threaded applications, since the rewriting of the tables and virgin
   may not be thread-safe.
 */
unsafe fn fixedtables(mut state:
                          &mut inflate_state) { /* build fixed huffman tables if first call (may not be thread safe) */
    /* literal/length table */
    /* distance table */
    /* do this just once */
    /* !BUILDFIXED */
    /* BUILDFIXED */
    state.lencode = lenfix;
    state.lenbits = 9;
    state.distcode = distfix;
    state.distbits = 5;
}
/* Macros for inflateBack(): */
/* Load returned state from inflate_fast() */
/* Set state from registers for inflate_fast() */
/* Clear the input bit accumulator */
/* Assure that some input is available.  If input is requested, but denied,
   then return a Z_BUF_ERROR from inflateBack(). */
/* Get a byte of input into the bit accumulator, or return from inflateBack()
   with an error if there is no input available. */
/* Assure that there are at least n bits in the bit accumulator.  If there is
   not enough available input to do that, then return from inflateBack() with
   an error. */
/* Return the low n bits of the bit accumulator (n < 16) */
/* Remove n bits from the bit accumulator */
/* Remove zero to seven bits as needed to go to a byte boundary */
/* Assure that some output space is available, by writing out the window
   if it's full.  If the write fails, return from inflateBack() with a
   Z_BUF_ERROR. */
/*
   strm provides the memory allocation functions and window buffer on input,
   and provides information on the unused input on return.  For Z_DATA_ERROR
   returns, strm will also provide an error message.

   in() and out() are the call-back input and output functions.  When
   inflateBack() needs more input, it calls in().  When inflateBack() has
   filled the window with output, or when it completes with data in the
   window, it calls out() to write out the data.  The application must not
   change the provided input until in() is called again or inflateBack()
   returns.  The application must not change the window/output buffer until
   inflateBack() returns.

   in() and out() are called with a descriptor parameter provided in the
   inflateBack() call.  This parameter can be a structure that provides the
   information required to do the read or write, as well as accumulated
   information on the input and output such as totals and check values.

   in() should return zero on failure.  out() should return non-zero on
   failure.  If either in() or out() fails, than inflateBack() returns a
   Z_BUF_ERROR.  strm->next_in can be checked for Z_NULL to see whether it
   was in() or out() that caused in the error.  Otherwise,  inflateBack()
   returns Z_STREAM_END on success, Z_DATA_ERROR for an deflate format
   error, or Z_MEM_ERROR if it could not allocate memory for the state.
   inflateBack() can also return Z_STREAM_ERROR if the input parameters
   are not correct, i.e. strm is Z_NULL or the state was not initialized.
 */
/* next input */
/* next output */
/* available input and output */
/* bit buffer */
/* bits in bit buffer */
/* number of stored or match bytes to copy */
/* where to copy match bytes from */
/* current decoding table entry */
/* parent table entry */
/* length to copy for repeats, bits to drop */
/* return code */
/* permutation of code lengths */
/* Check that the strm exists and that the state was initialized */
/* Reset the state */
/* Inflate until end of block marked as last */
/* determine and dispatch block type */
/* stored block */
/* fixed block */
/* decode codes */
/* dynamic block */
/* get and verify stored block length */
/* go to byte boundary */
/* copy stored block from input to output */
/* get dynamic table entries descriptor */
/* get code length code lengths (not a typo) */
/* get length and distance code code lengths */
/* handle error breaks in while */
/* check for end-of-block code (better have one) */
/* build code tables -- note: do not change the lenbits or distbits
               values here (9 and 6) without reading the comments in inftrees.h
               concerning the ENOUGH constants, which depend on those values */
/* use inflate_fast() if we have enough input and output */
/* get a literal, length, or end-of-block code */
/* process literal */
/* process end of block */
/* invalid code */
/* length code -- get extra bits, if any */
/* get distance code */
/* get distance extra bits, if any */
/* copy match from window to output */
/* inflate stream terminated properly -- write leftover output */
pub static Z_STREAM_END: c_int = 1;
pub static Z_BUF_ERROR: c_int = -5;
pub static Z_DATA_ERROR: c_int = -3;
#[no_mangle]
pub unsafe extern "C" fn inflateBack(mut strm: z_streamp, mut in_: in_func,
                                     mut in_desc: *mut c_void,
                                     mut out: out_func,
                                     mut out_desc: *mut c_void) -> c_int {
    let mut copy: c_uint; /* can't happen, but makes compilers happy */
    let mut from: *mut u8; /* Return unused input */
    let mut here: code;
    let mut len: c_uint;
    let mut ret;
    let mut order =
        [16u16, 17u16, 18u16, 0u16, 8u16, 7u16, 9u16, 6u16, 10u16, 5u16,
         11u16, 4u16, 12u16, 3u16, 13u16, 2u16, 14u16, 1u16, 15u16];
    if strm == Z_NULL || strm.state == Z_NULL { return Z_STREAM_ERROR; }
    let mut state = strm.state as *mut inflate_state;
    strm.msg = Z_NULL;
    state.mode = TYPE;
    state.last = 0;
    state.whave = 0;
    let mut next = strm.next_in;
    let mut have = if next != Z_NULL { strm.avail_in } else { 0 };
    let mut hold = 0;
    let mut bits = 0;
    let mut put = state.window;
    let mut left = state.wsize;
    let mut last: code;
    loop  {
        match state.mode {
            TYPE => {
                if state.last != 0 {
                    loop  {
                        hold >>= bits & 7;
                        bits -= bits & 7;
                        if 0 == 0 { break  };
                    }
                    state.mode = DONE;
                }
                loop  {
                    while bits < 3 {
                        loop  {
                            loop  {
                                if have == 0 {
                                    have = in_(in_desc, &mut next);
                                    if have == 0 {
                                        next = ptr::null_mut();
                                        ret = (-5);
                                        break inf_leave ;
                                    };
                                }
                                if 0 == 0 { break  };
                            }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    if 0 == 0 { break  };
                }
                state.last = ((hold as c_uint) & ((1 << 1) - 1));
                loop  { hold >>= 1; bits -= 1; if 0 == 0 { break  }; }
                match ((hold as c_uint) & ((1 << 2) - 1)) {
                    0 => state.mode = STORED,
                    1 => { fixedtables(state); state.mode = LEN }
                    2 => state.mode = TABLE,
                    3 => { strm.msg = "invalid block type"; state.mode = BAD }
                }
                loop  { hold >>= 2; bits -= 2; if 0 == 0 { break  }; }
            }
            STORED => {
                loop  {
                    hold >>= bits & 7;
                    bits -= bits & 7;
                    if 0 == 0 { break  };
                }
                loop  {
                    while bits < 32 {
                        loop  {
                            loop  {
                                if have == 0 {
                                    have = in_(in_desc, &mut next);
                                    if have == 0 {
                                        next = ptr::null_mut();
                                        ret = (-5);
                                        break inf_leave ;
                                    };
                                }
                                if 0 == 0 { break  };
                            }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    if 0 == 0 { break  };
                }
                if (hold & 65535) != ((hold >> 16) ^ 65535) {
                    strm.msg = "invalid stored block lengths";
                    state.mode = BAD;
                }
                state.length = (hold as c_uint) & 65535;
                loop  { hold = 0; bits = 0; if 0 == 0 { break  }; }
                while state.length != 0 {
                    copy = state.length;
                    loop  {
                        if have == 0 {
                            have = in_(in_desc, &mut next);
                            if have == 0 {
                                next = ptr::null_mut();
                                ret = (-5);
                                break inf_leave ;
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    loop  {
                        if left == 0 {
                            put = state.window;
                            left = state.wsize;
                            state.whave = left;
                            if out(out_desc, put, left) != 0 {
                                ret = (-5);
                                break inf_leave ;
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    if copy > have { copy = have; }
                    if copy > left { copy = left; }
                    memcpy(put as *mut _, next as *const _, copy);
                    have -= copy;
                    next = next.offset(copy);
                    left -= copy;
                    put = put.offset(copy);
                    state.length -= copy;
                }
                state.mode = TYPE
            }
            TABLE => {
                loop  {
                    while bits < 14 {
                        loop  {
                            loop  {
                                if have == 0 {
                                    have = in_(in_desc, &mut next);
                                    if have == 0 {
                                        next = ptr::null_mut();
                                        ret = (-5);
                                        break inf_leave ;
                                    };
                                }
                                if 0 == 0 { break  };
                            }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    if 0 == 0 { break  };
                }
                state.nlen = ((hold as c_uint) & ((1 << 5) - 1)) + 257;
                loop  { hold >>= 5; bits -= 5; if 0 == 0 { break  }; }
                state.ndist = ((hold as c_uint) & ((1 << 5) - 1)) + 1;
                loop  { hold >>= 5; bits -= 5; if 0 == 0 { break  }; }
                state.ncode = ((hold as c_uint) & ((1 << 4) - 1)) + 4;
                loop  { hold >>= 4; bits -= 4; if 0 == 0 { break  }; }
                if state.nlen > 286 || state.ndist > 30 {
                    strm.msg = "too many length or distance symbols";
                    state.mode = BAD;
                }
                state.have = 0;
                while state.have < state.ncode {
                    loop  {
                        while bits < 3 {
                            loop  {
                                loop  {
                                    if have == 0 {
                                        have = in_(in_desc, &mut next);
                                        if have == 0 {
                                            next = ptr::null_mut();
                                            ret = (-5);
                                            break inf_leave ;
                                        };
                                    }
                                    if 0 == 0 { break  };
                                }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    state.lens[order[{
                                         let mut _t = state.have;
                                         state.have += 1;
                                         _t
                                     }]] =
                        ((hold as c_uint) & ((1 << 3) - 1)) as u16;
                    loop  { hold >>= 3; bits -= 3; if 0 == 0 { break  }; };
                }
                while state.have < 19 {
                    state.lens[order[{
                                         let mut _t = state.have;
                                         state.have += 1;
                                         _t
                                     }]] = 0u16;
                }
                state.next = state.codes;
                state.lencode = (state.next) as *const code;
                state.lenbits = 7;
                ret =
                    inflate_table(CODES, state.lens, 19, &mut (state.next),
                                  &mut (state.lenbits), state.work);
                if ret != 0 {
                    strm.msg = "invalid code lengths set";
                    state.mode = BAD;
                }
                state.have = 0;
                while state.have < state.nlen + state.ndist {
                    loop  {
                        here =
                            *state.lencode.offset(((hold as c_uint) &
                                                       ((1 << (state.lenbits))
                                                            - 1)));
                        if ((here.bits) as c_uint) <= bits { break ; }
                        loop  {
                            loop  {
                                if have == 0 {
                                    have = in_(in_desc, &mut next);
                                    if have == 0 {
                                        next = ptr::null_mut();
                                        ret = (-5);
                                        break inf_leave ;
                                    };
                                }
                                if 0 == 0 { break  };
                            }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    if here.val < 16 {
                        loop  {
                            hold >>= (here.bits);
                            bits -= (here.bits) as c_uint;
                            if 0 == 0 { break  };
                        }
                        state.lens[{
                                       let mut _t = state.have;
                                       state.have += 1;
                                       _t
                                   }] = here.val;
                    } else {
                        if here.val == 16 {
                            loop  {
                                while bits < ((here.bits + 2) as c_uint) {
                                    loop  {
                                        loop  {
                                            if have == 0 {
                                                have =
                                                    in_(in_desc, &mut next);
                                                if have == 0 {
                                                    next = ptr::null_mut();
                                                    ret = (-5);
                                                    break inf_leave ;
                                                };
                                            }
                                            if 0 == 0 { break  };
                                        }
                                        have -= 1;
                                        hold +=
                                            ((*{
                                                   let mut _t = next;
                                                   next = next.offset(1);
                                                   _t
                                               }) as c_long) << bits;
                                        bits += 8;
                                        if 0 == 0 { break  };
                                    };
                                }
                                if 0 == 0 { break  };
                            }
                            loop  {
                                hold >>= (here.bits);
                                bits -= (here.bits) as c_uint;
                                if 0 == 0 { break  };
                            }
                            if state.have == 0 {
                                strm.msg = "invalid bit length repeat";
                                state.mode = BAD;
                                break ;
                            }
                            len = (state.lens[state.have - 1]) as c_uint;
                            copy = 3 + ((hold as c_uint) & ((1 << 2) - 1));
                            loop  {
                                hold >>= 2;
                                bits -= 2;
                                if 0 == 0 { break  };
                            };
                        } else if here.val == 17 {
                            loop  {
                                while bits < ((here.bits + 3) as c_uint) {
                                    loop  {
                                        loop  {
                                            if have == 0 {
                                                have =
                                                    in_(in_desc, &mut next);
                                                if have == 0 {
                                                    next = ptr::null_mut();
                                                    ret = (-5);
                                                    break inf_leave ;
                                                };
                                            }
                                            if 0 == 0 { break  };
                                        }
                                        have -= 1;
                                        hold +=
                                            ((*{
                                                   let mut _t = next;
                                                   next = next.offset(1);
                                                   _t
                                               }) as c_long) << bits;
                                        bits += 8;
                                        if 0 == 0 { break  };
                                    };
                                }
                                if 0 == 0 { break  };
                            }
                            loop  {
                                hold >>= (here.bits);
                                bits -= (here.bits) as c_uint;
                                if 0 == 0 { break  };
                            }
                            len = 0;
                            copy = 3 + ((hold as c_uint) & ((1 << 3) - 1));
                            loop  {
                                hold >>= 3;
                                bits -= 3;
                                if 0 == 0 { break  };
                            };
                        } else {
                            loop  {
                                while bits < ((here.bits + 7) as c_uint) {
                                    loop  {
                                        loop  {
                                            if have == 0 {
                                                have =
                                                    in_(in_desc, &mut next);
                                                if have == 0 {
                                                    next = ptr::null_mut();
                                                    ret = (-5);
                                                    break inf_leave ;
                                                };
                                            }
                                            if 0 == 0 { break  };
                                        }
                                        have -= 1;
                                        hold +=
                                            ((*{
                                                   let mut _t = next;
                                                   next = next.offset(1);
                                                   _t
                                               }) as c_long) << bits;
                                        bits += 8;
                                        if 0 == 0 { break  };
                                    };
                                }
                                if 0 == 0 { break  };
                            }
                            loop  {
                                hold >>= (here.bits);
                                bits -= (here.bits) as c_uint;
                                if 0 == 0 { break  };
                            }
                            len = 0;
                            copy = 11 + ((hold as c_uint) & ((1 << 7) - 1));
                            loop  {
                                hold >>= 7;
                                bits -= 7;
                                if 0 == 0 { break  };
                            };
                        }
                        if state.have + copy > state.nlen + state.ndist {
                            strm.msg = "invalid bit length repeat";
                            state.mode = BAD;
                            break ;
                        }
                        while { let mut _t = copy; copy -= 1; _t } != 0 {
                            state.lens[{
                                           let mut _t = state.have;
                                           state.have += 1;
                                           _t
                                       }] = len as u16;
                        };
                    };
                }
                if state.mode == BAD { }
                if state.lens[256] == 0 {
                    strm.msg = "invalid code -- missing end-of-block";
                    state.mode = BAD;
                }
                state.next = state.codes;
                state.lencode = (state.next) as *const code;
                state.lenbits = 9;
                ret =
                    inflate_table(LENS, state.lens, state.nlen,
                                  &mut (state.next), &mut (state.lenbits),
                                  state.work);
                if ret != 0 {
                    strm.msg = "invalid literal/lengths set";
                    state.mode = BAD;
                }
                state.distcode = (state.next) as *const code;
                state.distbits = 6;
                ret =
                    inflate_table(DISTS, state.lens.offset(state.nlen),
                                  state.ndist, &mut (state.next),
                                  &mut (state.distbits), state.work);
                if ret != 0 {
                    strm.msg = "invalid distances set";
                    state.mode = BAD;
                }
                state.mode = LEN;
                if have >= 6 && left >= 258 {
                    loop  {
                        strm.next_out = put;
                        strm.avail_out = left;
                        strm.next_in = next;
                        strm.avail_in = have;
                        state.hold = hold;
                        state.bits = bits;
                        if 0 == 0 { break  };
                    }
                    if state.whave < state.wsize {
                        state.whave = state.wsize - left;
                    }
                    inflate_fast(strm, state.wsize);
                    loop  {
                        put = strm.next_out;
                        left = strm.avail_out;
                        next = strm.next_in;
                        have = strm.avail_in;
                        hold = state.hold;
                        bits = state.bits;
                        if 0 == 0 { break  };
                    };
                }
                loop  {
                    here =
                        *state.lencode.offset(((hold as c_uint) &
                                                   ((1 << (state.lenbits)) -
                                                        1)));
                    if ((here.bits) as c_uint) <= bits { break ; }
                    loop  {
                        loop  {
                            if have == 0 {
                                have = in_(in_desc, &mut next);
                                if have == 0 {
                                    next = ptr::null_mut();
                                    ret = (-5);
                                    break inf_leave ;
                                };
                            }
                            if 0 == 0 { break  };
                        }
                        have -= 1;
                        hold +=
                            ((*{
                                   let mut _t = next;
                                   next = next.offset(1);
                                   _t
                               }) as c_long) << bits;
                        bits += 8;
                        if 0 == 0 { break  };
                    };
                }
                if here.op != 0 && (here.op & 240) == 0 {
                    last = here;
                    loop  {
                        here =
                            *state.lencode.offset(last.val +
                                                      (((hold as c_uint) &
                                                            ((1 <<
                                                                  (last.bits +
                                                                       last.op))
                                                                 - 1)) >>
                                                           last.bits));
                        if ((last.bits + here.bits) as c_uint) <= bits {
                            break ;
                        }
                        loop  {
                            loop  {
                                if have == 0 {
                                    have = in_(in_desc, &mut next);
                                    if have == 0 {
                                        next = ptr::null_mut();
                                        ret = (-5);
                                        break inf_leave ;
                                    };
                                }
                                if 0 == 0 { break  };
                            }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    loop  {
                        hold >>= (last.bits);
                        bits -= (last.bits) as c_uint;
                        if 0 == 0 { break  };
                    };
                }
                loop  {
                    hold >>= (here.bits);
                    bits -= (here.bits) as c_uint;
                    if 0 == 0 { break  };
                }
                state.length = here.val as c_uint;
                if here.op == 0 {
                    loop  {
                        if left == 0 {
                            put = state.window;
                            left = state.wsize;
                            state.whave = left;
                            if out(out_desc, put, left) != 0 {
                                ret = (-5);
                                break inf_leave ;
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    *{ let mut _t = put; put = put.offset(1); _t } =
                        (state.length) as u8;
                    left -= 1;
                    state.mode = LEN;
                }
                if here.op & 32 != 0 { state.mode = TYPE; }
                if here.op & 64 != 0 {
                    strm.msg = "invalid literal/length code";
                    state.mode = BAD;
                }
                state.extra = ((here.op) as c_uint) & 15;
                if state.extra != 0 {
                    loop  {
                        while bits < (state.extra) {
                            loop  {
                                loop  {
                                    if have == 0 {
                                        have = in_(in_desc, &mut next);
                                        if have == 0 {
                                            next = ptr::null_mut();
                                            ret = (-5);
                                            break inf_leave ;
                                        };
                                    }
                                    if 0 == 0 { break  };
                                }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    state.length +=
                        ((hold as c_uint) & ((1 << (state.extra)) - 1));
                    loop  {
                        hold >>= (state.extra);
                        bits -= (state.extra);
                        if 0 == 0 { break  };
                    };
                }
                loop  {
                    here =
                        *state.distcode.offset(((hold as c_uint) &
                                                    ((1 << (state.distbits)) -
                                                         1)));
                    if ((here.bits) as c_uint) <= bits { break ; }
                    loop  {
                        loop  {
                            if have == 0 {
                                have = in_(in_desc, &mut next);
                                if have == 0 {
                                    next = ptr::null_mut();
                                    ret = (-5);
                                    break inf_leave ;
                                };
                            }
                            if 0 == 0 { break  };
                        }
                        have -= 1;
                        hold +=
                            ((*{
                                   let mut _t = next;
                                   next = next.offset(1);
                                   _t
                               }) as c_long) << bits;
                        bits += 8;
                        if 0 == 0 { break  };
                    };
                }
                if (here.op & 240) == 0 {
                    last = here;
                    loop  {
                        here =
                            *state.distcode.offset(last.val +
                                                       (((hold as c_uint) &
                                                             ((1 <<
                                                                   (last.bits
                                                                        +
                                                                        last.op))
                                                                  - 1)) >>
                                                            last.bits));
                        if ((last.bits + here.bits) as c_uint) <= bits {
                            break ;
                        }
                        loop  {
                            loop  {
                                if have == 0 {
                                    have = in_(in_desc, &mut next);
                                    if have == 0 {
                                        next = ptr::null_mut();
                                        ret = (-5);
                                        break inf_leave ;
                                    };
                                }
                                if 0 == 0 { break  };
                            }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    loop  {
                        hold >>= (last.bits);
                        bits -= (last.bits) as c_uint;
                        if 0 == 0 { break  };
                    };
                }
                loop  {
                    hold >>= (here.bits);
                    bits -= (here.bits) as c_uint;
                    if 0 == 0 { break  };
                }
                if here.op & 64 != 0 {
                    strm.msg = "invalid distance code";
                    state.mode = BAD;
                }
                state.offset = here.val as c_uint;
                state.extra = ((here.op) as c_uint) & 15;
                if state.extra != 0 {
                    loop  {
                        while bits < (state.extra) {
                            loop  {
                                loop  {
                                    if have == 0 {
                                        have = in_(in_desc, &mut next);
                                        if have == 0 {
                                            next = ptr::null_mut();
                                            ret = (-5);
                                            break inf_leave ;
                                        };
                                    }
                                    if 0 == 0 { break  };
                                }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    state.offset +=
                        ((hold as c_uint) & ((1 << (state.extra)) - 1));
                    loop  {
                        hold >>= (state.extra);
                        bits -= (state.extra);
                        if 0 == 0 { break  };
                    };
                }
                if state.offset >
                       state.wsize -
                           if state.whave < state.wsize { left } else { 0 } {
                    strm.msg = "invalid distance too far back";
                    state.mode = BAD;
                }
                loop  {
                    loop  {
                        if left == 0 {
                            put = state.window;
                            left = state.wsize;
                            state.whave = left;
                            if out(out_desc, put, left) != 0 {
                                ret = (-5);
                                break inf_leave ;
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    copy = state.wsize - state.offset;
                    if copy < left {
                        from = put.offset(copy);
                        copy = left - copy;
                    } else { from = put.offset(-state.offset); copy = left; }
                    if copy > state.length { copy = state.length; }
                    state.length -= copy;
                    left -= copy;
                    loop  {
                        *{ let mut _t = put; put = put.offset(1); _t } =
                            *{ let mut _t = from; from = from.offset(1); _t };
                        if { copy -= 1; copy } == 0 { break  };
                    }
                    if state.length == 0 { break  };
                }
            }
            LEN => {
                if have >= 6 && left >= 258 {
                    loop  {
                        strm.next_out = put;
                        strm.avail_out = left;
                        strm.next_in = next;
                        strm.avail_in = have;
                        state.hold = hold;
                        state.bits = bits;
                        if 0 == 0 { break  };
                    }
                    if state.whave < state.wsize {
                        state.whave = state.wsize - left;
                    }
                    inflate_fast(strm, state.wsize);
                    loop  {
                        put = strm.next_out;
                        left = strm.avail_out;
                        next = strm.next_in;
                        have = strm.avail_in;
                        hold = state.hold;
                        bits = state.bits;
                        if 0 == 0 { break  };
                    };
                }
                loop  {
                    here =
                        *state.lencode.offset(((hold as c_uint) &
                                                   ((1 << (state.lenbits)) -
                                                        1)));
                    if ((here.bits) as c_uint) <= bits { break ; }
                    loop  {
                        loop  {
                            if have == 0 {
                                have = in_(in_desc, &mut next);
                                if have == 0 {
                                    next = ptr::null_mut();
                                    ret = (-5);
                                    break inf_leave ;
                                };
                            }
                            if 0 == 0 { break  };
                        }
                        have -= 1;
                        hold +=
                            ((*{
                                   let mut _t = next;
                                   next = next.offset(1);
                                   _t
                               }) as c_long) << bits;
                        bits += 8;
                        if 0 == 0 { break  };
                    };
                }
                if here.op != 0 && (here.op & 240) == 0 {
                    last = here;
                    loop  {
                        here =
                            *state.lencode.offset(last.val +
                                                      (((hold as c_uint) &
                                                            ((1 <<
                                                                  (last.bits +
                                                                       last.op))
                                                                 - 1)) >>
                                                           last.bits));
                        if ((last.bits + here.bits) as c_uint) <= bits {
                            break ;
                        }
                        loop  {
                            loop  {
                                if have == 0 {
                                    have = in_(in_desc, &mut next);
                                    if have == 0 {
                                        next = ptr::null_mut();
                                        ret = (-5);
                                        break inf_leave ;
                                    };
                                }
                                if 0 == 0 { break  };
                            }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    loop  {
                        hold >>= (last.bits);
                        bits -= (last.bits) as c_uint;
                        if 0 == 0 { break  };
                    };
                }
                loop  {
                    hold >>= (here.bits);
                    bits -= (here.bits) as c_uint;
                    if 0 == 0 { break  };
                }
                state.length = here.val as c_uint;
                if here.op == 0 {
                    loop  {
                        if left == 0 {
                            put = state.window;
                            left = state.wsize;
                            state.whave = left;
                            if out(out_desc, put, left) != 0 {
                                ret = (-5);
                                break inf_leave ;
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    *{ let mut _t = put; put = put.offset(1); _t } =
                        (state.length) as u8;
                    left -= 1;
                    state.mode = LEN;
                }
                if here.op & 32 != 0 { state.mode = TYPE; }
                if here.op & 64 != 0 {
                    strm.msg = "invalid literal/length code";
                    state.mode = BAD;
                }
                state.extra = ((here.op) as c_uint) & 15;
                if state.extra != 0 {
                    loop  {
                        while bits < (state.extra) {
                            loop  {
                                loop  {
                                    if have == 0 {
                                        have = in_(in_desc, &mut next);
                                        if have == 0 {
                                            next = ptr::null_mut();
                                            ret = (-5);
                                            break inf_leave ;
                                        };
                                    }
                                    if 0 == 0 { break  };
                                }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    state.length +=
                        ((hold as c_uint) & ((1 << (state.extra)) - 1));
                    loop  {
                        hold >>= (state.extra);
                        bits -= (state.extra);
                        if 0 == 0 { break  };
                    };
                }
                loop  {
                    here =
                        *state.distcode.offset(((hold as c_uint) &
                                                    ((1 << (state.distbits)) -
                                                         1)));
                    if ((here.bits) as c_uint) <= bits { break ; }
                    loop  {
                        loop  {
                            if have == 0 {
                                have = in_(in_desc, &mut next);
                                if have == 0 {
                                    next = ptr::null_mut();
                                    ret = (-5);
                                    break inf_leave ;
                                };
                            }
                            if 0 == 0 { break  };
                        }
                        have -= 1;
                        hold +=
                            ((*{
                                   let mut _t = next;
                                   next = next.offset(1);
                                   _t
                               }) as c_long) << bits;
                        bits += 8;
                        if 0 == 0 { break  };
                    };
                }
                if (here.op & 240) == 0 {
                    last = here;
                    loop  {
                        here =
                            *state.distcode.offset(last.val +
                                                       (((hold as c_uint) &
                                                             ((1 <<
                                                                   (last.bits
                                                                        +
                                                                        last.op))
                                                                  - 1)) >>
                                                            last.bits));
                        if ((last.bits + here.bits) as c_uint) <= bits {
                            break ;
                        }
                        loop  {
                            loop  {
                                if have == 0 {
                                    have = in_(in_desc, &mut next);
                                    if have == 0 {
                                        next = ptr::null_mut();
                                        ret = (-5);
                                        break inf_leave ;
                                    };
                                }
                                if 0 == 0 { break  };
                            }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    loop  {
                        hold >>= (last.bits);
                        bits -= (last.bits) as c_uint;
                        if 0 == 0 { break  };
                    };
                }
                loop  {
                    hold >>= (here.bits);
                    bits -= (here.bits) as c_uint;
                    if 0 == 0 { break  };
                }
                if here.op & 64 != 0 {
                    strm.msg = "invalid distance code";
                    state.mode = BAD;
                }
                state.offset = here.val as c_uint;
                state.extra = ((here.op) as c_uint) & 15;
                if state.extra != 0 {
                    loop  {
                        while bits < (state.extra) {
                            loop  {
                                loop  {
                                    if have == 0 {
                                        have = in_(in_desc, &mut next);
                                        if have == 0 {
                                            next = ptr::null_mut();
                                            ret = (-5);
                                            break inf_leave ;
                                        };
                                    }
                                    if 0 == 0 { break  };
                                }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    state.offset +=
                        ((hold as c_uint) & ((1 << (state.extra)) - 1));
                    loop  {
                        hold >>= (state.extra);
                        bits -= (state.extra);
                        if 0 == 0 { break  };
                    };
                }
                if state.offset >
                       state.wsize -
                           if state.whave < state.wsize { left } else { 0 } {
                    strm.msg = "invalid distance too far back";
                    state.mode = BAD;
                }
                loop  {
                    loop  {
                        if left == 0 {
                            put = state.window;
                            left = state.wsize;
                            state.whave = left;
                            if out(out_desc, put, left) != 0 {
                                ret = (-5);
                                break inf_leave ;
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    copy = state.wsize - state.offset;
                    if copy < left {
                        from = put.offset(copy);
                        copy = left - copy;
                    } else { from = put.offset(-state.offset); copy = left; }
                    if copy > state.length { copy = state.length; }
                    state.length -= copy;
                    left -= copy;
                    loop  {
                        *{ let mut _t = put; put = put.offset(1); _t } =
                            *{ let mut _t = from; from = from.offset(1); _t };
                        if { copy -= 1; copy } == 0 { break  };
                    }
                    if state.length == 0 { break  };
                }
            }
            DONE => {
                ret = Z_STREAM_END;
                if left < state.wsize {
                    if out(out_desc, state.window, state.wsize - left) != 0 {
                        ret = Z_BUF_ERROR;
                    };
                }
                break inf_leave
            }
            BAD => { ret = Z_DATA_ERROR; break inf_leave  }
            _ => { ret = Z_STREAM_ERROR; break inf_leave  }
        }
    }
    inf_leave: loop  { strm.next_in = next; break  }
    strm.avail_in = have;
    return ret;
}
#[no_mangle]
pub unsafe extern "C" fn inflateBackEnd(mut strm: z_streamp) -> c_int {
    if strm == Z_NULL || strm.state == Z_NULL ||
           strm.zfree == (0 as free_func) {
        return Z_STREAM_ERROR;
    }
    (*((strm).zfree))((strm).opaque, (strm.state) as voidpf);
    strm.state = Z_NULL;
    return Z_OK;
}
