/* gzlib.c -- zlib functions common to reading and writing gzip files
 * Copyright (C) 2004-2017 Mark Adler
 * For conditions of distribution and use, see copyright notice in zlib.h
 */
/* Local functions */
/* Map the Windows error number in ERROR to a locale-dependent error message
   string and return a pointer to it.  Typically, the values for ERROR come
   from GetLastError.

   The string pointed to shall not be modified by the application, but may be
   overwritten by a subsequent call to gz_strwinerror

   The gz_strwinerror function does not change the current setting of
   GetLastError. */
/* Default language */
/* If there is an \r\n appended, zap it.  */
/* UNDER_CE */
/* Reset gzip file state */
/* no output data available */
pub static GZ_READ: c_int = 7247;
/* for reading ... */
/* not at end of file */
/* have not read past end yet */
pub static LOOK: c_int = 0;
/* look for gzip header */
/* no seek request pending */
pub static Z_OK: c_int = 0;
unsafe fn gz_reset(mut state: gz_statep) {
    state.x.have = 0; /* clear error */
    if state.mode == GZ_READ {
        state.eof = 0; /* no uncompressed data yet */
        state.past = 0; /* no input data yet */
        state.how = LOOK;
    }
    state.seek = 0;
    gz_error(state, Z_OK, ptr::null_mut());
    state.x.pos = 0;
    state.strm.avail_in = 0;
}
/* Open a gzip file either by name or file descriptor. */
/* check input */
/* allocate gzFile structure to return */
/* no buffers allocated yet */
pub static GZBUFSIZE: c_uint = 8192;
/* requested buffer size */
/* no error message yet */
/* interpret mode */
pub static GZ_NONE: c_int = 0;
pub static Z_DEFAULT_COMPRESSION: c_int = -1;
pub static Z_DEFAULT_STRATEGY: c_int = 0;
pub static GZ_WRITE: c_int = 31153;
pub static GZ_APPEND: c_int = 1;
/* can't read and write at the same time */
/* ignore -- will request binary anyway */
pub static Z_FILTERED: c_int = 1;
pub static Z_HUFFMAN_ONLY: c_int = 2;
pub static Z_RLE: c_int = 3;
pub static Z_FIXED: c_int = 4;
/* could consider as an error, but just ignore */
/* must provide an "r", "w", or "a" */
/* can't force transparent read */
/* for empty file */
/* save the path name for error messages */
/* compute the flags for open() */
pub static O_CLOEXEC: c_int = 16777216;
pub static O_RDONLY: c_int = 0;
pub static O_WRONLY: c_int = 1;
pub static O_CREAT: c_int = 512;
pub static O_EXCL: c_int = 2048;
pub static O_TRUNC: c_int = 1024;
pub static O_APPEND: c_int = 8;
pub static LSEEK: *mut unsafe extern "C" fn(, ...) =
     /* open the file with the appropriate flags (or just use fd) */
    lseek;
pub static SEEK_END: c_int = 2;
/* so gzoffset() is correct */
/* simplify later checks */
/* save the current position for rewinding (only if reading) */
pub static SEEK_CUR: c_int = 1;
unsafe fn gz_open(mut path: *const c_void, mut fd: c_int, mut mode: *const i8)
 -> gzFile {
    let mut cloexec = 0; /* initialize stream */
    let mut exclusive = 0; /* return stream */
    if path.is_null() { return ptr::null_mut(); }
    let mut state = malloc(std::mem::size_of::<gz_state>()) as gz_statep;
    if state.is_null() { return ptr::null_mut(); }
    state.size = 0;
    state.want = GZBUFSIZE;
    state.msg = ptr::null_mut();
    state.mode = GZ_NONE;
    state.level = Z_DEFAULT_COMPRESSION;
    state.strategy = Z_DEFAULT_STRATEGY;
    state.direct = 0;
    while *mode != 0 {
        if *mode >= '0' && *mode <= '9' {
            state.level = *mode - '0';
        } else {
            match *mode {
                'r' => state.mode = GZ_READ,
                'w' => state.mode = GZ_WRITE,
                'a' => state.mode = GZ_APPEND,
                '+' => { free(state); return ptr::null_mut() }
                'b' => { }
                'e' => cloexec = 1,
                'x' => exclusive = 1,
                'f' => state.strategy = Z_FILTERED,
                'h' => state.strategy = Z_HUFFMAN_ONLY,
                'R' => state.strategy = Z_RLE,
                'F' => state.strategy = Z_FIXED,
                'T' => state.direct = 1,
                _ => { }
            };
        }
        mode = mode.offset(1);
    }
    if state.mode == GZ_NONE { free(state); return ptr::null_mut(); }
    if state.mode == GZ_READ {
        if state.direct != 0 { free(state); return ptr::null_mut(); }
        state.direct = 1;
    }
    let mut len = strlen(path as *const i8);
    state.path = malloc(len + 1) as *mut i8;
    if state.path.is_null() { free(state); return ptr::null_mut(); }
    __builtin___snprintf_chk(state.path, len + 1, 0,
                             __builtin_object_size(state.path as *const _,
                                                   if 2 > 1 { 1 } else { 0 }),
                             "%s", path as *const i8);
    let mut oflag =
        if cloexec != 0 { O_CLOEXEC } else { 0 } |
            if state.mode == GZ_READ {
                O_RDONLY
            } else {
                O_WRONLY | O_CREAT | if exclusive != 0 { O_EXCL } else { 0 } |
                    if state.mode == GZ_WRITE { O_TRUNC } else { O_APPEND }
            };
    state.fd = if fd > -1 { fd } else { open(path as *const i8, oflag, 438) };
    if state.fd == -1 {
        free(state.path as *mut _);
        free(state);
        return ptr::null_mut();
    }
    if state.mode == GZ_APPEND {
        LSEEK(state.fd, 0, SEEK_END);
        state.mode = GZ_WRITE;
    }
    if state.mode == GZ_READ {
        state.start = LSEEK(state.fd, 0, SEEK_CUR);
        if state.start == -1 { state.start = 0; };
    }
    gz_reset(state);
    return state as gzFile;
}
/* -- see zlib.h -- */
#[no_mangle]
pub unsafe extern "C" fn gzopen(mut path: *const i8, mut mode: *const i8)
 -> gzFile {
    return gz_open(path as *const _, -1, mode);
}
/* -- see zlib.h -- */
#[no_mangle]
pub unsafe extern "C" fn gzopen64(mut path: *const i8, mut mode: *const i8)
 -> gzFile {
    return gz_open(path as *const _, -1, mode);
}
/* -- see zlib.h -- */
#[no_mangle]
pub unsafe extern "C" fn gzdopen(mut fd: c_int, mut mode: *const i8)
 -> gzFile {
    let mut path: *mut i8; /* identifier for error messages */
    if fd == -1 ||
           {
               path =
                   malloc(7 + 3 * std::mem::size_of::<c_int>()) as
                       *mut i8; /* for debugging */
               path
           }.is_null() {
        return ptr::null_mut();
    }
    __builtin___snprintf_chk(path, 7 + 3 * std::mem::size_of::<c_int>(), 0,
                             __builtin_object_size(path as *const _,
                                                   if 2 > 1 { 1 } else { 0 }),
                             "<fd:%d>", fd);
    let mut gz = gz_open(path as *const _, fd, mode);
    free(path as *mut _);
    return gz;
}
/* -- see zlib.h -- */
/* -- see zlib.h -- */
#[no_mangle]
pub unsafe extern "C" fn gzbuffer(mut file: gzFile, mut size: c_uint)
 -> c_int { /* get internal structure and check integrity */
    if file.is_null() {
        return -1; /* make sure we haven't already allocated memory */
    } /* check and set requested size */
    let mut state = file as gz_statep; /* need to be able to double it */
    if state.mode != GZ_READ && state.mode != GZ_WRITE {
        return -1; /* need two bytes to check magic header */
    }
    if state.size != 0 { return -1; }
    if (size << 1) < size { return -1; }
    if size < 2 { size = 2; }
    state.want = size;
    return 0;
}
/* -- see zlib.h -- */
/* get internal structure */
/* check that we're reading and that there's no error */
pub static Z_BUF_ERROR: c_int = -5;
/* back up and start over */
pub static SEEK_SET: c_int = 0;
#[no_mangle]
pub unsafe extern "C" fn gzrewind(mut file: gzFile) -> c_int {
    if file.is_null() { return -1; }
    let mut state = file as gz_statep;
    if state.mode != GZ_READ ||
           (state.err != Z_OK && state.err != Z_BUF_ERROR) {
        return -1;
    }
    if LSEEK(state.fd, state.start, SEEK_SET) == -1 { return -1; }
    gz_reset(state);
    return 0;
}
/* -- see zlib.h -- */
/* get internal structure and check integrity */
/* check that there's no error */
/* can only seek from start or relative to current position */
/* normalize offset to a SEEK_CUR specification */
/* if within raw area while reading, just go there */
pub static COPY: c_int = 1;
#[no_mangle]
pub unsafe extern "C" fn gzseek64(mut file: gzFile, mut offset: c_long,
                                  mut whence: c_int) -> c_long {
    let mut n: c_uint;
    let mut ret: c_long;
    if file.is_null() { return -1; }
    let mut state = file as gz_statep;
    if state.mode != GZ_READ && state.mode != GZ_WRITE { return -1; }
    if state.err != Z_OK && state.err != Z_BUF_ERROR { return -1; }
    if whence != SEEK_SET && whence != SEEK_CUR { return -1; }
    if whence == SEEK_SET {
        offset -= state.x.pos;
    } else if state.seek != 0 { offset += state.skip; }
    state.seek = 0;
    if state.mode == GZ_READ && state.how == COPY && state.x.pos + offset >= 0
       {
        ret = LSEEK(state.fd, offset - state.x.have, SEEK_CUR);
        if ret == -1 { return -1; }
        state.x.have = 0;
        state.eof = 0;
        state.past = 0;
        state.seek = 0;
        gz_error(state, Z_OK, ptr::null_mut());
        state.strm.avail_in = 0;
        state.x.pos += offset;
        return state.x.pos;
    }
    /* calculate skip amount, rewinding if needed for back seek when reading */
    if offset < 0 {
        if state.mode != GZ_READ  /* writing -- can't go backwards */
           {
            return -1; /* before start of file! */
        } /* rewind, then skip to offset */
        offset +=
            state.x.pos; /* if reading, skip what's in output buffer (one less gzgetc() check) */
        if offset < 0 {
            return -1; /* request skip (if not zero) */
        }
        if gzrewind(file) == -1 { return -1; };
    }
    if state.mode == GZ_READ {
        n =
            if (std::mem::size_of::<c_int>() == std::mem::size_of::<c_long>()
                    && (state.x.have) > 2147483647) ||
                   (state.x.have as c_long) > offset {
                offset as c_uint
            } else { state.x.have };
        state.x.have -= n;
        state.x.next = state.x.next.offset(n);
        state.x.pos += n;
        offset -= n;
    }
    if offset != 0 { state.seek = 1; state.skip = offset; }
    return state.x.pos + offset;
}
/* -- see zlib.h -- */
#[no_mangle]
pub unsafe extern "C" fn gzseek(mut file: gzFile, mut offset: c_long,
                                mut whence: c_int) -> c_long {
    let mut ret = gzseek64(file, offset, whence);
    return if ret == ret { ret } else { -1 };
}
/* -- see zlib.h -- */
#[no_mangle]
pub unsafe extern "C" fn gztell64(mut file: gzFile)
 -> c_long { /* get internal structure and check integrity */
    if file.is_null() {
        return -1; /* return position */
    }
    let mut state = file as gz_statep;
    if state.mode != GZ_READ && state.mode != GZ_WRITE { return -1; }
    return state.x.pos + if state.seek != 0 { state.skip } else { 0 };
}
/* -- see zlib.h -- */
#[no_mangle]
pub unsafe extern "C" fn gztell(mut file: gzFile) -> c_long {
    let mut ret = gztell64(file);
    return if ret == ret { ret } else { -1 };
}
/* -- see zlib.h -- */
#[no_mangle]
pub unsafe extern "C" fn gzoffset64(mut file: gzFile)
 -> c_long { /* get internal structure and check integrity */
    if file.is_null() {
        return -1; /* compute and return effective offset in file */
    } /* reading */
    let mut state = file as gz_statep; /* don't count buffered input */
    if state.mode != GZ_READ && state.mode != GZ_WRITE { return -1; }
    let mut offset = LSEEK(state.fd, 0, SEEK_CUR);
    if offset == -1 { return -1; }
    if state.mode == GZ_READ { offset -= state.strm.avail_in; }
    return offset;
}
/* -- see zlib.h -- */
#[no_mangle]
pub unsafe extern "C" fn gzoffset(mut file: gzFile) -> c_long {
    let mut ret = gzoffset64(file);
    return if ret == ret { ret } else { -1 };
}
/* -- see zlib.h -- */
#[no_mangle]
pub unsafe extern "C" fn gzeof(mut file: gzFile)
 -> c_int { /* get internal structure and check integrity */
    if file.is_null() {
        return 0; /* return end-of-file state */
    }
    let mut state = file as gz_statep;
    if state.mode != GZ_READ && state.mode != GZ_WRITE { return 0; }
    return if state.mode == GZ_READ { state.past } else { 0 };
}
/* -- see zlib.h -- */
/* get internal structure and check integrity */
/* return error information */
pub static Z_MEM_ERROR: c_int = -4;
#[no_mangle]
pub unsafe extern "C" fn gzerror(mut file: gzFile, mut errnum: *mut c_int)
 -> *const i8 {
    if file.is_null() { return ptr::null(); }
    let mut state = file as gz_statep;
    if state.mode != GZ_READ && state.mode != GZ_WRITE { return ptr::null(); }
    if !errnum.is_null() { *errnum = state.err; }
    return if state.err == Z_MEM_ERROR {
               "out of memory"
           } else if state.msg.is_null() { "" } else { state.msg };
}
/* -- see zlib.h -- */
#[no_mangle]
pub unsafe extern "C" fn gzclearerr(mut file:
                                        gzFile) { /* get internal structure and check integrity */
    if file.is_null() {
        return; /* clear error and end-of-file */
    }
    let mut state = file as gz_statep;
    if state.mode != GZ_READ && state.mode != GZ_WRITE { return; }
    if state.mode == GZ_READ { state.eof = 0; state.past = 0; }
    gz_error(state, Z_OK, ptr::null_mut());
}
/* Create an error message in allocated memory and set state->err and
   state->msg accordingly.  Free any previous error message already there.  Do
   not try to free or allocate space if the error is Z_MEM_ERROR (out of
   memory).  Simply save the error message as a static string.  If there is an
   allocation failure constructing the error message, then convert the error to
   out of memory. */
#[no_mangle]
pub unsafe extern "C" fn gz_error(mut state: gz_statep, mut err: c_int,
                                  mut msg:
                                      *const i8) { /* free previously allocated message and clear */
    if !state.msg.is_null() {
        if state.err != Z_MEM_ERROR {
            free(state.msg as
                     *mut _); /* if fatal, set state->x.have to 0 so that the gzgetc() macro fails */
        } /* set error code, and if no message, then done */
        state.msg =
            ptr::null_mut(); /* for an out of memory error, return literal string when requested */
    } /* construct error message with path */
    if err != Z_OK && err != Z_BUF_ERROR { state.x.have = 0; }
    state.err = err;
    if msg.is_null() { return; }
    if err == Z_MEM_ERROR { return; }
    if {
           state.msg =
               malloc(strlen(state.path) + strlen(msg) + 3) as *mut i8;
           state.msg
       }.is_null() {
        state.err = Z_MEM_ERROR;
        return;
    }
    __builtin___snprintf_chk(state.msg, strlen(state.path) + strlen(msg) + 3,
                             0,
                             __builtin_object_size(state.msg as *const _,
                                                   if 2 > 1 { 1 } else { 0 }),
                             "%s%s%s", state.path, ": ", msg);
}
