/* inflate.c -- zlib decompression
 * Copyright (C) 1995-2016 Mark Adler
 * For conditions of distribution and use, see copyright notice in zlib.h
 */
/*
 * Change history:
 *
 * 1.2.beta0    24 Nov 2002
 * - First version -- complete rewrite of inflate to simplify code, avoid
 *   creation of window when not needed, minimize use of window when it is
 *   needed, make inffast.c even faster, implement gzip decoding, and to
 *   improve code readability and style over the previous zlib inflate code
 *
 * 1.2.beta1    25 Nov 2002
 * - Use pointers for available input and output checking in inffast.c
 * - Remove input and output counters in inffast.c
 * - Change inffast.c entry and loop from avail_in >= 7 to >= 6
 * - Remove unnecessary second byte pull from length extra in inffast.c
 * - Unroll direct copy to three copies per loop in inffast.c
 *
 * 1.2.beta2    4 Dec 2002
 * - Change external routine names to reduce potential conflicts
 * - Correct filename to inffixed.h for fixed tables in inflate.c
 * - Make hbuf[] unsigned char to match parameter type in inflate.c
 * - Change strm->next_out[-state->offset] to *(strm->next_out - state->offset)
 *   to avoid negation problem on Alphas (64 bit) in inflate.c
 *
 * 1.2.beta3    22 Dec 2002
 * - Add comments on state->bits assertion in inffast.c
 * - Add comments on op field in inftrees.h
 * - Fix bug in reuse of allocated window after inflateReset()
 * - Remove bit fields--back to byte structure for speed
 * - Remove distance extra == 0 check in inflate_fast()--only helps for lengths
 * - Change post-increments to pre-increments in inflate_fast(), PPC biased?
 * - Add compile time option, POSTINC, to use post-increments instead (Intel?)
 * - Make MATCH copy in inflate() much faster for when inflate_fast() not used
 * - Use local copies of stream next and avail values, as well as local bit
 *   buffer and bit count in inflate()--for speed when inflate_fast() not used
 *
 * 1.2.beta4    1 Jan 2003
 * - Split ptr - 257 statements in inflate_table() to avoid compiler warnings
 * - Move a comment on output buffer sizes from inffast.c to inflate.c
 * - Add comments in inffast.c to introduce the inflate_fast() routine
 * - Rearrange window copies in inflate_fast() for speed and simplification
 * - Unroll last copy for window match in inflate_fast()
 * - Use local copies of window variables in inflate_fast() for speed
 * - Pull out common wnext == 0 case for speed in inflate_fast()
 * - Make op and len in inflate_fast() unsigned for consistency
 * - Add FAR to lcode and dcode declarations in inflate_fast()
 * - Simplified bad distance check in inflate_fast()
 * - Added inflateBackInit(), inflateBack(), and inflateBackEnd() in new
 *   source file infback.c to provide a call-back interface to inflate for
 *   programs like gzip and unzip -- uses window as output buffer to avoid
 *   window copying
 *
 * 1.2.beta5    1 Jan 2003
 * - Improved inflateBack() interface to allow the caller to provide initial
 *   input in strm.
 * - Fixed stored blocks bug in inflateBack()
 *
 * 1.2.beta6    4 Jan 2003
 * - Added comments in inffast.c on effectiveness of POSTINC
 * - Typecasting all around to reduce compiler warnings
 * - Changed loops from while (1) or do {} while (1) to for (;;), again to
 *   make compilers happy
 * - Changed type of window in inflateBackInit() to unsigned char *
 *
 * 1.2.beta7    27 Jan 2003
 * - Changed many types to unsigned or unsigned short to avoid warnings
 * - Added inflateCopy() function
 *
 * 1.2.0        9 Mar 2003
 * - Changed inflateBack() interface to provide separate opaque descriptors
 *   for the in() and out() functions
 * - Changed inflateBack() argument and in_func typedef to swap the length
 *   and buffer address return values for the input function
 * - Check next_in and next_out for Z_NULL on entry to inflate()
 *
 * The history for versions after 1.2.0 are in ChangeLog in zlib distribution.
 */
/* function prototypes */
pub static Z_NULL: z_streamp = 0;
unsafe fn inflateStateCheck(mut strm: z_streamp) -> c_int {
    if strm == Z_NULL || strm.zalloc == (0 as alloc_func) ||
           strm.zfree == (0 as free_func) {
        return 1; /* to support ill-conceived Java test suite */
    } /* get the state */
    let mut state =
        strm.state as
            *mut inflate_state; /* extract wrap request from windowBits parameter */
    if state == Z_NULL || state.strm != strm || state.mode < HEAD ||
           state.mode > SYNC {
        return 1; /* set number of window bits, free window if different */
    } /* update state and reset the rest of it */
    return 0; /* in case we return an error */
}
pub static Z_STREAM_ERROR: c_int = -2;
pub static Z_OK: c_int = 0;
#[no_mangle]
pub unsafe extern "C" fn inflateResetKeep(mut strm: z_streamp) -> c_int {
    if inflateStateCheck(strm) != 0 {
        return Z_STREAM_ERROR; /* to pass state test in inflateReset2() */
    }
    let mut state = strm.state as *mut inflate_state;
    strm.total_in =
        { strm.total_out = { state.total = 0; state.total }; strm.total_out };
    strm.msg = Z_NULL;
    if state.wrap != 0 { strm.adler = state.wrap & 1; }
    state.mode = HEAD;
    state.last = 0;
    state.havedict = 0;
    state.dmax = 32768;
    state.head = Z_NULL;
    state.hold = 0;
    state.bits = 0;
    state.lencode =
        {
            state.distcode = { state.next = state.codes; state.next };
            state.distcode
        };
    state.sane = 1;
    state.back = -1;
    return Z_OK;
}
#[no_mangle]
pub unsafe extern "C" fn inflateReset(mut strm: z_streamp) -> c_int {
    if inflateStateCheck(strm) != 0 { return Z_STREAM_ERROR; }
    let mut state = strm.state as *mut inflate_state;
    state.wsize = 0;
    state.whave = 0;
    state.wnext = 0;
    return inflateResetKeep(strm);
}
#[no_mangle]
pub unsafe extern "C" fn inflateReset2(mut strm: z_streamp,
                                       mut windowBits: c_int) -> c_int {
    let mut wrap;
    if inflateStateCheck(strm) != 0 { return Z_STREAM_ERROR; }
    let mut state = strm.state as *mut inflate_state;
    if windowBits < 0 {
        wrap = 0;
        windowBits = -windowBits;
    } else {
        wrap = (windowBits >> 4) + 5;
        if windowBits < 48 { windowBits &= 15; };
    }
    if windowBits != 0 && (windowBits < 8 || windowBits > 15) {
        return Z_STREAM_ERROR;
    }
    if state.window != Z_NULL && state.wbits != (windowBits as c_uint) {
        (*((strm).zfree))((strm).opaque, (state.window) as voidpf);
        state.window = Z_NULL;
    }
    state.wrap = wrap;
    state.wbits = windowBits as c_uint;
    return inflateReset(strm);
}
pub static ZLIB_VERSION: *mut i8 = "1.2.11";
pub static Z_VERSION_ERROR: c_int = -6;
pub static Z_MEM_ERROR: c_int = -4;
#[no_mangle]
pub unsafe extern "C" fn inflateInit2_(mut strm: z_streamp,
                                       mut windowBits: c_int,
                                       mut version: *const i8,
                                       mut stream_size: c_int) -> c_int {
    if version == Z_NULL || *version.offset(0) != ZLIB_VERSION[0] ||
           stream_size != (std::mem::size_of::<z_stream>() as c_int) {
        return Z_VERSION_ERROR;
    }
    if strm == Z_NULL { return Z_STREAM_ERROR; }
    strm.msg = Z_NULL;
    if strm.zalloc == (0 as alloc_func) {
        strm.zalloc = zcalloc;
        strm.opaque = 0 as voidpf;
    }
    if strm.zfree == (0 as free_func) { strm.zfree = zcfree; }
    let mut state =
        (*((strm).zalloc))((strm).opaque, 1,
                           std::mem::size_of::<inflate_state>()) as
            *mut inflate_state;
    if state == Z_NULL { return Z_MEM_ERROR; }
    strm.state = state as *mut internal_state;
    state.strm = strm;
    state.window = Z_NULL;
    state.mode = HEAD;
    let mut ret = inflateReset2(strm, windowBits);
    if ret != Z_OK {
        (*((strm).zfree))((strm).opaque, (state) as voidpf);
        strm.state = Z_NULL;
    }
    return ret;
}
pub static DEF_WBITS: c_int = 15;
#[no_mangle]
pub unsafe extern "C" fn inflateInit_(mut strm: z_streamp,
                                      mut version: *const i8,
                                      mut stream_size: c_int) -> c_int {
    return inflateInit2_(strm, DEF_WBITS, version, stream_size);
}
#[no_mangle]
pub unsafe extern "C" fn inflatePrime(mut strm: z_streamp, mut bits: c_int,
                                      mut value: c_int) -> c_int {
    if inflateStateCheck(strm) != 0 { return Z_STREAM_ERROR; }
    let mut state = strm.state as *mut inflate_state;
    if bits < 0 { state.hold = 0; state.bits = 0; return Z_OK; }
    if bits > 16 || state.bits + (bits as uInt) > 32 {
        return Z_STREAM_ERROR;
    }
    value &= (1 << bits) - 1;
    state.hold += (value as c_uint) << state.bits;
    state.bits += bits as uInt;
    return Z_OK;
}
/*
   Return state with length and distance decoding tables and index sizes set to
   fixed code decoding.  Normally this returns fixed tables from inffixed.h.
   If BUILDFIXED is defined, then instead this routine builds the tables the
   first time it's called, and returns those tables the first time and
   thereafter.  This reduces the size of the code by about 2K bytes, in
   exchange for a little execution time.  However, BUILDFIXED should not be
   used for threaded applications, since the rewriting of the tables and virgin
   may not be thread-safe.
 */
unsafe fn fixedtables(mut state:
                          &mut inflate_state) { /* build fixed huffman tables if first call (may not be thread safe) */
    /* literal/length table */
    /* distance table */
    /* do this just once */
    /* !BUILDFIXED */
    /* BUILDFIXED */
    state.lencode = lenfix;
    state.lenbits = 9;
    state.distcode = distfix;
    state.distbits = 5;
}
/*
   Write out the inffixed.h that is #include'd above.  Defining MAKEFIXED also
   defines BUILDFIXED, so the tables are built on the fly.  makefixed() writes
   those tables to stdout, which would be piped to inffixed.h.  A small program
   can simply call makefixed to do this:

    void makefixed(void);

    int main(void)
    {
        makefixed();
        return 0;
    }

   Then that can be linked with zlib built with MAKEFIXED defined and run:

    a.out > inffixed.h
 */
/* MAKEFIXED */
/*
   Update the window with the last wsize (normally 32K) bytes written before
   returning.  If window does not exist yet, create it.  This is only called
   when a window is already in use, or when output has been written during this
   inflate call, but the end of the deflate stream has not been reached yet.
   It is also called to create a window for dictionary data when a dictionary
   is loaded.

   Providing output buffers larger than 32K to inflate() should provide a speed
   advantage, since only the last 32K of output is copied to the sliding window
   upon return from inflate(), and since all distances after the first 32K of
   output will fall in the output data, making match copies simpler and faster.
   The advantage may be dependent on the size of the processor's data caches.
 */
unsafe fn updatewindow(mut strm: z_streamp, mut end: *const Bytef,
                       mut copy: c_uint) -> c_int {
    let mut dist:
            c_uint; /* if it hasn't been done already, allocate space for the window */
    let mut state =
        strm.state as
            *mut inflate_state; /* if window not in use yet, initialize */
    if state.window == Z_NULL {
        state.window =
            (*((strm).zalloc))((strm).opaque, (1 << state.wbits),
                               std::mem::size_of::<u8>()) as
                *mut u8; /* copy state->wsize or less output bytes into the circular window */
        if state.window == Z_NULL { return 1; };
    }
    if state.wsize == 0 {
        state.wsize = 1 << state.wbits;
        state.wnext = 0;
        state.whave = 0;
    }
    if copy >= state.wsize {
        memcpy(state.window as *mut _, end.offset(-state.wsize), state.wsize);
        state.wnext = 0;
        state.whave = state.wsize;
    } else {
        dist = state.wsize - state.wnext;
        if dist > copy { dist = copy; }
        memcpy(state.window.offset(state.wnext), end.offset(-copy), dist);
        copy -= dist;
        if copy != 0 {
            memcpy(state.window as *mut _, end.offset(-copy), copy);
            state.wnext = copy;
            state.whave = state.wsize;
        } else {
            state.wnext += dist;
            if state.wnext == state.wsize { state.wnext = 0; }
            if state.whave < state.wsize { state.whave += dist; };
        };
    }
    return 0;
}
/* Macros for inflate(): */
/* check function to use adler32() for zlib or crc32() for gzip */
/* check macros for header crc */
/* Load registers with state in inflate() for speed */
/* Restore state from registers in inflate() */
/* Clear the input bit accumulator */
/* Get a byte of input into the bit accumulator, or return from inflate()
   if there is no input available. */
/* Assure that there are at least n bits in the bit accumulator.  If there is
   not enough available input to do that, then return from inflate(). */
/* Return the low n bits of the bit accumulator (n < 16) */
/* Remove n bits from the bit accumulator */
/* Remove zero to seven bits as needed to go to a byte boundary */
/*
   inflate() uses a state machine to process as much input data and generate as
   much output data as possible before returning.  The state machine is
   structured roughly as follows:

    for (;;) switch (state) {
    ...
    case STATEn:
        if (not enough input data or output space to make progress)
            return;
        ... make progress ...
        state = STATEm;
        break;
    ...
    }

   so when inflate() is called again, the same case is attempted again, and
   if the appropriate resources are provided, the machine proceeds to the
   next state.  The NEEDBITS() macro is usually the way the state evaluates
   whether it can proceed or should return.  NEEDBITS() does the return if
   the requested bits are not available.  The typical use of the BITS macros
   is:

        NEEDBITS(n);
        ... do something with BITS(n) ...
        DROPBITS(n);

   where NEEDBITS(n) either returns from inflate() if there isn't enough
   input left to load n bits into the accumulator, or it continues.  BITS(n)
   gives the low n bits in the accumulator.  When done, DROPBITS(n) drops
   the low n bits off the accumulator.  INITBITS() clears the accumulator
   and sets the number of available bits to zero.  BYTEBITS() discards just
   enough bits to put the accumulator on a byte boundary.  After BYTEBITS()
   and a NEEDBITS(8), then BITS(8) would return the next byte in the stream.

   NEEDBITS(n) uses PULLBYTE() to get an available byte of input, or to return
   if there is no input available.  The decoding of variable length codes uses
   PULLBYTE() directly in order to pull just enough bytes to decode the next
   code, and no more.

   Some states loop until they get enough input, making sure that enough
   state information is maintained to continue the loop where it left off
   if NEEDBITS() returns in the loop.  For example, want, need, and keep
   would all have to actually be part of the saved state in case NEEDBITS()
   returns:

    case STATEw:
        while (want < need) {
            NEEDBITS(n);
            keep[want++] = BITS(n);
            DROPBITS(n);
        }
        state = STATEx;
    case STATEx:

   As shown above, if the next state is also the next case, then the break
   is omitted.

   A state may also return if there is not enough output space available to
   complete that state.  Those states are copying stored data, writing a
   literal byte, and copying a matching string.

   When returning, a "goto inf_leave" is used to update the total counters,
   update the check value, and determine whether any progress has been made
   during that inflate() call in order to return the proper return code.
   Progress is defined as a change in either strm->avail_in or strm->avail_out.
   When there is a window, goto inf_leave will update the window with the last
   output written.  If a goto inf_leave occurs in the middle of decompression
   and there is no window currently, goto inf_leave will create one and copy
   output to the window for the next call of inflate().

   In this implementation, the flush parameter of inflate() only affects the
   return code (per zlib.h).  inflate() always writes as much as possible to
   strm->next_out, given the space available and the provided input--the effect
   documented in zlib.h of Z_SYNC_FLUSH.  Furthermore, inflate() always defers
   the allocation of and copying into a sliding window until necessary, which
   provides the effect documented in zlib.h for Z_FINISH when the entire input
   stream available.  So the only thing the flush parameter actually does is:
   when flush is set to Z_FINISH, inflate() cannot return Z_OK.  Instead it
   will return Z_BUF_ERROR if it has not reached the end of the stream.
 */
/* next input */
/* next output */
/* available input and output */
/* bit buffer */
/* bits in bit buffer */
/* save starting available input and output */
/* number of stored or match bytes to copy */
/* where to copy match bytes from */
/* current decoding table entry */
/* parent table entry */
/* length to copy for repeats, bits to drop */
/* return code */
/* buffer for gzip header crc calculation */
/* permutation of code lengths */
/* skip check */
/* gzip header */
/* expect zlib header */
/* check if zlib header allowed */
pub static Z_DEFLATED: c_uint = 8;
pub static Z_NEED_DICT: c_int = 2;
pub static Z_BLOCK: c_int = 5;
pub static Z_TREES: c_int = 6;
/* stored block */
/* fixed block */
/* decode codes */
/* dynamic block */
/* go to byte boundary */
/* handle error breaks in while */
/* check for end-of-block code (better have one) */
/* build code tables -- note: do not change the lenbits or distbits
               values here (9 and 6) without reading the comments in inftrees.h
               concerning the ENOUGH constants, which depend on those values */
/* copy from window */
/* copy from output */
pub static Z_STREAM_END: c_int = 1;
pub static Z_DATA_ERROR: c_int = -3;
/*
       Return from inflate(), updating the total counts and the check value.
       If there was no progress during the inflate() call, return a buffer
       error.  Call updatewindow() to create and/or update the window state.
       Note: a memory error from inflate() is non-recoverable.
     */
pub static Z_FINISH: c_int = 4;
pub static Z_BUF_ERROR: c_int = -5;
#[no_mangle]
pub unsafe extern "C" fn inflate(mut strm: z_streamp, mut flush: c_int)
 -> c_int {
    let mut next: *mut u8; /* check state */
    let mut put: *mut u8; /* copy dictionary */
    let mut have: c_uint; /* check state */
    let mut left: c_uint; /* check for correct dictionary identifier */
    let mut hold: c_long;
    let mut bits: c_uint;
    let mut copy: c_uint;
    let mut from: *mut u8;
    let mut here: code;
    let mut last: code;
    let mut hbuf: [u8; 4];
    let mut order =
        [16u16, 17u16, 18u16, 0u16, 8u16, 7u16, 9u16, 6u16, 10u16, 5u16,
         11u16, 4u16, 12u16, 3u16, 13u16, 2u16, 14u16, 1u16, 15u16];
    if inflateStateCheck(strm) != 0 || strm.next_out == Z_NULL ||
           (strm.next_in == Z_NULL && strm.avail_in != 0) {
        return Z_STREAM_ERROR;
    }
    let mut state = strm.state as *mut inflate_state;
    if state.mode == TYPE { state.mode = TYPEDO; }
    loop  {
        put = strm.next_out;
        left = strm.avail_out;
        next = strm.next_in;
        have = strm.avail_in;
        hold = state.hold;
        bits = state.bits;
        if 0 == 0 { break  };
    }
    let mut in_ = have;
    let mut out = left;
    let mut ret = Z_OK;
    let mut len: c_uint;
    loop  {
        match state.mode {
            HEAD => {
                if state.wrap == 0 { state.mode = TYPEDO; }
                loop  {
                    while bits < 16 {
                        loop  {
                            if have == 0 { break inf_leave ; }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    if 0 == 0 { break  };
                }
                if (state.wrap & 2) != 0 && hold == 35615 {
                    if state.wbits == 0 { state.wbits = 15; }
                    state.check = crc32(0, Z_NULL, 0);
                    loop  {
                        hbuf[0] = (hold) as u8;
                        hbuf[1] = ((hold) >> 8) as u8;
                        state.check = crc32(state.check, hbuf, 2);
                        if 0 == 0 { break  };
                    }
                    loop  { hold = 0; bits = 0; if 0 == 0 { break  }; }
                    state.mode = FLAGS;
                }
                state.flags = 0;
                if state.head != Z_NULL { state.head.done = -1; }
                if (state.wrap & 1) == 0 ||
                       ((((hold as c_uint) & ((1 << 8) - 1)) << 8) +
                            (hold >> 8)) % 31 != 0 {
                    strm.msg = "incorrect header check";
                    state.mode = BAD;
                }
                if ((hold as c_uint) & ((1 << 4) - 1)) != Z_DEFLATED {
                    strm.msg = "unknown compression method";
                    state.mode = BAD;
                }
                loop  { hold >>= 4; bits -= 4; if 0 == 0 { break  }; }
                len = ((hold as c_uint) & ((1 << 4) - 1)) + 8;
                if state.wbits == 0 { state.wbits = len; }
                if len > 15 || len > state.wbits {
                    strm.msg = "invalid window size";
                    state.mode = BAD;
                }
                state.dmax = 1 << len;
                strm.adler =
                    { state.check = adler32(0, Z_NULL, 0); state.check };
                state.mode = if hold & 512 != 0 { DICTID } else { TYPE };
                loop  { hold = 0; bits = 0; if 0 == 0 { break  }; }
            }
            FLAGS => {
                loop  {
                    while bits < 16 {
                        loop  {
                            if have == 0 { break inf_leave ; }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    if 0 == 0 { break  };
                }
                state.flags = (hold) as c_int;
                if (state.flags & 255) != Z_DEFLATED {
                    strm.msg = "unknown compression method";
                    state.mode = BAD;
                }
                if state.flags & 57344 != 0 {
                    strm.msg = "unknown header flags set";
                    state.mode = BAD;
                }
                if state.head != Z_NULL {
                    state.head.text = ((hold >> 8) & 1) as c_int;
                }
                if (state.flags & 512) != 0 && (state.wrap & 4) != 0 {
                    loop  {
                        hbuf[0] = (hold) as u8;
                        hbuf[1] = ((hold) >> 8) as u8;
                        state.check = crc32(state.check, hbuf, 2);
                        if 0 == 0 { break  };
                    };
                }
                loop  { hold = 0; bits = 0; if 0 == 0 { break  }; }
                state.mode = TIME;
                loop  {
                    while bits < 32 {
                        loop  {
                            if have == 0 { break inf_leave ; }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    if 0 == 0 { break  };
                }
                if state.head != Z_NULL { state.head.time = hold; }
                if (state.flags & 512) != 0 && (state.wrap & 4) != 0 {
                    loop  {
                        hbuf[0] = (hold) as u8;
                        hbuf[1] = ((hold) >> 8) as u8;
                        hbuf[2] = ((hold) >> 16) as u8;
                        hbuf[3] = ((hold) >> 24) as u8;
                        state.check = crc32(state.check, hbuf, 4);
                        if 0 == 0 { break  };
                    };
                }
                loop  { hold = 0; bits = 0; if 0 == 0 { break  }; }
                state.mode = OS;
                loop  {
                    while bits < 16 {
                        loop  {
                            if have == 0 { break inf_leave ; }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    if 0 == 0 { break  };
                }
                if state.head != Z_NULL {
                    state.head.xflags = (hold & 255) as c_int;
                    state.head.os = (hold >> 8) as c_int;
                }
                if (state.flags & 512) != 0 && (state.wrap & 4) != 0 {
                    loop  {
                        hbuf[0] = (hold) as u8;
                        hbuf[1] = ((hold) >> 8) as u8;
                        state.check = crc32(state.check, hbuf, 2);
                        if 0 == 0 { break  };
                    };
                }
                loop  { hold = 0; bits = 0; if 0 == 0 { break  }; }
                state.mode = EXLEN;
                if state.flags & 1024 != 0 {
                    loop  {
                        while bits < 16 {
                            loop  {
                                if have == 0 { break inf_leave ; }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    state.length = (hold) as c_uint;
                    if state.head != Z_NULL {
                        state.head.extra_len = hold as c_uint;
                    }
                    if (state.flags & 512) != 0 && (state.wrap & 4) != 0 {
                        loop  {
                            hbuf[0] = (hold) as u8;
                            hbuf[1] = ((hold) >> 8) as u8;
                            state.check = crc32(state.check, hbuf, 2);
                            if 0 == 0 { break  };
                        };
                    }
                    loop  { hold = 0; bits = 0; if 0 == 0 { break  }; };
                } else if state.head != Z_NULL { state.head.extra = Z_NULL; }
                state.mode = EXTRA;
                if state.flags & 1024 != 0 {
                    copy = state.length;
                    if copy > have { copy = have; }
                    if copy != 0 {
                        if state.head != Z_NULL && state.head.extra != Z_NULL
                           {
                            len = state.head.extra_len - state.length;
                            memcpy(state.head.extra.offset(len),
                                   next as *const _,
                                   if len + copy > state.head.extra_max {
                                       state.head.extra_max - len
                                   } else { copy });
                        }
                        if (state.flags & 512) != 0 && (state.wrap & 4) != 0 {
                            state.check = crc32(state.check, next, copy);
                        }
                        have -= copy;
                        next = next.offset(copy);
                        state.length -= copy;
                    }
                    if state.length != 0 { break inf_leave ; };
                }
                state.length = 0;
                state.mode = NAME;
                if state.flags & 2048 != 0 {
                    if have == 0 { break inf_leave ; }
                    copy = 0;
                    loop  {
                        len =
                            (*next.offset({
                                              let mut _t = copy;
                                              copy += 1;
                                              _t
                                          })) as c_uint;
                        if state.head != Z_NULL && state.head.name != Z_NULL
                               && state.length < state.head.name_max {
                            *state.head.name.offset({
                                                        let mut _t =
                                                            state.length;
                                                        state.length += 1;
                                                        _t
                                                    }) = len as Bytef;
                        }
                        if !(len != 0 && copy < have) { break  };
                    }
                    if (state.flags & 512) != 0 && (state.wrap & 4) != 0 {
                        state.check = crc32(state.check, next, copy);
                    }
                    have -= copy;
                    next = next.offset(copy);
                    if len != 0 { break inf_leave ; };
                } else if state.head != Z_NULL { state.head.name = Z_NULL; }
                state.length = 0;
                state.mode = COMMENT;
                if state.flags & 4096 != 0 {
                    if have == 0 { break inf_leave ; }
                    copy = 0;
                    loop  {
                        len =
                            (*next.offset({
                                              let mut _t = copy;
                                              copy += 1;
                                              _t
                                          })) as c_uint;
                        if state.head != Z_NULL &&
                               state.head.comment != Z_NULL &&
                               state.length < state.head.comm_max {
                            *state.head.comment.offset({
                                                           let mut _t =
                                                               state.length;
                                                           state.length += 1;
                                                           _t
                                                       }) = len as Bytef;
                        }
                        if !(len != 0 && copy < have) { break  };
                    }
                    if (state.flags & 512) != 0 && (state.wrap & 4) != 0 {
                        state.check = crc32(state.check, next, copy);
                    }
                    have -= copy;
                    next = next.offset(copy);
                    if len != 0 { break inf_leave ; };
                } else if state.head != Z_NULL {
                    state.head.comment = Z_NULL;
                }
                state.mode = HCRC;
                if state.flags & 512 != 0 {
                    loop  {
                        while bits < 16 {
                            loop  {
                                if have == 0 { break inf_leave ; }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    if (state.wrap & 4) != 0 && hold != (state.check & 65535)
                       {
                        strm.msg = "header crc mismatch";
                        state.mode = BAD;
                    }
                    loop  { hold = 0; bits = 0; if 0 == 0 { break  }; };
                }
                if state.head != Z_NULL {
                    state.head.hcrc = ((state.flags >> 9) & 1) as c_int;
                    state.head.done = 1;
                }
                strm.adler =
                    { state.check = crc32(0, Z_NULL, 0); state.check };
                state.mode = TYPE
            }
            TIME => {
                loop  {
                    while bits < 32 {
                        loop  {
                            if have == 0 { break inf_leave ; }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    if 0 == 0 { break  };
                }
                if state.head != Z_NULL { state.head.time = hold; }
                if (state.flags & 512) != 0 && (state.wrap & 4) != 0 {
                    loop  {
                        hbuf[0] = (hold) as u8;
                        hbuf[1] = ((hold) >> 8) as u8;
                        hbuf[2] = ((hold) >> 16) as u8;
                        hbuf[3] = ((hold) >> 24) as u8;
                        state.check = crc32(state.check, hbuf, 4);
                        if 0 == 0 { break  };
                    };
                }
                loop  { hold = 0; bits = 0; if 0 == 0 { break  }; }
                state.mode = OS;
                loop  {
                    while bits < 16 {
                        loop  {
                            if have == 0 { break inf_leave ; }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    if 0 == 0 { break  };
                }
                if state.head != Z_NULL {
                    state.head.xflags = (hold & 255) as c_int;
                    state.head.os = (hold >> 8) as c_int;
                }
                if (state.flags & 512) != 0 && (state.wrap & 4) != 0 {
                    loop  {
                        hbuf[0] = (hold) as u8;
                        hbuf[1] = ((hold) >> 8) as u8;
                        state.check = crc32(state.check, hbuf, 2);
                        if 0 == 0 { break  };
                    };
                }
                loop  { hold = 0; bits = 0; if 0 == 0 { break  }; }
                state.mode = EXLEN;
                if state.flags & 1024 != 0 {
                    loop  {
                        while bits < 16 {
                            loop  {
                                if have == 0 { break inf_leave ; }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    state.length = (hold) as c_uint;
                    if state.head != Z_NULL {
                        state.head.extra_len = hold as c_uint;
                    }
                    if (state.flags & 512) != 0 && (state.wrap & 4) != 0 {
                        loop  {
                            hbuf[0] = (hold) as u8;
                            hbuf[1] = ((hold) >> 8) as u8;
                            state.check = crc32(state.check, hbuf, 2);
                            if 0 == 0 { break  };
                        };
                    }
                    loop  { hold = 0; bits = 0; if 0 == 0 { break  }; };
                } else if state.head != Z_NULL { state.head.extra = Z_NULL; }
                state.mode = EXTRA;
                if state.flags & 1024 != 0 {
                    copy = state.length;
                    if copy > have { copy = have; }
                    if copy != 0 {
                        if state.head != Z_NULL && state.head.extra != Z_NULL
                           {
                            len = state.head.extra_len - state.length;
                            memcpy(state.head.extra.offset(len),
                                   next as *const _,
                                   if len + copy > state.head.extra_max {
                                       state.head.extra_max - len
                                   } else { copy });
                        }
                        if (state.flags & 512) != 0 && (state.wrap & 4) != 0 {
                            state.check = crc32(state.check, next, copy);
                        }
                        have -= copy;
                        next = next.offset(copy);
                        state.length -= copy;
                    }
                    if state.length != 0 { break inf_leave ; };
                }
                state.length = 0;
                state.mode = NAME;
                if state.flags & 2048 != 0 {
                    if have == 0 { break inf_leave ; }
                    copy = 0;
                    loop  {
                        len =
                            (*next.offset({
                                              let mut _t = copy;
                                              copy += 1;
                                              _t
                                          })) as c_uint;
                        if state.head != Z_NULL && state.head.name != Z_NULL
                               && state.length < state.head.name_max {
                            *state.head.name.offset({
                                                        let mut _t =
                                                            state.length;
                                                        state.length += 1;
                                                        _t
                                                    }) = len as Bytef;
                        }
                        if !(len != 0 && copy < have) { break  };
                    }
                    if (state.flags & 512) != 0 && (state.wrap & 4) != 0 {
                        state.check = crc32(state.check, next, copy);
                    }
                    have -= copy;
                    next = next.offset(copy);
                    if len != 0 { break inf_leave ; };
                } else if state.head != Z_NULL { state.head.name = Z_NULL; }
                state.length = 0;
                state.mode = COMMENT;
                if state.flags & 4096 != 0 {
                    if have == 0 { break inf_leave ; }
                    copy = 0;
                    loop  {
                        len =
                            (*next.offset({
                                              let mut _t = copy;
                                              copy += 1;
                                              _t
                                          })) as c_uint;
                        if state.head != Z_NULL &&
                               state.head.comment != Z_NULL &&
                               state.length < state.head.comm_max {
                            *state.head.comment.offset({
                                                           let mut _t =
                                                               state.length;
                                                           state.length += 1;
                                                           _t
                                                       }) = len as Bytef;
                        }
                        if !(len != 0 && copy < have) { break  };
                    }
                    if (state.flags & 512) != 0 && (state.wrap & 4) != 0 {
                        state.check = crc32(state.check, next, copy);
                    }
                    have -= copy;
                    next = next.offset(copy);
                    if len != 0 { break inf_leave ; };
                } else if state.head != Z_NULL {
                    state.head.comment = Z_NULL;
                }
                state.mode = HCRC;
                if state.flags & 512 != 0 {
                    loop  {
                        while bits < 16 {
                            loop  {
                                if have == 0 { break inf_leave ; }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    if (state.wrap & 4) != 0 && hold != (state.check & 65535)
                       {
                        strm.msg = "header crc mismatch";
                        state.mode = BAD;
                    }
                    loop  { hold = 0; bits = 0; if 0 == 0 { break  }; };
                }
                if state.head != Z_NULL {
                    state.head.hcrc = ((state.flags >> 9) & 1) as c_int;
                    state.head.done = 1;
                }
                strm.adler =
                    { state.check = crc32(0, Z_NULL, 0); state.check };
                state.mode = TYPE
            }
            OS => {
                loop  {
                    while bits < 16 {
                        loop  {
                            if have == 0 { break inf_leave ; }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    if 0 == 0 { break  };
                }
                if state.head != Z_NULL {
                    state.head.xflags = (hold & 255) as c_int;
                    state.head.os = (hold >> 8) as c_int;
                }
                if (state.flags & 512) != 0 && (state.wrap & 4) != 0 {
                    loop  {
                        hbuf[0] = (hold) as u8;
                        hbuf[1] = ((hold) >> 8) as u8;
                        state.check = crc32(state.check, hbuf, 2);
                        if 0 == 0 { break  };
                    };
                }
                loop  { hold = 0; bits = 0; if 0 == 0 { break  }; }
                state.mode = EXLEN;
                if state.flags & 1024 != 0 {
                    loop  {
                        while bits < 16 {
                            loop  {
                                if have == 0 { break inf_leave ; }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    state.length = (hold) as c_uint;
                    if state.head != Z_NULL {
                        state.head.extra_len = hold as c_uint;
                    }
                    if (state.flags & 512) != 0 && (state.wrap & 4) != 0 {
                        loop  {
                            hbuf[0] = (hold) as u8;
                            hbuf[1] = ((hold) >> 8) as u8;
                            state.check = crc32(state.check, hbuf, 2);
                            if 0 == 0 { break  };
                        };
                    }
                    loop  { hold = 0; bits = 0; if 0 == 0 { break  }; };
                } else if state.head != Z_NULL { state.head.extra = Z_NULL; }
                state.mode = EXTRA;
                if state.flags & 1024 != 0 {
                    copy = state.length;
                    if copy > have { copy = have; }
                    if copy != 0 {
                        if state.head != Z_NULL && state.head.extra != Z_NULL
                           {
                            len = state.head.extra_len - state.length;
                            memcpy(state.head.extra.offset(len),
                                   next as *const _,
                                   if len + copy > state.head.extra_max {
                                       state.head.extra_max - len
                                   } else { copy });
                        }
                        if (state.flags & 512) != 0 && (state.wrap & 4) != 0 {
                            state.check = crc32(state.check, next, copy);
                        }
                        have -= copy;
                        next = next.offset(copy);
                        state.length -= copy;
                    }
                    if state.length != 0 { break inf_leave ; };
                }
                state.length = 0;
                state.mode = NAME;
                if state.flags & 2048 != 0 {
                    if have == 0 { break inf_leave ; }
                    copy = 0;
                    loop  {
                        len =
                            (*next.offset({
                                              let mut _t = copy;
                                              copy += 1;
                                              _t
                                          })) as c_uint;
                        if state.head != Z_NULL && state.head.name != Z_NULL
                               && state.length < state.head.name_max {
                            *state.head.name.offset({
                                                        let mut _t =
                                                            state.length;
                                                        state.length += 1;
                                                        _t
                                                    }) = len as Bytef;
                        }
                        if !(len != 0 && copy < have) { break  };
                    }
                    if (state.flags & 512) != 0 && (state.wrap & 4) != 0 {
                        state.check = crc32(state.check, next, copy);
                    }
                    have -= copy;
                    next = next.offset(copy);
                    if len != 0 { break inf_leave ; };
                } else if state.head != Z_NULL { state.head.name = Z_NULL; }
                state.length = 0;
                state.mode = COMMENT;
                if state.flags & 4096 != 0 {
                    if have == 0 { break inf_leave ; }
                    copy = 0;
                    loop  {
                        len =
                            (*next.offset({
                                              let mut _t = copy;
                                              copy += 1;
                                              _t
                                          })) as c_uint;
                        if state.head != Z_NULL &&
                               state.head.comment != Z_NULL &&
                               state.length < state.head.comm_max {
                            *state.head.comment.offset({
                                                           let mut _t =
                                                               state.length;
                                                           state.length += 1;
                                                           _t
                                                       }) = len as Bytef;
                        }
                        if !(len != 0 && copy < have) { break  };
                    }
                    if (state.flags & 512) != 0 && (state.wrap & 4) != 0 {
                        state.check = crc32(state.check, next, copy);
                    }
                    have -= copy;
                    next = next.offset(copy);
                    if len != 0 { break inf_leave ; };
                } else if state.head != Z_NULL {
                    state.head.comment = Z_NULL;
                }
                state.mode = HCRC;
                if state.flags & 512 != 0 {
                    loop  {
                        while bits < 16 {
                            loop  {
                                if have == 0 { break inf_leave ; }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    if (state.wrap & 4) != 0 && hold != (state.check & 65535)
                       {
                        strm.msg = "header crc mismatch";
                        state.mode = BAD;
                    }
                    loop  { hold = 0; bits = 0; if 0 == 0 { break  }; };
                }
                if state.head != Z_NULL {
                    state.head.hcrc = ((state.flags >> 9) & 1) as c_int;
                    state.head.done = 1;
                }
                strm.adler =
                    { state.check = crc32(0, Z_NULL, 0); state.check };
                state.mode = TYPE
            }
            EXLEN => {
                if state.flags & 1024 != 0 {
                    loop  {
                        while bits < 16 {
                            loop  {
                                if have == 0 { break inf_leave ; }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    state.length = (hold) as c_uint;
                    if state.head != Z_NULL {
                        state.head.extra_len = hold as c_uint;
                    }
                    if (state.flags & 512) != 0 && (state.wrap & 4) != 0 {
                        loop  {
                            hbuf[0] = (hold) as u8;
                            hbuf[1] = ((hold) >> 8) as u8;
                            state.check = crc32(state.check, hbuf, 2);
                            if 0 == 0 { break  };
                        };
                    }
                    loop  { hold = 0; bits = 0; if 0 == 0 { break  }; };
                } else if state.head != Z_NULL { state.head.extra = Z_NULL; }
                state.mode = EXTRA;
                if state.flags & 1024 != 0 {
                    copy = state.length;
                    if copy > have { copy = have; }
                    if copy != 0 {
                        if state.head != Z_NULL && state.head.extra != Z_NULL
                           {
                            len = state.head.extra_len - state.length;
                            memcpy(state.head.extra.offset(len),
                                   next as *const _,
                                   if len + copy > state.head.extra_max {
                                       state.head.extra_max - len
                                   } else { copy });
                        }
                        if (state.flags & 512) != 0 && (state.wrap & 4) != 0 {
                            state.check = crc32(state.check, next, copy);
                        }
                        have -= copy;
                        next = next.offset(copy);
                        state.length -= copy;
                    }
                    if state.length != 0 { break inf_leave ; };
                }
                state.length = 0;
                state.mode = NAME;
                if state.flags & 2048 != 0 {
                    if have == 0 { break inf_leave ; }
                    copy = 0;
                    loop  {
                        len =
                            (*next.offset({
                                              let mut _t = copy;
                                              copy += 1;
                                              _t
                                          })) as c_uint;
                        if state.head != Z_NULL && state.head.name != Z_NULL
                               && state.length < state.head.name_max {
                            *state.head.name.offset({
                                                        let mut _t =
                                                            state.length;
                                                        state.length += 1;
                                                        _t
                                                    }) = len as Bytef;
                        }
                        if !(len != 0 && copy < have) { break  };
                    }
                    if (state.flags & 512) != 0 && (state.wrap & 4) != 0 {
                        state.check = crc32(state.check, next, copy);
                    }
                    have -= copy;
                    next = next.offset(copy);
                    if len != 0 { break inf_leave ; };
                } else if state.head != Z_NULL { state.head.name = Z_NULL; }
                state.length = 0;
                state.mode = COMMENT;
                if state.flags & 4096 != 0 {
                    if have == 0 { break inf_leave ; }
                    copy = 0;
                    loop  {
                        len =
                            (*next.offset({
                                              let mut _t = copy;
                                              copy += 1;
                                              _t
                                          })) as c_uint;
                        if state.head != Z_NULL &&
                               state.head.comment != Z_NULL &&
                               state.length < state.head.comm_max {
                            *state.head.comment.offset({
                                                           let mut _t =
                                                               state.length;
                                                           state.length += 1;
                                                           _t
                                                       }) = len as Bytef;
                        }
                        if !(len != 0 && copy < have) { break  };
                    }
                    if (state.flags & 512) != 0 && (state.wrap & 4) != 0 {
                        state.check = crc32(state.check, next, copy);
                    }
                    have -= copy;
                    next = next.offset(copy);
                    if len != 0 { break inf_leave ; };
                } else if state.head != Z_NULL {
                    state.head.comment = Z_NULL;
                }
                state.mode = HCRC;
                if state.flags & 512 != 0 {
                    loop  {
                        while bits < 16 {
                            loop  {
                                if have == 0 { break inf_leave ; }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    if (state.wrap & 4) != 0 && hold != (state.check & 65535)
                       {
                        strm.msg = "header crc mismatch";
                        state.mode = BAD;
                    }
                    loop  { hold = 0; bits = 0; if 0 == 0 { break  }; };
                }
                if state.head != Z_NULL {
                    state.head.hcrc = ((state.flags >> 9) & 1) as c_int;
                    state.head.done = 1;
                }
                strm.adler =
                    { state.check = crc32(0, Z_NULL, 0); state.check };
                state.mode = TYPE
            }
            EXTRA => {
                if state.flags & 1024 != 0 {
                    copy = state.length;
                    if copy > have { copy = have; }
                    if copy != 0 {
                        if state.head != Z_NULL && state.head.extra != Z_NULL
                           {
                            len = state.head.extra_len - state.length;
                            memcpy(state.head.extra.offset(len),
                                   next as *const _,
                                   if len + copy > state.head.extra_max {
                                       state.head.extra_max - len
                                   } else { copy });
                        }
                        if (state.flags & 512) != 0 && (state.wrap & 4) != 0 {
                            state.check = crc32(state.check, next, copy);
                        }
                        have -= copy;
                        next = next.offset(copy);
                        state.length -= copy;
                    }
                    if state.length != 0 { break inf_leave ; };
                }
                state.length = 0;
                state.mode = NAME;
                if state.flags & 2048 != 0 {
                    if have == 0 { break inf_leave ; }
                    copy = 0;
                    loop  {
                        len =
                            (*next.offset({
                                              let mut _t = copy;
                                              copy += 1;
                                              _t
                                          })) as c_uint;
                        if state.head != Z_NULL && state.head.name != Z_NULL
                               && state.length < state.head.name_max {
                            *state.head.name.offset({
                                                        let mut _t =
                                                            state.length;
                                                        state.length += 1;
                                                        _t
                                                    }) = len as Bytef;
                        }
                        if !(len != 0 && copy < have) { break  };
                    }
                    if (state.flags & 512) != 0 && (state.wrap & 4) != 0 {
                        state.check = crc32(state.check, next, copy);
                    }
                    have -= copy;
                    next = next.offset(copy);
                    if len != 0 { break inf_leave ; };
                } else if state.head != Z_NULL { state.head.name = Z_NULL; }
                state.length = 0;
                state.mode = COMMENT;
                if state.flags & 4096 != 0 {
                    if have == 0 { break inf_leave ; }
                    copy = 0;
                    loop  {
                        len =
                            (*next.offset({
                                              let mut _t = copy;
                                              copy += 1;
                                              _t
                                          })) as c_uint;
                        if state.head != Z_NULL &&
                               state.head.comment != Z_NULL &&
                               state.length < state.head.comm_max {
                            *state.head.comment.offset({
                                                           let mut _t =
                                                               state.length;
                                                           state.length += 1;
                                                           _t
                                                       }) = len as Bytef;
                        }
                        if !(len != 0 && copy < have) { break  };
                    }
                    if (state.flags & 512) != 0 && (state.wrap & 4) != 0 {
                        state.check = crc32(state.check, next, copy);
                    }
                    have -= copy;
                    next = next.offset(copy);
                    if len != 0 { break inf_leave ; };
                } else if state.head != Z_NULL {
                    state.head.comment = Z_NULL;
                }
                state.mode = HCRC;
                if state.flags & 512 != 0 {
                    loop  {
                        while bits < 16 {
                            loop  {
                                if have == 0 { break inf_leave ; }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    if (state.wrap & 4) != 0 && hold != (state.check & 65535)
                       {
                        strm.msg = "header crc mismatch";
                        state.mode = BAD;
                    }
                    loop  { hold = 0; bits = 0; if 0 == 0 { break  }; };
                }
                if state.head != Z_NULL {
                    state.head.hcrc = ((state.flags >> 9) & 1) as c_int;
                    state.head.done = 1;
                }
                strm.adler =
                    { state.check = crc32(0, Z_NULL, 0); state.check };
                state.mode = TYPE
            }
            NAME => {
                if state.flags & 2048 != 0 {
                    if have == 0 { break inf_leave ; }
                    copy = 0;
                    loop  {
                        len =
                            (*next.offset({
                                              let mut _t = copy;
                                              copy += 1;
                                              _t
                                          })) as c_uint;
                        if state.head != Z_NULL && state.head.name != Z_NULL
                               && state.length < state.head.name_max {
                            *state.head.name.offset({
                                                        let mut _t =
                                                            state.length;
                                                        state.length += 1;
                                                        _t
                                                    }) = len as Bytef;
                        }
                        if !(len != 0 && copy < have) { break  };
                    }
                    if (state.flags & 512) != 0 && (state.wrap & 4) != 0 {
                        state.check = crc32(state.check, next, copy);
                    }
                    have -= copy;
                    next = next.offset(copy);
                    if len != 0 { break inf_leave ; };
                } else if state.head != Z_NULL { state.head.name = Z_NULL; }
                state.length = 0;
                state.mode = COMMENT;
                if state.flags & 4096 != 0 {
                    if have == 0 { break inf_leave ; }
                    copy = 0;
                    loop  {
                        len =
                            (*next.offset({
                                              let mut _t = copy;
                                              copy += 1;
                                              _t
                                          })) as c_uint;
                        if state.head != Z_NULL &&
                               state.head.comment != Z_NULL &&
                               state.length < state.head.comm_max {
                            *state.head.comment.offset({
                                                           let mut _t =
                                                               state.length;
                                                           state.length += 1;
                                                           _t
                                                       }) = len as Bytef;
                        }
                        if !(len != 0 && copy < have) { break  };
                    }
                    if (state.flags & 512) != 0 && (state.wrap & 4) != 0 {
                        state.check = crc32(state.check, next, copy);
                    }
                    have -= copy;
                    next = next.offset(copy);
                    if len != 0 { break inf_leave ; };
                } else if state.head != Z_NULL {
                    state.head.comment = Z_NULL;
                }
                state.mode = HCRC;
                if state.flags & 512 != 0 {
                    loop  {
                        while bits < 16 {
                            loop  {
                                if have == 0 { break inf_leave ; }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    if (state.wrap & 4) != 0 && hold != (state.check & 65535)
                       {
                        strm.msg = "header crc mismatch";
                        state.mode = BAD;
                    }
                    loop  { hold = 0; bits = 0; if 0 == 0 { break  }; };
                }
                if state.head != Z_NULL {
                    state.head.hcrc = ((state.flags >> 9) & 1) as c_int;
                    state.head.done = 1;
                }
                strm.adler =
                    { state.check = crc32(0, Z_NULL, 0); state.check };
                state.mode = TYPE
            }
            COMMENT => {
                if state.flags & 4096 != 0 {
                    if have == 0 { break inf_leave ; }
                    copy = 0;
                    loop  {
                        len =
                            (*next.offset({
                                              let mut _t = copy;
                                              copy += 1;
                                              _t
                                          })) as c_uint;
                        if state.head != Z_NULL &&
                               state.head.comment != Z_NULL &&
                               state.length < state.head.comm_max {
                            *state.head.comment.offset({
                                                           let mut _t =
                                                               state.length;
                                                           state.length += 1;
                                                           _t
                                                       }) = len as Bytef;
                        }
                        if !(len != 0 && copy < have) { break  };
                    }
                    if (state.flags & 512) != 0 && (state.wrap & 4) != 0 {
                        state.check = crc32(state.check, next, copy);
                    }
                    have -= copy;
                    next = next.offset(copy);
                    if len != 0 { break inf_leave ; };
                } else if state.head != Z_NULL {
                    state.head.comment = Z_NULL;
                }
                state.mode = HCRC;
                if state.flags & 512 != 0 {
                    loop  {
                        while bits < 16 {
                            loop  {
                                if have == 0 { break inf_leave ; }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    if (state.wrap & 4) != 0 && hold != (state.check & 65535)
                       {
                        strm.msg = "header crc mismatch";
                        state.mode = BAD;
                    }
                    loop  { hold = 0; bits = 0; if 0 == 0 { break  }; };
                }
                if state.head != Z_NULL {
                    state.head.hcrc = ((state.flags >> 9) & 1) as c_int;
                    state.head.done = 1;
                }
                strm.adler =
                    { state.check = crc32(0, Z_NULL, 0); state.check };
                state.mode = TYPE
            }
            HCRC => {
                if state.flags & 512 != 0 {
                    loop  {
                        while bits < 16 {
                            loop  {
                                if have == 0 { break inf_leave ; }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    if (state.wrap & 4) != 0 && hold != (state.check & 65535)
                       {
                        strm.msg = "header crc mismatch";
                        state.mode = BAD;
                    }
                    loop  { hold = 0; bits = 0; if 0 == 0 { break  }; };
                }
                if state.head != Z_NULL {
                    state.head.hcrc = ((state.flags >> 9) & 1) as c_int;
                    state.head.done = 1;
                }
                strm.adler =
                    { state.check = crc32(0, Z_NULL, 0); state.check };
                state.mode = TYPE
            }
            DICTID => {
                loop  {
                    while bits < 32 {
                        loop  {
                            if have == 0 { break inf_leave ; }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    if 0 == 0 { break  };
                }
                strm.adler =
                    {
                        state.check =
                            ((((hold) >> 24) & 255) + (((hold) >> 8) & 65280)
                                 + (((hold) & 65280) << 8) +
                                 (((hold) & 255) << 24));
                        state.check
                    };
                loop  { hold = 0; bits = 0; if 0 == 0 { break  }; }
                state.mode = DICT;
                if state.havedict == 0 {
                    loop  {
                        strm.next_out = put;
                        strm.avail_out = left;
                        strm.next_in = next;
                        strm.avail_in = have;
                        state.hold = hold;
                        state.bits = bits;
                        if 0 == 0 { break  };
                    }
                    return Z_NEED_DICT;
                }
                strm.adler =
                    { state.check = adler32(0, Z_NULL, 0); state.check };
                state.mode = TYPE;
                if flush == Z_BLOCK || flush == Z_TREES { break inf_leave ; }
                if state.last != 0 {
                    loop  {
                        hold >>= bits & 7;
                        bits -= bits & 7;
                        if 0 == 0 { break  };
                    }
                    state.mode = CHECK;
                }
                loop  {
                    while bits < 3 {
                        loop  {
                            if have == 0 { break inf_leave ; }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    if 0 == 0 { break  };
                }
                state.last = ((hold as c_uint) & ((1 << 1) - 1));
                loop  { hold >>= 1; bits -= 1; if 0 == 0 { break  }; }
                match ((hold as c_uint) & ((1 << 2) - 1)) {
                    0 => state.mode = STORED,
                    1 => {
                        fixedtables(state);
                        state.mode = LEN_;
                        if flush == Z_TREES {
                            loop  {
                                hold >>= 2;
                                bits -= 2;
                                if 0 == 0 { break  };
                            }
                            break inf_leave ;
                        }
                    }
                    2 => state.mode = TABLE,
                    3 => { strm.msg = "invalid block type"; state.mode = BAD }
                }
                loop  { hold >>= 2; bits -= 2; if 0 == 0 { break  }; }
            }
            DICT => {
                if state.havedict == 0 {
                    loop  {
                        strm.next_out = put;
                        strm.avail_out = left;
                        strm.next_in = next;
                        strm.avail_in = have;
                        state.hold = hold;
                        state.bits = bits;
                        if 0 == 0 { break  };
                    }
                    return Z_NEED_DICT;
                }
                strm.adler =
                    { state.check = adler32(0, Z_NULL, 0); state.check };
                state.mode = TYPE;
                if flush == Z_BLOCK || flush == Z_TREES { break inf_leave ; }
                if state.last != 0 {
                    loop  {
                        hold >>= bits & 7;
                        bits -= bits & 7;
                        if 0 == 0 { break  };
                    }
                    state.mode = CHECK;
                }
                loop  {
                    while bits < 3 {
                        loop  {
                            if have == 0 { break inf_leave ; }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    if 0 == 0 { break  };
                }
                state.last = ((hold as c_uint) & ((1 << 1) - 1));
                loop  { hold >>= 1; bits -= 1; if 0 == 0 { break  }; }
                match ((hold as c_uint) & ((1 << 2) - 1)) {
                    0 => state.mode = STORED,
                    1 => {
                        fixedtables(state);
                        state.mode = LEN_;
                        if flush == Z_TREES {
                            loop  {
                                hold >>= 2;
                                bits -= 2;
                                if 0 == 0 { break  };
                            }
                            break inf_leave ;
                        }
                    }
                    2 => state.mode = TABLE,
                    3 => { strm.msg = "invalid block type"; state.mode = BAD }
                }
                loop  { hold >>= 2; bits -= 2; if 0 == 0 { break  }; }
            }
            TYPE => {
                if flush == Z_BLOCK || flush == Z_TREES { break inf_leave ; }
                if state.last != 0 {
                    loop  {
                        hold >>= bits & 7;
                        bits -= bits & 7;
                        if 0 == 0 { break  };
                    }
                    state.mode = CHECK;
                }
                loop  {
                    while bits < 3 {
                        loop  {
                            if have == 0 { break inf_leave ; }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    if 0 == 0 { break  };
                }
                state.last = ((hold as c_uint) & ((1 << 1) - 1));
                loop  { hold >>= 1; bits -= 1; if 0 == 0 { break  }; }
                match ((hold as c_uint) & ((1 << 2) - 1)) {
                    0 => state.mode = STORED,
                    1 => {
                        fixedtables(state);
                        state.mode = LEN_;
                        if flush == Z_TREES {
                            loop  {
                                hold >>= 2;
                                bits -= 2;
                                if 0 == 0 { break  };
                            }
                            break inf_leave ;
                        }
                    }
                    2 => state.mode = TABLE,
                    3 => { strm.msg = "invalid block type"; state.mode = BAD }
                }
                loop  { hold >>= 2; bits -= 2; if 0 == 0 { break  }; }
            }
            TYPEDO => {
                if state.last != 0 {
                    loop  {
                        hold >>= bits & 7;
                        bits -= bits & 7;
                        if 0 == 0 { break  };
                    }
                    state.mode = CHECK;
                }
                loop  {
                    while bits < 3 {
                        loop  {
                            if have == 0 { break inf_leave ; }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    if 0 == 0 { break  };
                }
                state.last = ((hold as c_uint) & ((1 << 1) - 1));
                loop  { hold >>= 1; bits -= 1; if 0 == 0 { break  }; }
                match ((hold as c_uint) & ((1 << 2) - 1)) {
                    0 => state.mode = STORED,
                    1 => {
                        fixedtables(state);
                        state.mode = LEN_;
                        if flush == Z_TREES {
                            loop  {
                                hold >>= 2;
                                bits -= 2;
                                if 0 == 0 { break  };
                            }
                            break inf_leave ;
                        }
                    }
                    2 => state.mode = TABLE,
                    3 => { strm.msg = "invalid block type"; state.mode = BAD }
                }
                loop  { hold >>= 2; bits -= 2; if 0 == 0 { break  }; }
            }
            STORED => {
                loop  {
                    hold >>= bits & 7;
                    bits -= bits & 7;
                    if 0 == 0 { break  };
                }
                loop  {
                    while bits < 32 {
                        loop  {
                            if have == 0 { break inf_leave ; }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    if 0 == 0 { break  };
                }
                if (hold & 65535) != ((hold >> 16) ^ 65535) {
                    strm.msg = "invalid stored block lengths";
                    state.mode = BAD;
                }
                state.length = (hold as c_uint) & 65535;
                loop  { hold = 0; bits = 0; if 0 == 0 { break  }; }
                state.mode = COPY_;
                if flush == Z_TREES { break inf_leave ; }
                state.mode = COPY;
                copy = state.length;
                if copy != 0 {
                    if copy > have { copy = have; }
                    if copy > left { copy = left; }
                    if copy == 0 { break inf_leave ; }
                    memcpy(put as *mut _, next as *const _, copy);
                    have -= copy;
                    next = next.offset(copy);
                    left -= copy;
                    put = put.offset(copy);
                    state.length -= copy;
                }
                state.mode = TYPE
            }
            COPY_ => {
                state.mode = COPY;
                copy = state.length;
                if copy != 0 {
                    if copy > have { copy = have; }
                    if copy > left { copy = left; }
                    if copy == 0 { break inf_leave ; }
                    memcpy(put as *mut _, next as *const _, copy);
                    have -= copy;
                    next = next.offset(copy);
                    left -= copy;
                    put = put.offset(copy);
                    state.length -= copy;
                }
                state.mode = TYPE
            }
            COPY => {
                copy = state.length;
                if copy != 0 {
                    if copy > have { copy = have; }
                    if copy > left { copy = left; }
                    if copy == 0 { break inf_leave ; }
                    memcpy(put as *mut _, next as *const _, copy);
                    have -= copy;
                    next = next.offset(copy);
                    left -= copy;
                    put = put.offset(copy);
                    state.length -= copy;
                }
                state.mode = TYPE
            }
            TABLE => {
                loop  {
                    while bits < 14 {
                        loop  {
                            if have == 0 { break inf_leave ; }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    if 0 == 0 { break  };
                }
                state.nlen = ((hold as c_uint) & ((1 << 5) - 1)) + 257;
                loop  { hold >>= 5; bits -= 5; if 0 == 0 { break  }; }
                state.ndist = ((hold as c_uint) & ((1 << 5) - 1)) + 1;
                loop  { hold >>= 5; bits -= 5; if 0 == 0 { break  }; }
                state.ncode = ((hold as c_uint) & ((1 << 4) - 1)) + 4;
                loop  { hold >>= 4; bits -= 4; if 0 == 0 { break  }; }
                if state.nlen > 286 || state.ndist > 30 {
                    strm.msg = "too many length or distance symbols";
                    state.mode = BAD;
                }
                state.have = 0;
                state.mode = LENLENS;
                while state.have < state.ncode {
                    loop  {
                        while bits < 3 {
                            loop  {
                                if have == 0 { break inf_leave ; }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    state.lens[order[{
                                         let mut _t = state.have;
                                         state.have += 1;
                                         _t
                                     }]] =
                        ((hold as c_uint) & ((1 << 3) - 1)) as u16;
                    loop  { hold >>= 3; bits -= 3; if 0 == 0 { break  }; };
                }
                while state.have < 19 {
                    state.lens[order[{
                                         let mut _t = state.have;
                                         state.have += 1;
                                         _t
                                     }]] = 0u16;
                }
                state.next = state.codes;
                state.lencode = (state.next) as *const code;
                state.lenbits = 7;
                ret =
                    inflate_table(CODES, state.lens, 19, &mut (state.next),
                                  &mut (state.lenbits), state.work);
                if ret != 0 {
                    strm.msg = "invalid code lengths set";
                    state.mode = BAD;
                }
                state.have = 0;
                state.mode = CODELENS;
                while state.have < state.nlen + state.ndist {
                    loop  {
                        here =
                            *state.lencode.offset(((hold as c_uint) &
                                                       ((1 << (state.lenbits))
                                                            - 1)));
                        if ((here.bits) as c_uint) <= bits { break ; }
                        loop  {
                            if have == 0 { break inf_leave ; }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    if here.val < 16 {
                        loop  {
                            hold >>= (here.bits);
                            bits -= (here.bits) as c_uint;
                            if 0 == 0 { break  };
                        }
                        state.lens[{
                                       let mut _t = state.have;
                                       state.have += 1;
                                       _t
                                   }] = here.val;
                    } else {
                        if here.val == 16 {
                            loop  {
                                while bits < ((here.bits + 2) as c_uint) {
                                    loop  {
                                        if have == 0 { break inf_leave ; }
                                        have -= 1;
                                        hold +=
                                            ((*{
                                                   let mut _t = next;
                                                   next = next.offset(1);
                                                   _t
                                               }) as c_long) << bits;
                                        bits += 8;
                                        if 0 == 0 { break  };
                                    };
                                }
                                if 0 == 0 { break  };
                            }
                            loop  {
                                hold >>= (here.bits);
                                bits -= (here.bits) as c_uint;
                                if 0 == 0 { break  };
                            }
                            if state.have == 0 {
                                strm.msg = "invalid bit length repeat";
                                state.mode = BAD;
                                break ;
                            }
                            len = state.lens[state.have - 1];
                            copy = 3 + ((hold as c_uint) & ((1 << 2) - 1));
                            loop  {
                                hold >>= 2;
                                bits -= 2;
                                if 0 == 0 { break  };
                            };
                        } else if here.val == 17 {
                            loop  {
                                while bits < ((here.bits + 3) as c_uint) {
                                    loop  {
                                        if have == 0 { break inf_leave ; }
                                        have -= 1;
                                        hold +=
                                            ((*{
                                                   let mut _t = next;
                                                   next = next.offset(1);
                                                   _t
                                               }) as c_long) << bits;
                                        bits += 8;
                                        if 0 == 0 { break  };
                                    };
                                }
                                if 0 == 0 { break  };
                            }
                            loop  {
                                hold >>= (here.bits);
                                bits -= (here.bits) as c_uint;
                                if 0 == 0 { break  };
                            }
                            len = 0;
                            copy = 3 + ((hold as c_uint) & ((1 << 3) - 1));
                            loop  {
                                hold >>= 3;
                                bits -= 3;
                                if 0 == 0 { break  };
                            };
                        } else {
                            loop  {
                                while bits < ((here.bits + 7) as c_uint) {
                                    loop  {
                                        if have == 0 { break inf_leave ; }
                                        have -= 1;
                                        hold +=
                                            ((*{
                                                   let mut _t = next;
                                                   next = next.offset(1);
                                                   _t
                                               }) as c_long) << bits;
                                        bits += 8;
                                        if 0 == 0 { break  };
                                    };
                                }
                                if 0 == 0 { break  };
                            }
                            loop  {
                                hold >>= (here.bits);
                                bits -= (here.bits) as c_uint;
                                if 0 == 0 { break  };
                            }
                            len = 0;
                            copy = 11 + ((hold as c_uint) & ((1 << 7) - 1));
                            loop  {
                                hold >>= 7;
                                bits -= 7;
                                if 0 == 0 { break  };
                            };
                        }
                        if state.have + copy > state.nlen + state.ndist {
                            strm.msg = "invalid bit length repeat";
                            state.mode = BAD;
                            break ;
                        }
                        while { let mut _t = copy; copy -= 1; _t } != 0 {
                            state.lens[{
                                           let mut _t = state.have;
                                           state.have += 1;
                                           _t
                                       }] = len as u16;
                        };
                    };
                }
                if state.mode == BAD { }
                if state.lens[256] == 0 {
                    strm.msg = "invalid code -- missing end-of-block";
                    state.mode = BAD;
                }
                state.next = state.codes;
                state.lencode = (state.next) as *const code;
                state.lenbits = 9;
                ret =
                    inflate_table(LENS, state.lens, state.nlen,
                                  &mut (state.next), &mut (state.lenbits),
                                  state.work);
                if ret != 0 {
                    strm.msg = "invalid literal/lengths set";
                    state.mode = BAD;
                }
                state.distcode = (state.next) as *const code;
                state.distbits = 6;
                ret =
                    inflate_table(DISTS, state.lens.offset(state.nlen),
                                  state.ndist, &mut (state.next),
                                  &mut (state.distbits), state.work);
                if ret != 0 {
                    strm.msg = "invalid distances set";
                    state.mode = BAD;
                }
                state.mode = LEN_;
                if flush == Z_TREES { break inf_leave ; }
                state.mode = LEN;
                if have >= 6 && left >= 258 {
                    loop  {
                        strm.next_out = put;
                        strm.avail_out = left;
                        strm.next_in = next;
                        strm.avail_in = have;
                        state.hold = hold;
                        state.bits = bits;
                        if 0 == 0 { break  };
                    }
                    inflate_fast(strm, out);
                    loop  {
                        put = strm.next_out;
                        left = strm.avail_out;
                        next = strm.next_in;
                        have = strm.avail_in;
                        hold = state.hold;
                        bits = state.bits;
                        if 0 == 0 { break  };
                    }
                    if state.mode == TYPE { state.back = -1; };
                }
                state.back = 0;
                loop  {
                    here =
                        *state.lencode.offset(((hold as c_uint) &
                                                   ((1 << (state.lenbits)) -
                                                        1)));
                    if ((here.bits) as c_uint) <= bits { break ; }
                    loop  {
                        if have == 0 { break inf_leave ; }
                        have -= 1;
                        hold +=
                            ((*{
                                   let mut _t = next;
                                   next = next.offset(1);
                                   _t
                               }) as c_long) << bits;
                        bits += 8;
                        if 0 == 0 { break  };
                    };
                }
                if here.op != 0 && (here.op & 240) == 0 {
                    last = here;
                    loop  {
                        here =
                            *state.lencode.offset(last.val +
                                                      (((hold as c_uint) &
                                                            ((1 <<
                                                                  (last.bits +
                                                                       last.op))
                                                                 - 1)) >>
                                                           last.bits));
                        if ((last.bits + here.bits) as c_uint) <= bits {
                            break ;
                        }
                        loop  {
                            if have == 0 { break inf_leave ; }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    loop  {
                        hold >>= (last.bits);
                        bits -= (last.bits) as c_uint;
                        if 0 == 0 { break  };
                    }
                    state.back += last.bits;
                }
                loop  {
                    hold >>= (here.bits);
                    bits -= (here.bits) as c_uint;
                    if 0 == 0 { break  };
                }
                state.back += here.bits;
                state.length = here.val as c_uint;
                if ((here.op) as c_int) == 0 { state.mode = LIT; }
                if here.op & 32 != 0 { state.back = -1; state.mode = TYPE; }
                if here.op & 64 != 0 {
                    strm.msg = "invalid literal/length code";
                    state.mode = BAD;
                }
                state.extra = ((here.op) as c_uint) & 15;
                state.mode = LENEXT;
                if state.extra != 0 {
                    loop  {
                        while bits < (state.extra) {
                            loop  {
                                if have == 0 { break inf_leave ; }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    state.length +=
                        ((hold as c_uint) & ((1 << (state.extra)) - 1));
                    loop  {
                        hold >>= (state.extra);
                        bits -= (state.extra);
                        if 0 == 0 { break  };
                    }
                    state.back += state.extra;
                }
                state.was = state.length;
                state.mode = DIST;
                loop  {
                    here =
                        *state.distcode.offset(((hold as c_uint) &
                                                    ((1 << (state.distbits)) -
                                                         1)));
                    if ((here.bits) as c_uint) <= bits { break ; }
                    loop  {
                        if have == 0 { break inf_leave ; }
                        have -= 1;
                        hold +=
                            ((*{
                                   let mut _t = next;
                                   next = next.offset(1);
                                   _t
                               }) as c_long) << bits;
                        bits += 8;
                        if 0 == 0 { break  };
                    };
                }
                if (here.op & 240) == 0 {
                    last = here;
                    loop  {
                        here =
                            *state.distcode.offset(last.val +
                                                       (((hold as c_uint) &
                                                             ((1 <<
                                                                   (last.bits
                                                                        +
                                                                        last.op))
                                                                  - 1)) >>
                                                            last.bits));
                        if ((last.bits + here.bits) as c_uint) <= bits {
                            break ;
                        }
                        loop  {
                            if have == 0 { break inf_leave ; }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    loop  {
                        hold >>= (last.bits);
                        bits -= (last.bits) as c_uint;
                        if 0 == 0 { break  };
                    }
                    state.back += last.bits;
                }
                loop  {
                    hold >>= (here.bits);
                    bits -= (here.bits) as c_uint;
                    if 0 == 0 { break  };
                }
                state.back += here.bits;
                if here.op & 64 != 0 {
                    strm.msg = "invalid distance code";
                    state.mode = BAD;
                }
                state.offset = here.val as c_uint;
                state.extra = ((here.op) as c_uint) & 15;
                state.mode = DISTEXT;
                if state.extra != 0 {
                    loop  {
                        while bits < (state.extra) {
                            loop  {
                                if have == 0 { break inf_leave ; }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    state.offset +=
                        ((hold as c_uint) & ((1 << (state.extra)) - 1));
                    loop  {
                        hold >>= (state.extra);
                        bits -= (state.extra);
                        if 0 == 0 { break  };
                    }
                    state.back += state.extra;
                }
                state.mode = MATCH;
                if left == 0 { break inf_leave ; }
                copy = out - left;
                if state.offset > copy {
                    copy = state.offset - copy;
                    if copy > state.whave {
                        if state.sane != 0 {
                            strm.msg = "invalid distance too far back";
                            state.mode = BAD;
                        };
                    }
                    if copy > state.wnext {
                        copy -= state.wnext;
                        from = state.window.offset((state.wsize - copy));
                    } else {
                        from = state.window.offset((state.wnext - copy));
                    }
                    if copy > state.length { copy = state.length; };
                } else {
                    from = put.offset(-state.offset);
                    copy = state.length;
                }
                if copy > left { copy = left; }
                left -= copy;
                state.length -= copy;
                loop  {
                    *{ let mut _t = put; put = put.offset(1); _t } =
                        *{ let mut _t = from; from = from.offset(1); _t };
                    if { copy -= 1; copy } == 0 { break  };
                }
                if state.length == 0 { state.mode = LEN; }
            }
            LENLENS => {
                while state.have < state.ncode {
                    loop  {
                        while bits < 3 {
                            loop  {
                                if have == 0 { break inf_leave ; }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    state.lens[order[{
                                         let mut _t = state.have;
                                         state.have += 1;
                                         _t
                                     }]] =
                        ((hold as c_uint) & ((1 << 3) - 1)) as u16;
                    loop  { hold >>= 3; bits -= 3; if 0 == 0 { break  }; };
                }
                while state.have < 19 {
                    state.lens[order[{
                                         let mut _t = state.have;
                                         state.have += 1;
                                         _t
                                     }]] = 0u16;
                }
                state.next = state.codes;
                state.lencode = (state.next) as *const code;
                state.lenbits = 7;
                ret =
                    inflate_table(CODES, state.lens, 19, &mut (state.next),
                                  &mut (state.lenbits), state.work);
                if ret != 0 {
                    strm.msg = "invalid code lengths set";
                    state.mode = BAD;
                }
                state.have = 0;
                state.mode = CODELENS;
                while state.have < state.nlen + state.ndist {
                    loop  {
                        here =
                            *state.lencode.offset(((hold as c_uint) &
                                                       ((1 << (state.lenbits))
                                                            - 1)));
                        if ((here.bits) as c_uint) <= bits { break ; }
                        loop  {
                            if have == 0 { break inf_leave ; }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    if here.val < 16 {
                        loop  {
                            hold >>= (here.bits);
                            bits -= (here.bits) as c_uint;
                            if 0 == 0 { break  };
                        }
                        state.lens[{
                                       let mut _t = state.have;
                                       state.have += 1;
                                       _t
                                   }] = here.val;
                    } else {
                        if here.val == 16 {
                            loop  {
                                while bits < ((here.bits + 2) as c_uint) {
                                    loop  {
                                        if have == 0 { break inf_leave ; }
                                        have -= 1;
                                        hold +=
                                            ((*{
                                                   let mut _t = next;
                                                   next = next.offset(1);
                                                   _t
                                               }) as c_long) << bits;
                                        bits += 8;
                                        if 0 == 0 { break  };
                                    };
                                }
                                if 0 == 0 { break  };
                            }
                            loop  {
                                hold >>= (here.bits);
                                bits -= (here.bits) as c_uint;
                                if 0 == 0 { break  };
                            }
                            if state.have == 0 {
                                strm.msg = "invalid bit length repeat";
                                state.mode = BAD;
                                break ;
                            }
                            len = state.lens[state.have - 1];
                            copy = 3 + ((hold as c_uint) & ((1 << 2) - 1));
                            loop  {
                                hold >>= 2;
                                bits -= 2;
                                if 0 == 0 { break  };
                            };
                        } else if here.val == 17 {
                            loop  {
                                while bits < ((here.bits + 3) as c_uint) {
                                    loop  {
                                        if have == 0 { break inf_leave ; }
                                        have -= 1;
                                        hold +=
                                            ((*{
                                                   let mut _t = next;
                                                   next = next.offset(1);
                                                   _t
                                               }) as c_long) << bits;
                                        bits += 8;
                                        if 0 == 0 { break  };
                                    };
                                }
                                if 0 == 0 { break  };
                            }
                            loop  {
                                hold >>= (here.bits);
                                bits -= (here.bits) as c_uint;
                                if 0 == 0 { break  };
                            }
                            len = 0;
                            copy = 3 + ((hold as c_uint) & ((1 << 3) - 1));
                            loop  {
                                hold >>= 3;
                                bits -= 3;
                                if 0 == 0 { break  };
                            };
                        } else {
                            loop  {
                                while bits < ((here.bits + 7) as c_uint) {
                                    loop  {
                                        if have == 0 { break inf_leave ; }
                                        have -= 1;
                                        hold +=
                                            ((*{
                                                   let mut _t = next;
                                                   next = next.offset(1);
                                                   _t
                                               }) as c_long) << bits;
                                        bits += 8;
                                        if 0 == 0 { break  };
                                    };
                                }
                                if 0 == 0 { break  };
                            }
                            loop  {
                                hold >>= (here.bits);
                                bits -= (here.bits) as c_uint;
                                if 0 == 0 { break  };
                            }
                            len = 0;
                            copy = 11 + ((hold as c_uint) & ((1 << 7) - 1));
                            loop  {
                                hold >>= 7;
                                bits -= 7;
                                if 0 == 0 { break  };
                            };
                        }
                        if state.have + copy > state.nlen + state.ndist {
                            strm.msg = "invalid bit length repeat";
                            state.mode = BAD;
                            break ;
                        }
                        while { let mut _t = copy; copy -= 1; _t } != 0 {
                            state.lens[{
                                           let mut _t = state.have;
                                           state.have += 1;
                                           _t
                                       }] = len as u16;
                        };
                    };
                }
                if state.mode == BAD { }
                if state.lens[256] == 0 {
                    strm.msg = "invalid code -- missing end-of-block";
                    state.mode = BAD;
                }
                state.next = state.codes;
                state.lencode = (state.next) as *const code;
                state.lenbits = 9;
                ret =
                    inflate_table(LENS, state.lens, state.nlen,
                                  &mut (state.next), &mut (state.lenbits),
                                  state.work);
                if ret != 0 {
                    strm.msg = "invalid literal/lengths set";
                    state.mode = BAD;
                }
                state.distcode = (state.next) as *const code;
                state.distbits = 6;
                ret =
                    inflate_table(DISTS, state.lens.offset(state.nlen),
                                  state.ndist, &mut (state.next),
                                  &mut (state.distbits), state.work);
                if ret != 0 {
                    strm.msg = "invalid distances set";
                    state.mode = BAD;
                }
                state.mode = LEN_;
                if flush == Z_TREES { break inf_leave ; }
                state.mode = LEN;
                if have >= 6 && left >= 258 {
                    loop  {
                        strm.next_out = put;
                        strm.avail_out = left;
                        strm.next_in = next;
                        strm.avail_in = have;
                        state.hold = hold;
                        state.bits = bits;
                        if 0 == 0 { break  };
                    }
                    inflate_fast(strm, out);
                    loop  {
                        put = strm.next_out;
                        left = strm.avail_out;
                        next = strm.next_in;
                        have = strm.avail_in;
                        hold = state.hold;
                        bits = state.bits;
                        if 0 == 0 { break  };
                    }
                    if state.mode == TYPE { state.back = -1; };
                }
                state.back = 0;
                loop  {
                    here =
                        *state.lencode.offset(((hold as c_uint) &
                                                   ((1 << (state.lenbits)) -
                                                        1)));
                    if ((here.bits) as c_uint) <= bits { break ; }
                    loop  {
                        if have == 0 { break inf_leave ; }
                        have -= 1;
                        hold +=
                            ((*{
                                   let mut _t = next;
                                   next = next.offset(1);
                                   _t
                               }) as c_long) << bits;
                        bits += 8;
                        if 0 == 0 { break  };
                    };
                }
                if here.op != 0 && (here.op & 240) == 0 {
                    last = here;
                    loop  {
                        here =
                            *state.lencode.offset(last.val +
                                                      (((hold as c_uint) &
                                                            ((1 <<
                                                                  (last.bits +
                                                                       last.op))
                                                                 - 1)) >>
                                                           last.bits));
                        if ((last.bits + here.bits) as c_uint) <= bits {
                            break ;
                        }
                        loop  {
                            if have == 0 { break inf_leave ; }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    loop  {
                        hold >>= (last.bits);
                        bits -= (last.bits) as c_uint;
                        if 0 == 0 { break  };
                    }
                    state.back += last.bits;
                }
                loop  {
                    hold >>= (here.bits);
                    bits -= (here.bits) as c_uint;
                    if 0 == 0 { break  };
                }
                state.back += here.bits;
                state.length = here.val as c_uint;
                if ((here.op) as c_int) == 0 { state.mode = LIT; }
                if here.op & 32 != 0 { state.back = -1; state.mode = TYPE; }
                if here.op & 64 != 0 {
                    strm.msg = "invalid literal/length code";
                    state.mode = BAD;
                }
                state.extra = ((here.op) as c_uint) & 15;
                state.mode = LENEXT;
                if state.extra != 0 {
                    loop  {
                        while bits < (state.extra) {
                            loop  {
                                if have == 0 { break inf_leave ; }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    state.length +=
                        ((hold as c_uint) & ((1 << (state.extra)) - 1));
                    loop  {
                        hold >>= (state.extra);
                        bits -= (state.extra);
                        if 0 == 0 { break  };
                    }
                    state.back += state.extra;
                }
                state.was = state.length;
                state.mode = DIST;
                loop  {
                    here =
                        *state.distcode.offset(((hold as c_uint) &
                                                    ((1 << (state.distbits)) -
                                                         1)));
                    if ((here.bits) as c_uint) <= bits { break ; }
                    loop  {
                        if have == 0 { break inf_leave ; }
                        have -= 1;
                        hold +=
                            ((*{
                                   let mut _t = next;
                                   next = next.offset(1);
                                   _t
                               }) as c_long) << bits;
                        bits += 8;
                        if 0 == 0 { break  };
                    };
                }
                if (here.op & 240) == 0 {
                    last = here;
                    loop  {
                        here =
                            *state.distcode.offset(last.val +
                                                       (((hold as c_uint) &
                                                             ((1 <<
                                                                   (last.bits
                                                                        +
                                                                        last.op))
                                                                  - 1)) >>
                                                            last.bits));
                        if ((last.bits + here.bits) as c_uint) <= bits {
                            break ;
                        }
                        loop  {
                            if have == 0 { break inf_leave ; }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    loop  {
                        hold >>= (last.bits);
                        bits -= (last.bits) as c_uint;
                        if 0 == 0 { break  };
                    }
                    state.back += last.bits;
                }
                loop  {
                    hold >>= (here.bits);
                    bits -= (here.bits) as c_uint;
                    if 0 == 0 { break  };
                }
                state.back += here.bits;
                if here.op & 64 != 0 {
                    strm.msg = "invalid distance code";
                    state.mode = BAD;
                }
                state.offset = here.val as c_uint;
                state.extra = ((here.op) as c_uint) & 15;
                state.mode = DISTEXT;
                if state.extra != 0 {
                    loop  {
                        while bits < (state.extra) {
                            loop  {
                                if have == 0 { break inf_leave ; }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    state.offset +=
                        ((hold as c_uint) & ((1 << (state.extra)) - 1));
                    loop  {
                        hold >>= (state.extra);
                        bits -= (state.extra);
                        if 0 == 0 { break  };
                    }
                    state.back += state.extra;
                }
                state.mode = MATCH;
                if left == 0 { break inf_leave ; }
                copy = out - left;
                if state.offset > copy {
                    copy = state.offset - copy;
                    if copy > state.whave {
                        if state.sane != 0 {
                            strm.msg = "invalid distance too far back";
                            state.mode = BAD;
                        };
                    }
                    if copy > state.wnext {
                        copy -= state.wnext;
                        from = state.window.offset((state.wsize - copy));
                    } else {
                        from = state.window.offset((state.wnext - copy));
                    }
                    if copy > state.length { copy = state.length; };
                } else {
                    from = put.offset(-state.offset);
                    copy = state.length;
                }
                if copy > left { copy = left; }
                left -= copy;
                state.length -= copy;
                loop  {
                    *{ let mut _t = put; put = put.offset(1); _t } =
                        *{ let mut _t = from; from = from.offset(1); _t };
                    if { copy -= 1; copy } == 0 { break  };
                }
                if state.length == 0 { state.mode = LEN; }
            }
            CODELENS => {
                while state.have < state.nlen + state.ndist {
                    loop  {
                        here =
                            *state.lencode.offset(((hold as c_uint) &
                                                       ((1 << (state.lenbits))
                                                            - 1)));
                        if ((here.bits) as c_uint) <= bits { break ; }
                        loop  {
                            if have == 0 { break inf_leave ; }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    if here.val < 16 {
                        loop  {
                            hold >>= (here.bits);
                            bits -= (here.bits) as c_uint;
                            if 0 == 0 { break  };
                        }
                        state.lens[{
                                       let mut _t = state.have;
                                       state.have += 1;
                                       _t
                                   }] = here.val;
                    } else {
                        if here.val == 16 {
                            loop  {
                                while bits < ((here.bits + 2) as c_uint) {
                                    loop  {
                                        if have == 0 { break inf_leave ; }
                                        have -= 1;
                                        hold +=
                                            ((*{
                                                   let mut _t = next;
                                                   next = next.offset(1);
                                                   _t
                                               }) as c_long) << bits;
                                        bits += 8;
                                        if 0 == 0 { break  };
                                    };
                                }
                                if 0 == 0 { break  };
                            }
                            loop  {
                                hold >>= (here.bits);
                                bits -= (here.bits) as c_uint;
                                if 0 == 0 { break  };
                            }
                            if state.have == 0 {
                                strm.msg = "invalid bit length repeat";
                                state.mode = BAD;
                                break ;
                            }
                            len = state.lens[state.have - 1];
                            copy = 3 + ((hold as c_uint) & ((1 << 2) - 1));
                            loop  {
                                hold >>= 2;
                                bits -= 2;
                                if 0 == 0 { break  };
                            };
                        } else if here.val == 17 {
                            loop  {
                                while bits < ((here.bits + 3) as c_uint) {
                                    loop  {
                                        if have == 0 { break inf_leave ; }
                                        have -= 1;
                                        hold +=
                                            ((*{
                                                   let mut _t = next;
                                                   next = next.offset(1);
                                                   _t
                                               }) as c_long) << bits;
                                        bits += 8;
                                        if 0 == 0 { break  };
                                    };
                                }
                                if 0 == 0 { break  };
                            }
                            loop  {
                                hold >>= (here.bits);
                                bits -= (here.bits) as c_uint;
                                if 0 == 0 { break  };
                            }
                            len = 0;
                            copy = 3 + ((hold as c_uint) & ((1 << 3) - 1));
                            loop  {
                                hold >>= 3;
                                bits -= 3;
                                if 0 == 0 { break  };
                            };
                        } else {
                            loop  {
                                while bits < ((here.bits + 7) as c_uint) {
                                    loop  {
                                        if have == 0 { break inf_leave ; }
                                        have -= 1;
                                        hold +=
                                            ((*{
                                                   let mut _t = next;
                                                   next = next.offset(1);
                                                   _t
                                               }) as c_long) << bits;
                                        bits += 8;
                                        if 0 == 0 { break  };
                                    };
                                }
                                if 0 == 0 { break  };
                            }
                            loop  {
                                hold >>= (here.bits);
                                bits -= (here.bits) as c_uint;
                                if 0 == 0 { break  };
                            }
                            len = 0;
                            copy = 11 + ((hold as c_uint) & ((1 << 7) - 1));
                            loop  {
                                hold >>= 7;
                                bits -= 7;
                                if 0 == 0 { break  };
                            };
                        }
                        if state.have + copy > state.nlen + state.ndist {
                            strm.msg = "invalid bit length repeat";
                            state.mode = BAD;
                            break ;
                        }
                        while { let mut _t = copy; copy -= 1; _t } != 0 {
                            state.lens[{
                                           let mut _t = state.have;
                                           state.have += 1;
                                           _t
                                       }] = len as u16;
                        };
                    };
                }
                if state.mode == BAD { }
                if state.lens[256] == 0 {
                    strm.msg = "invalid code -- missing end-of-block";
                    state.mode = BAD;
                }
                state.next = state.codes;
                state.lencode = (state.next) as *const code;
                state.lenbits = 9;
                ret =
                    inflate_table(LENS, state.lens, state.nlen,
                                  &mut (state.next), &mut (state.lenbits),
                                  state.work);
                if ret != 0 {
                    strm.msg = "invalid literal/lengths set";
                    state.mode = BAD;
                }
                state.distcode = (state.next) as *const code;
                state.distbits = 6;
                ret =
                    inflate_table(DISTS, state.lens.offset(state.nlen),
                                  state.ndist, &mut (state.next),
                                  &mut (state.distbits), state.work);
                if ret != 0 {
                    strm.msg = "invalid distances set";
                    state.mode = BAD;
                }
                state.mode = LEN_;
                if flush == Z_TREES { break inf_leave ; }
                state.mode = LEN;
                if have >= 6 && left >= 258 {
                    loop  {
                        strm.next_out = put;
                        strm.avail_out = left;
                        strm.next_in = next;
                        strm.avail_in = have;
                        state.hold = hold;
                        state.bits = bits;
                        if 0 == 0 { break  };
                    }
                    inflate_fast(strm, out);
                    loop  {
                        put = strm.next_out;
                        left = strm.avail_out;
                        next = strm.next_in;
                        have = strm.avail_in;
                        hold = state.hold;
                        bits = state.bits;
                        if 0 == 0 { break  };
                    }
                    if state.mode == TYPE { state.back = -1; };
                }
                state.back = 0;
                loop  {
                    here =
                        *state.lencode.offset(((hold as c_uint) &
                                                   ((1 << (state.lenbits)) -
                                                        1)));
                    if ((here.bits) as c_uint) <= bits { break ; }
                    loop  {
                        if have == 0 { break inf_leave ; }
                        have -= 1;
                        hold +=
                            ((*{
                                   let mut _t = next;
                                   next = next.offset(1);
                                   _t
                               }) as c_long) << bits;
                        bits += 8;
                        if 0 == 0 { break  };
                    };
                }
                if here.op != 0 && (here.op & 240) == 0 {
                    last = here;
                    loop  {
                        here =
                            *state.lencode.offset(last.val +
                                                      (((hold as c_uint) &
                                                            ((1 <<
                                                                  (last.bits +
                                                                       last.op))
                                                                 - 1)) >>
                                                           last.bits));
                        if ((last.bits + here.bits) as c_uint) <= bits {
                            break ;
                        }
                        loop  {
                            if have == 0 { break inf_leave ; }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    loop  {
                        hold >>= (last.bits);
                        bits -= (last.bits) as c_uint;
                        if 0 == 0 { break  };
                    }
                    state.back += last.bits;
                }
                loop  {
                    hold >>= (here.bits);
                    bits -= (here.bits) as c_uint;
                    if 0 == 0 { break  };
                }
                state.back += here.bits;
                state.length = here.val as c_uint;
                if ((here.op) as c_int) == 0 { state.mode = LIT; }
                if here.op & 32 != 0 { state.back = -1; state.mode = TYPE; }
                if here.op & 64 != 0 {
                    strm.msg = "invalid literal/length code";
                    state.mode = BAD;
                }
                state.extra = ((here.op) as c_uint) & 15;
                state.mode = LENEXT;
                if state.extra != 0 {
                    loop  {
                        while bits < (state.extra) {
                            loop  {
                                if have == 0 { break inf_leave ; }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    state.length +=
                        ((hold as c_uint) & ((1 << (state.extra)) - 1));
                    loop  {
                        hold >>= (state.extra);
                        bits -= (state.extra);
                        if 0 == 0 { break  };
                    }
                    state.back += state.extra;
                }
                state.was = state.length;
                state.mode = DIST;
                loop  {
                    here =
                        *state.distcode.offset(((hold as c_uint) &
                                                    ((1 << (state.distbits)) -
                                                         1)));
                    if ((here.bits) as c_uint) <= bits { break ; }
                    loop  {
                        if have == 0 { break inf_leave ; }
                        have -= 1;
                        hold +=
                            ((*{
                                   let mut _t = next;
                                   next = next.offset(1);
                                   _t
                               }) as c_long) << bits;
                        bits += 8;
                        if 0 == 0 { break  };
                    };
                }
                if (here.op & 240) == 0 {
                    last = here;
                    loop  {
                        here =
                            *state.distcode.offset(last.val +
                                                       (((hold as c_uint) &
                                                             ((1 <<
                                                                   (last.bits
                                                                        +
                                                                        last.op))
                                                                  - 1)) >>
                                                            last.bits));
                        if ((last.bits + here.bits) as c_uint) <= bits {
                            break ;
                        }
                        loop  {
                            if have == 0 { break inf_leave ; }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    loop  {
                        hold >>= (last.bits);
                        bits -= (last.bits) as c_uint;
                        if 0 == 0 { break  };
                    }
                    state.back += last.bits;
                }
                loop  {
                    hold >>= (here.bits);
                    bits -= (here.bits) as c_uint;
                    if 0 == 0 { break  };
                }
                state.back += here.bits;
                if here.op & 64 != 0 {
                    strm.msg = "invalid distance code";
                    state.mode = BAD;
                }
                state.offset = here.val as c_uint;
                state.extra = ((here.op) as c_uint) & 15;
                state.mode = DISTEXT;
                if state.extra != 0 {
                    loop  {
                        while bits < (state.extra) {
                            loop  {
                                if have == 0 { break inf_leave ; }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    state.offset +=
                        ((hold as c_uint) & ((1 << (state.extra)) - 1));
                    loop  {
                        hold >>= (state.extra);
                        bits -= (state.extra);
                        if 0 == 0 { break  };
                    }
                    state.back += state.extra;
                }
                state.mode = MATCH;
                if left == 0 { break inf_leave ; }
                copy = out - left;
                if state.offset > copy {
                    copy = state.offset - copy;
                    if copy > state.whave {
                        if state.sane != 0 {
                            strm.msg = "invalid distance too far back";
                            state.mode = BAD;
                        };
                    }
                    if copy > state.wnext {
                        copy -= state.wnext;
                        from = state.window.offset((state.wsize - copy));
                    } else {
                        from = state.window.offset((state.wnext - copy));
                    }
                    if copy > state.length { copy = state.length; };
                } else {
                    from = put.offset(-state.offset);
                    copy = state.length;
                }
                if copy > left { copy = left; }
                left -= copy;
                state.length -= copy;
                loop  {
                    *{ let mut _t = put; put = put.offset(1); _t } =
                        *{ let mut _t = from; from = from.offset(1); _t };
                    if { copy -= 1; copy } == 0 { break  };
                }
                if state.length == 0 { state.mode = LEN; }
            }
            LEN_ => {
                state.mode = LEN;
                if have >= 6 && left >= 258 {
                    loop  {
                        strm.next_out = put;
                        strm.avail_out = left;
                        strm.next_in = next;
                        strm.avail_in = have;
                        state.hold = hold;
                        state.bits = bits;
                        if 0 == 0 { break  };
                    }
                    inflate_fast(strm, out);
                    loop  {
                        put = strm.next_out;
                        left = strm.avail_out;
                        next = strm.next_in;
                        have = strm.avail_in;
                        hold = state.hold;
                        bits = state.bits;
                        if 0 == 0 { break  };
                    }
                    if state.mode == TYPE { state.back = -1; };
                }
                state.back = 0;
                loop  {
                    here =
                        *state.lencode.offset(((hold as c_uint) &
                                                   ((1 << (state.lenbits)) -
                                                        1)));
                    if ((here.bits) as c_uint) <= bits { break ; }
                    loop  {
                        if have == 0 { break inf_leave ; }
                        have -= 1;
                        hold +=
                            ((*{
                                   let mut _t = next;
                                   next = next.offset(1);
                                   _t
                               }) as c_long) << bits;
                        bits += 8;
                        if 0 == 0 { break  };
                    };
                }
                if here.op != 0 && (here.op & 240) == 0 {
                    last = here;
                    loop  {
                        here =
                            *state.lencode.offset(last.val +
                                                      (((hold as c_uint) &
                                                            ((1 <<
                                                                  (last.bits +
                                                                       last.op))
                                                                 - 1)) >>
                                                           last.bits));
                        if ((last.bits + here.bits) as c_uint) <= bits {
                            break ;
                        }
                        loop  {
                            if have == 0 { break inf_leave ; }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    loop  {
                        hold >>= (last.bits);
                        bits -= (last.bits) as c_uint;
                        if 0 == 0 { break  };
                    }
                    state.back += last.bits;
                }
                loop  {
                    hold >>= (here.bits);
                    bits -= (here.bits) as c_uint;
                    if 0 == 0 { break  };
                }
                state.back += here.bits;
                state.length = here.val as c_uint;
                if ((here.op) as c_int) == 0 { state.mode = LIT; }
                if here.op & 32 != 0 { state.back = -1; state.mode = TYPE; }
                if here.op & 64 != 0 {
                    strm.msg = "invalid literal/length code";
                    state.mode = BAD;
                }
                state.extra = ((here.op) as c_uint) & 15;
                state.mode = LENEXT;
                if state.extra != 0 {
                    loop  {
                        while bits < (state.extra) {
                            loop  {
                                if have == 0 { break inf_leave ; }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    state.length +=
                        ((hold as c_uint) & ((1 << (state.extra)) - 1));
                    loop  {
                        hold >>= (state.extra);
                        bits -= (state.extra);
                        if 0 == 0 { break  };
                    }
                    state.back += state.extra;
                }
                state.was = state.length;
                state.mode = DIST;
                loop  {
                    here =
                        *state.distcode.offset(((hold as c_uint) &
                                                    ((1 << (state.distbits)) -
                                                         1)));
                    if ((here.bits) as c_uint) <= bits { break ; }
                    loop  {
                        if have == 0 { break inf_leave ; }
                        have -= 1;
                        hold +=
                            ((*{
                                   let mut _t = next;
                                   next = next.offset(1);
                                   _t
                               }) as c_long) << bits;
                        bits += 8;
                        if 0 == 0 { break  };
                    };
                }
                if (here.op & 240) == 0 {
                    last = here;
                    loop  {
                        here =
                            *state.distcode.offset(last.val +
                                                       (((hold as c_uint) &
                                                             ((1 <<
                                                                   (last.bits
                                                                        +
                                                                        last.op))
                                                                  - 1)) >>
                                                            last.bits));
                        if ((last.bits + here.bits) as c_uint) <= bits {
                            break ;
                        }
                        loop  {
                            if have == 0 { break inf_leave ; }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    loop  {
                        hold >>= (last.bits);
                        bits -= (last.bits) as c_uint;
                        if 0 == 0 { break  };
                    }
                    state.back += last.bits;
                }
                loop  {
                    hold >>= (here.bits);
                    bits -= (here.bits) as c_uint;
                    if 0 == 0 { break  };
                }
                state.back += here.bits;
                if here.op & 64 != 0 {
                    strm.msg = "invalid distance code";
                    state.mode = BAD;
                }
                state.offset = here.val as c_uint;
                state.extra = ((here.op) as c_uint) & 15;
                state.mode = DISTEXT;
                if state.extra != 0 {
                    loop  {
                        while bits < (state.extra) {
                            loop  {
                                if have == 0 { break inf_leave ; }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    state.offset +=
                        ((hold as c_uint) & ((1 << (state.extra)) - 1));
                    loop  {
                        hold >>= (state.extra);
                        bits -= (state.extra);
                        if 0 == 0 { break  };
                    }
                    state.back += state.extra;
                }
                state.mode = MATCH;
                if left == 0 { break inf_leave ; }
                copy = out - left;
                if state.offset > copy {
                    copy = state.offset - copy;
                    if copy > state.whave {
                        if state.sane != 0 {
                            strm.msg = "invalid distance too far back";
                            state.mode = BAD;
                        };
                    }
                    if copy > state.wnext {
                        copy -= state.wnext;
                        from = state.window.offset((state.wsize - copy));
                    } else {
                        from = state.window.offset((state.wnext - copy));
                    }
                    if copy > state.length { copy = state.length; };
                } else {
                    from = put.offset(-state.offset);
                    copy = state.length;
                }
                if copy > left { copy = left; }
                left -= copy;
                state.length -= copy;
                loop  {
                    *{ let mut _t = put; put = put.offset(1); _t } =
                        *{ let mut _t = from; from = from.offset(1); _t };
                    if { copy -= 1; copy } == 0 { break  };
                }
                if state.length == 0 { state.mode = LEN; }
            }
            LEN => {
                if have >= 6 && left >= 258 {
                    loop  {
                        strm.next_out = put;
                        strm.avail_out = left;
                        strm.next_in = next;
                        strm.avail_in = have;
                        state.hold = hold;
                        state.bits = bits;
                        if 0 == 0 { break  };
                    }
                    inflate_fast(strm, out);
                    loop  {
                        put = strm.next_out;
                        left = strm.avail_out;
                        next = strm.next_in;
                        have = strm.avail_in;
                        hold = state.hold;
                        bits = state.bits;
                        if 0 == 0 { break  };
                    }
                    if state.mode == TYPE { state.back = -1; };
                }
                state.back = 0;
                loop  {
                    here =
                        *state.lencode.offset(((hold as c_uint) &
                                                   ((1 << (state.lenbits)) -
                                                        1)));
                    if ((here.bits) as c_uint) <= bits { break ; }
                    loop  {
                        if have == 0 { break inf_leave ; }
                        have -= 1;
                        hold +=
                            ((*{
                                   let mut _t = next;
                                   next = next.offset(1);
                                   _t
                               }) as c_long) << bits;
                        bits += 8;
                        if 0 == 0 { break  };
                    };
                }
                if here.op != 0 && (here.op & 240) == 0 {
                    last = here;
                    loop  {
                        here =
                            *state.lencode.offset(last.val +
                                                      (((hold as c_uint) &
                                                            ((1 <<
                                                                  (last.bits +
                                                                       last.op))
                                                                 - 1)) >>
                                                           last.bits));
                        if ((last.bits + here.bits) as c_uint) <= bits {
                            break ;
                        }
                        loop  {
                            if have == 0 { break inf_leave ; }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    loop  {
                        hold >>= (last.bits);
                        bits -= (last.bits) as c_uint;
                        if 0 == 0 { break  };
                    }
                    state.back += last.bits;
                }
                loop  {
                    hold >>= (here.bits);
                    bits -= (here.bits) as c_uint;
                    if 0 == 0 { break  };
                }
                state.back += here.bits;
                state.length = here.val as c_uint;
                if ((here.op) as c_int) == 0 { state.mode = LIT; }
                if here.op & 32 != 0 { state.back = -1; state.mode = TYPE; }
                if here.op & 64 != 0 {
                    strm.msg = "invalid literal/length code";
                    state.mode = BAD;
                }
                state.extra = ((here.op) as c_uint) & 15;
                state.mode = LENEXT;
                if state.extra != 0 {
                    loop  {
                        while bits < (state.extra) {
                            loop  {
                                if have == 0 { break inf_leave ; }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    state.length +=
                        ((hold as c_uint) & ((1 << (state.extra)) - 1));
                    loop  {
                        hold >>= (state.extra);
                        bits -= (state.extra);
                        if 0 == 0 { break  };
                    }
                    state.back += state.extra;
                }
                state.was = state.length;
                state.mode = DIST;
                loop  {
                    here =
                        *state.distcode.offset(((hold as c_uint) &
                                                    ((1 << (state.distbits)) -
                                                         1)));
                    if ((here.bits) as c_uint) <= bits { break ; }
                    loop  {
                        if have == 0 { break inf_leave ; }
                        have -= 1;
                        hold +=
                            ((*{
                                   let mut _t = next;
                                   next = next.offset(1);
                                   _t
                               }) as c_long) << bits;
                        bits += 8;
                        if 0 == 0 { break  };
                    };
                }
                if (here.op & 240) == 0 {
                    last = here;
                    loop  {
                        here =
                            *state.distcode.offset(last.val +
                                                       (((hold as c_uint) &
                                                             ((1 <<
                                                                   (last.bits
                                                                        +
                                                                        last.op))
                                                                  - 1)) >>
                                                            last.bits));
                        if ((last.bits + here.bits) as c_uint) <= bits {
                            break ;
                        }
                        loop  {
                            if have == 0 { break inf_leave ; }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    loop  {
                        hold >>= (last.bits);
                        bits -= (last.bits) as c_uint;
                        if 0 == 0 { break  };
                    }
                    state.back += last.bits;
                }
                loop  {
                    hold >>= (here.bits);
                    bits -= (here.bits) as c_uint;
                    if 0 == 0 { break  };
                }
                state.back += here.bits;
                if here.op & 64 != 0 {
                    strm.msg = "invalid distance code";
                    state.mode = BAD;
                }
                state.offset = here.val as c_uint;
                state.extra = ((here.op) as c_uint) & 15;
                state.mode = DISTEXT;
                if state.extra != 0 {
                    loop  {
                        while bits < (state.extra) {
                            loop  {
                                if have == 0 { break inf_leave ; }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    state.offset +=
                        ((hold as c_uint) & ((1 << (state.extra)) - 1));
                    loop  {
                        hold >>= (state.extra);
                        bits -= (state.extra);
                        if 0 == 0 { break  };
                    }
                    state.back += state.extra;
                }
                state.mode = MATCH;
                if left == 0 { break inf_leave ; }
                copy = out - left;
                if state.offset > copy {
                    copy = state.offset - copy;
                    if copy > state.whave {
                        if state.sane != 0 {
                            strm.msg = "invalid distance too far back";
                            state.mode = BAD;
                        };
                    }
                    if copy > state.wnext {
                        copy -= state.wnext;
                        from = state.window.offset((state.wsize - copy));
                    } else {
                        from = state.window.offset((state.wnext - copy));
                    }
                    if copy > state.length { copy = state.length; };
                } else {
                    from = put.offset(-state.offset);
                    copy = state.length;
                }
                if copy > left { copy = left; }
                left -= copy;
                state.length -= copy;
                loop  {
                    *{ let mut _t = put; put = put.offset(1); _t } =
                        *{ let mut _t = from; from = from.offset(1); _t };
                    if { copy -= 1; copy } == 0 { break  };
                }
                if state.length == 0 { state.mode = LEN; }
            }
            LENEXT => {
                if state.extra != 0 {
                    loop  {
                        while bits < (state.extra) {
                            loop  {
                                if have == 0 { break inf_leave ; }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    state.length +=
                        ((hold as c_uint) & ((1 << (state.extra)) - 1));
                    loop  {
                        hold >>= (state.extra);
                        bits -= (state.extra);
                        if 0 == 0 { break  };
                    }
                    state.back += state.extra;
                }
                state.was = state.length;
                state.mode = DIST;
                loop  {
                    here =
                        *state.distcode.offset(((hold as c_uint) &
                                                    ((1 << (state.distbits)) -
                                                         1)));
                    if ((here.bits) as c_uint) <= bits { break ; }
                    loop  {
                        if have == 0 { break inf_leave ; }
                        have -= 1;
                        hold +=
                            ((*{
                                   let mut _t = next;
                                   next = next.offset(1);
                                   _t
                               }) as c_long) << bits;
                        bits += 8;
                        if 0 == 0 { break  };
                    };
                }
                if (here.op & 240) == 0 {
                    last = here;
                    loop  {
                        here =
                            *state.distcode.offset(last.val +
                                                       (((hold as c_uint) &
                                                             ((1 <<
                                                                   (last.bits
                                                                        +
                                                                        last.op))
                                                                  - 1)) >>
                                                            last.bits));
                        if ((last.bits + here.bits) as c_uint) <= bits {
                            break ;
                        }
                        loop  {
                            if have == 0 { break inf_leave ; }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    loop  {
                        hold >>= (last.bits);
                        bits -= (last.bits) as c_uint;
                        if 0 == 0 { break  };
                    }
                    state.back += last.bits;
                }
                loop  {
                    hold >>= (here.bits);
                    bits -= (here.bits) as c_uint;
                    if 0 == 0 { break  };
                }
                state.back += here.bits;
                if here.op & 64 != 0 {
                    strm.msg = "invalid distance code";
                    state.mode = BAD;
                }
                state.offset = here.val as c_uint;
                state.extra = ((here.op) as c_uint) & 15;
                state.mode = DISTEXT;
                if state.extra != 0 {
                    loop  {
                        while bits < (state.extra) {
                            loop  {
                                if have == 0 { break inf_leave ; }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    state.offset +=
                        ((hold as c_uint) & ((1 << (state.extra)) - 1));
                    loop  {
                        hold >>= (state.extra);
                        bits -= (state.extra);
                        if 0 == 0 { break  };
                    }
                    state.back += state.extra;
                }
                state.mode = MATCH;
                if left == 0 { break inf_leave ; }
                copy = out - left;
                if state.offset > copy {
                    copy = state.offset - copy;
                    if copy > state.whave {
                        if state.sane != 0 {
                            strm.msg = "invalid distance too far back";
                            state.mode = BAD;
                        };
                    }
                    if copy > state.wnext {
                        copy -= state.wnext;
                        from = state.window.offset((state.wsize - copy));
                    } else {
                        from = state.window.offset((state.wnext - copy));
                    }
                    if copy > state.length { copy = state.length; };
                } else {
                    from = put.offset(-state.offset);
                    copy = state.length;
                }
                if copy > left { copy = left; }
                left -= copy;
                state.length -= copy;
                loop  {
                    *{ let mut _t = put; put = put.offset(1); _t } =
                        *{ let mut _t = from; from = from.offset(1); _t };
                    if { copy -= 1; copy } == 0 { break  };
                }
                if state.length == 0 { state.mode = LEN; }
            }
            DIST => {
                loop  {
                    here =
                        *state.distcode.offset(((hold as c_uint) &
                                                    ((1 << (state.distbits)) -
                                                         1)));
                    if ((here.bits) as c_uint) <= bits { break ; }
                    loop  {
                        if have == 0 { break inf_leave ; }
                        have -= 1;
                        hold +=
                            ((*{
                                   let mut _t = next;
                                   next = next.offset(1);
                                   _t
                               }) as c_long) << bits;
                        bits += 8;
                        if 0 == 0 { break  };
                    };
                }
                if (here.op & 240) == 0 {
                    last = here;
                    loop  {
                        here =
                            *state.distcode.offset(last.val +
                                                       (((hold as c_uint) &
                                                             ((1 <<
                                                                   (last.bits
                                                                        +
                                                                        last.op))
                                                                  - 1)) >>
                                                            last.bits));
                        if ((last.bits + here.bits) as c_uint) <= bits {
                            break ;
                        }
                        loop  {
                            if have == 0 { break inf_leave ; }
                            have -= 1;
                            hold +=
                                ((*{
                                       let mut _t = next;
                                       next = next.offset(1);
                                       _t
                                   }) as c_long) << bits;
                            bits += 8;
                            if 0 == 0 { break  };
                        };
                    }
                    loop  {
                        hold >>= (last.bits);
                        bits -= (last.bits) as c_uint;
                        if 0 == 0 { break  };
                    }
                    state.back += last.bits;
                }
                loop  {
                    hold >>= (here.bits);
                    bits -= (here.bits) as c_uint;
                    if 0 == 0 { break  };
                }
                state.back += here.bits;
                if here.op & 64 != 0 {
                    strm.msg = "invalid distance code";
                    state.mode = BAD;
                }
                state.offset = here.val as c_uint;
                state.extra = ((here.op) as c_uint) & 15;
                state.mode = DISTEXT;
                if state.extra != 0 {
                    loop  {
                        while bits < (state.extra) {
                            loop  {
                                if have == 0 { break inf_leave ; }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    state.offset +=
                        ((hold as c_uint) & ((1 << (state.extra)) - 1));
                    loop  {
                        hold >>= (state.extra);
                        bits -= (state.extra);
                        if 0 == 0 { break  };
                    }
                    state.back += state.extra;
                }
                state.mode = MATCH;
                if left == 0 { break inf_leave ; }
                copy = out - left;
                if state.offset > copy {
                    copy = state.offset - copy;
                    if copy > state.whave {
                        if state.sane != 0 {
                            strm.msg = "invalid distance too far back";
                            state.mode = BAD;
                        };
                    }
                    if copy > state.wnext {
                        copy -= state.wnext;
                        from = state.window.offset((state.wsize - copy));
                    } else {
                        from = state.window.offset((state.wnext - copy));
                    }
                    if copy > state.length { copy = state.length; };
                } else {
                    from = put.offset(-state.offset);
                    copy = state.length;
                }
                if copy > left { copy = left; }
                left -= copy;
                state.length -= copy;
                loop  {
                    *{ let mut _t = put; put = put.offset(1); _t } =
                        *{ let mut _t = from; from = from.offset(1); _t };
                    if { copy -= 1; copy } == 0 { break  };
                }
                if state.length == 0 { state.mode = LEN; }
            }
            DISTEXT => {
                if state.extra != 0 {
                    loop  {
                        while bits < (state.extra) {
                            loop  {
                                if have == 0 { break inf_leave ; }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    state.offset +=
                        ((hold as c_uint) & ((1 << (state.extra)) - 1));
                    loop  {
                        hold >>= (state.extra);
                        bits -= (state.extra);
                        if 0 == 0 { break  };
                    }
                    state.back += state.extra;
                }
                state.mode = MATCH;
                if left == 0 { break inf_leave ; }
                copy = out - left;
                if state.offset > copy {
                    copy = state.offset - copy;
                    if copy > state.whave {
                        if state.sane != 0 {
                            strm.msg = "invalid distance too far back";
                            state.mode = BAD;
                        };
                    }
                    if copy > state.wnext {
                        copy -= state.wnext;
                        from = state.window.offset((state.wsize - copy));
                    } else {
                        from = state.window.offset((state.wnext - copy));
                    }
                    if copy > state.length { copy = state.length; };
                } else {
                    from = put.offset(-state.offset);
                    copy = state.length;
                }
                if copy > left { copy = left; }
                left -= copy;
                state.length -= copy;
                loop  {
                    *{ let mut _t = put; put = put.offset(1); _t } =
                        *{ let mut _t = from; from = from.offset(1); _t };
                    if { copy -= 1; copy } == 0 { break  };
                }
                if state.length == 0 { state.mode = LEN; }
            }
            MATCH => {
                if left == 0 { break inf_leave ; }
                copy = out - left;
                if state.offset > copy {
                    copy = state.offset - copy;
                    if copy > state.whave {
                        if state.sane != 0 {
                            strm.msg = "invalid distance too far back";
                            state.mode = BAD;
                        };
                    }
                    if copy > state.wnext {
                        copy -= state.wnext;
                        from = state.window.offset((state.wsize - copy));
                    } else {
                        from = state.window.offset((state.wnext - copy));
                    }
                    if copy > state.length { copy = state.length; };
                } else {
                    from = put.offset(-state.offset);
                    copy = state.length;
                }
                if copy > left { copy = left; }
                left -= copy;
                state.length -= copy;
                loop  {
                    *{ let mut _t = put; put = put.offset(1); _t } =
                        *{ let mut _t = from; from = from.offset(1); _t };
                    if { copy -= 1; copy } == 0 { break  };
                }
                if state.length == 0 { state.mode = LEN; }
            }
            LIT => {
                if left == 0 { break inf_leave ; }
                *{ let mut _t = put; put = put.offset(1); _t } =
                    (state.length) as u8;
                left -= 1;
                state.mode = LEN
            }
            CHECK => {
                if state.wrap != 0 {
                    loop  {
                        while bits < 32 {
                            loop  {
                                if have == 0 { break inf_leave ; }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    out -= left;
                    strm.total_out += out;
                    state.total += out;
                    if (state.wrap & 4) != 0 && out != 0 {
                        strm.adler =
                            {
                                state.check =
                                    if state.flags != 0 {
                                        crc32(state.check, put.offset(-out),
                                              out)
                                    } else {
                                        adler32(state.check, put.offset(-out),
                                                out)
                                    };
                                state.check
                            };
                    }
                    out = left;
                    if (state.wrap & 4) != 0 &&
                           if state.flags != 0 {
                               hold
                           } else {
                               (((hold) >> 24) & 255) +
                                   (((hold) >> 8) & 65280) +
                                   (((hold) & 65280) << 8) +
                                   (((hold) & 255) << 24)
                           } != state.check {
                        strm.msg = "incorrect data check";
                        state.mode = BAD;
                    }
                    loop  { hold = 0; bits = 0; if 0 == 0 { break  }; };
                }
                state.mode = LENGTH;
                if state.wrap != 0 && state.flags != 0 {
                    loop  {
                        while bits < 32 {
                            loop  {
                                if have == 0 { break inf_leave ; }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    if hold != (state.total & 4294967295) {
                        strm.msg = "incorrect length check";
                        state.mode = BAD;
                    }
                    loop  { hold = 0; bits = 0; if 0 == 0 { break  }; };
                }
                state.mode = DONE;
                ret = Z_STREAM_END;
                break inf_leave
            }
            LENGTH => {
                if state.wrap != 0 && state.flags != 0 {
                    loop  {
                        while bits < 32 {
                            loop  {
                                if have == 0 { break inf_leave ; }
                                have -= 1;
                                hold +=
                                    ((*{
                                           let mut _t = next;
                                           next = next.offset(1);
                                           _t
                                       }) as c_long) << bits;
                                bits += 8;
                                if 0 == 0 { break  };
                            };
                        }
                        if 0 == 0 { break  };
                    }
                    if hold != (state.total & 4294967295) {
                        strm.msg = "incorrect length check";
                        state.mode = BAD;
                    }
                    loop  { hold = 0; bits = 0; if 0 == 0 { break  }; };
                }
                state.mode = DONE;
                ret = Z_STREAM_END;
                break inf_leave
            }
            DONE => { ret = Z_STREAM_END; break inf_leave  }
            BAD => { ret = Z_DATA_ERROR; break inf_leave  }
            MEM => return Z_MEM_ERROR,
            _ => return Z_STREAM_ERROR,
        }
    }
    inf_leave:
        loop  {
            loop  {
                strm.next_out = put;
                strm.avail_out = left;
                strm.next_in = next;
                strm.avail_in = have;
                state.hold = hold;
                state.bits = bits;
                if 0 == 0 { break  };
            }
            break
        }
    if state.wsize != 0 ||
           (out != strm.avail_out && state.mode < BAD &&
                (state.mode < CHECK || flush != Z_FINISH)) {
        if updatewindow(strm, strm.next_out, out - strm.avail_out) != 0 {
            state.mode = MEM;
            return Z_MEM_ERROR;
        }
    }
    in_ -= strm.avail_in;
    out -= strm.avail_out;
    strm.total_in += in_;
    strm.total_out += out;
    state.total += out;
    if (state.wrap & 4) != 0 && out != 0 {
        strm.adler =
            {
                state.check =
                    if state.flags != 0 {
                        crc32(state.check, strm.next_out.offset(-out), out)
                    } else {
                        adler32(state.check, strm.next_out.offset(-out), out)
                    };
                state.check
            };
    }
    strm.data_type =
        (state.bits as c_int) + if state.last != 0 { 64 } else { 0 } +
            if state.mode == TYPE { 128 } else { 0 } +
            if state.mode == LEN_ || state.mode == COPY_ { 256 } else { 0 };
    if ((in_ == 0 && out == 0) || flush == Z_FINISH) && ret == Z_OK {
        ret = Z_BUF_ERROR;
    }
    return ret;
}
#[no_mangle]
pub unsafe extern "C" fn inflateEnd(mut strm: z_streamp) -> c_int {
    if inflateStateCheck(strm) != 0 { return Z_STREAM_ERROR; }
    let mut state = strm.state as *mut inflate_state;
    if state.window != Z_NULL {
        (*((strm).zfree))((strm).opaque, (state.window) as voidpf);
    }
    (*((strm).zfree))((strm).opaque, (strm.state) as voidpf);
    strm.state = Z_NULL;
    return Z_OK;
}
#[no_mangle]
pub unsafe extern "C" fn inflateGetDictionary(mut strm: z_streamp,
                                              mut dictionary: *mut Bytef,
                                              mut dictLength: &mut uInt)
 -> c_int {
    if inflateStateCheck(strm) != 0 { return Z_STREAM_ERROR; }
    let mut state = strm.state as *mut inflate_state;
    if state.whave != 0 && dictionary != Z_NULL {
        memcpy(dictionary as *mut _, state.window.offset(state.wnext),
               state.whave - state.wnext);
        memcpy(dictionary.offset(state.whave).offset(-state.wnext),
               state.window as *const _, state.wnext);
    }
    if dictLength != Z_NULL { *dictLength = state.whave; }
    return Z_OK;
}
#[no_mangle]
pub unsafe extern "C" fn inflateSetDictionary(mut strm: z_streamp,
                                              mut dictionary: *const Bytef,
                                              mut dictLength: uInt) -> c_int {
    let mut dictid: c_long;
    if inflateStateCheck(strm) != 0 { return Z_STREAM_ERROR; }
    let mut state = strm.state as *mut inflate_state;
    if state.wrap != 0 && state.mode != DICT { return Z_STREAM_ERROR; }
    if state.mode == DICT {
        dictid = adler32(0, Z_NULL, 0);
        dictid = adler32(dictid, dictionary, dictLength);
        if dictid != state.check { return Z_DATA_ERROR; };
    }
    /* copy dictionary to window using updatewindow(), which will amend the
           existing dictionary if appropriate */
    let mut ret =
        updatewindow(strm, dictionary.offset(dictLength),
                     dictLength); /* check state */
    if ret != 0 {
        state.mode = MEM; /* save header structure */
        return Z_MEM_ERROR;
    }
    state.havedict = 1;
    return Z_OK;
}
#[no_mangle]
pub unsafe extern "C" fn inflateGetHeader(mut strm: z_streamp,
                                          mut head: gz_headerp) -> c_int {
    if inflateStateCheck(strm) != 0 { return Z_STREAM_ERROR; }
    let mut state = strm.state as *mut inflate_state;
    if (state.wrap & 2) == 0 { return Z_STREAM_ERROR; }
    state.head = head;
    head.done = 0;
    return Z_OK;
}
/*
   Search buf[0..len-1] for the pattern: 0, 0, 0xff, 0xff.  Return when found
   or when out of input.  When called, *have is the number of pattern bytes
   found in order so far, in 0..3.  On return *have is updated to the new
   state.  If on return *have equals four, then the pattern was found and the
   return value is how many bytes were read including the last byte of the
   pattern.  If *have is less than four, then the pattern has not been found
   yet and the return value is len.  In the latter case, syncsearch() can be
   called again with more data and the *have state.  *have is initialized to
   zero for the first call.
 */
unsafe fn syncsearch(mut have: &mut c_uint, mut buf: *const u8,
                     mut len: c_uint) -> c_uint {
    let mut got = *have; /* number of bytes to look at or looked at */
    let mut next = 0; /* temporary to save total_in and total_out */
    while next < len && got < 4 {
        if ((buf[next]) as c_int) == if got < 2 { 0 } else { 255 } {
            got += 1; /* to restore bit buffer to byte string */
        } else if buf[next] != 0 {
            got = 0; /* check parameters */
        } else {
            got = 4 - got; /* if first time, start search in bit buffer */
        } /* search available input */
        next +=
            1; /* return no joy or set up to restart inflate() on a new block */
    }
    *have = got;
    return next;
}
#[no_mangle]
pub unsafe extern "C" fn inflateSync(mut strm: z_streamp) -> c_int {
    let mut len: c_uint;
    let mut buf: [u8; 4];
    if inflateStateCheck(strm) != 0 { return Z_STREAM_ERROR; }
    let mut state = strm.state as *mut inflate_state;
    if strm.avail_in == 0 && state.bits < 8 { return Z_BUF_ERROR; }
    if state.mode != SYNC {
        state.mode = SYNC;
        state.hold <<= state.bits & 7;
        state.bits -= state.bits & 7;
        len = 0;
        while state.bits >= 8 {
            buf[{ let mut _t = len; len += 1; _t }] = (state.hold) as u8;
            state.hold >>= 8;
            state.bits -= 8;
        }
        state.have = 0;
        syncsearch(&mut (state.have), buf, len);
    }
    len = syncsearch(&mut (state.have), strm.next_in, strm.avail_in);
    strm.avail_in -= len;
    strm.next_in = strm.next_in.offset(len);
    strm.total_in += len;
    if state.have != 4 { return Z_DATA_ERROR; }
    let mut in_ = strm.total_in;
    let mut out = strm.total_out;
    inflateReset(strm);
    strm.total_in = in_;
    strm.total_out = out;
    state.mode = TYPE;
    return Z_OK;
}
/*
   Returns true if inflate is currently at the end of a block generated by
   Z_SYNC_FLUSH or Z_FULL_FLUSH. This function is used by one PPP
   implementation to provide an additional safety check. PPP uses
   Z_SYNC_FLUSH but removes the length bytes of the resulting empty stored
   block. When decompressing, PPP checks that at the end of input packet,
   inflate is waiting for these length bytes.
 */
#[no_mangle]
pub unsafe extern "C" fn inflateSyncPoint(mut strm: z_streamp) -> c_int {
    if inflateStateCheck(strm) != 0 {
        return Z_STREAM_ERROR; /* check input */
    } /* allocate space */
    let mut state = strm.state as *mut inflate_state; /* copy state */
    return state.mode == STORED && state.bits == 0;
}
pub static ENOUGH: c_int = 852 + 592;
#[no_mangle]
pub unsafe extern "C" fn inflateCopy(mut dest: z_streamp,
                                     mut source: z_streamp) -> c_int {
    let mut wsize: c_uint;
    if inflateStateCheck(source) != 0 || dest == Z_NULL {
        return Z_STREAM_ERROR;
    }
    let mut state = source.state as *mut inflate_state;
    let mut copy =
        (*((source).zalloc))((source).opaque, 1,
                             std::mem::size_of::<inflate_state>()) as
            *mut inflate_state;
    if copy == Z_NULL { return Z_MEM_ERROR; }
    let mut window = Z_NULL;
    if state.window != Z_NULL {
        window =
            (*((source).zalloc))((source).opaque, (1 << state.wbits),
                                 std::mem::size_of::<u8>()) as *mut u8;
        if window == Z_NULL {
            (*((source).zfree))((source).opaque, (copy) as voidpf);
            return Z_MEM_ERROR;
        };
    }
    memcpy(dest as voidpf, source as voidpf, std::mem::size_of::<z_stream>());
    memcpy(copy as voidpf, state as voidpf,
           std::mem::size_of::<inflate_state>());
    copy.strm = dest;
    if state.lencode >= state.codes &&
           state.lencode <= state.codes.offset(ENOUGH).offset(-1) {
        copy.lencode = copy.codes.offset(state.lencode.offset(-state.codes));
        copy.distcode =
            copy.codes.offset(state.distcode.offset(-state.codes));
    }
    copy.next = copy.codes.offset(state.next.offset(-state.codes));
    if window != Z_NULL {
        wsize = 1 << state.wbits;
        memcpy(window as *mut _, state.window as *const _, wsize);
    }
    copy.window = window;
    dest.state = copy as *mut internal_state;
    return Z_OK;
}
#[no_mangle]
pub unsafe extern "C" fn inflateUndermine(mut strm: z_streamp,
                                          mut subvert: c_int) -> c_int {
    if inflateStateCheck(strm) != 0 { return Z_STREAM_ERROR; }
    let mut state = strm.state as *mut inflate_state;
    subvert;
    state.sane = 1;
    return Z_DATA_ERROR;
}
#[no_mangle]
pub unsafe extern "C" fn inflateValidate(mut strm: z_streamp,
                                         mut check: c_int) -> c_int {
    if inflateStateCheck(strm) != 0 { return Z_STREAM_ERROR; }
    let mut state = strm.state as *mut inflate_state;
    if check != 0 { state.wrap |= 4; } else { state.wrap &= !4; }
    return Z_OK;
}
#[no_mangle]
pub unsafe extern "C" fn inflateMark(mut strm: z_streamp) -> c_long {
    if inflateStateCheck(strm) != 0 { return -(1 << 16); }
    let mut state = strm.state as *mut inflate_state;
    return ((((state.back as c_long) as c_long) << 16) as c_long) +
               if state.mode == COPY {
                   state.length
               } else if state.mode == MATCH {
                   state.was - state.length
               } else { 0 };
}
#[no_mangle]
pub unsafe extern "C" fn inflateCodesUsed(mut strm: z_streamp) -> c_long {
    if inflateStateCheck(strm) != 0 { return -1 as c_long; }
    let mut state = strm.state as *mut inflate_state;
    return state.next.offset(-state.codes) as c_long;
}
